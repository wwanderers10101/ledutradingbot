module.exports = async function (callback, exchangeName, apiKey, apiSecret) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();
    exchange.apiKey = apiKey;
    exchange.secret = apiSecret;
    exchange.options["warnOnFetchOpenOrdersWithoutSymbol"] = false;
    let markets = await exchange.fetchOpenOrders();
    callback(/* error */ null, markets);
};

//module.exports((err, result) => console.log(result), 'binance');