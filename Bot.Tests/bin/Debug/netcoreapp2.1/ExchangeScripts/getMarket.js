module.exports = async function (callback, exchangeName) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();
    let markets = await exchange.load_markets();
    callback(/* error */ null, markets);
};

//module.exports((err, result) => console.log(result), 'binance');