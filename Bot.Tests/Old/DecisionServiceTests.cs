﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using Core.Model;
//using Core.Model.Enums;
//using CryptoLp.Bot.Liquidity;
//using CryptoLp.Bot.Liquidity.Models;
//using CryptoLp.Bot.Models.WashTreding;
//using FluentAssertions;
//using NUnit.Framework;
//using OrderType = CryptoLp.Bot.Liquidity.Models.OrderType;
//using CoreOrderType = Core.Model.Enums.OrderType;
//
//namespace CryptoLp.Bot.Tests
//{
//    public class DecitionMakerTests
//    {
//        private static readonly Dictionary<decimal, decimal> _bidsDict = new Dictionary<decimal, decimal>
//        {
//            { 99.9m, 3m },
//            { 95m, 5m },
//            { 90m, 2m }
//        };
//        
//        private static readonly Dictionary<decimal, decimal> _asksDict = new Dictionary<decimal, decimal>
//        {
//            { 100m, 3m },
//            { 105m, 1m },
//            { 110m, 6m }
//        };
//        
//        private static readonly LocalOrderBookSnapshot _orderBook = new LocalOrderBookSnapshot(
//            _bidsDict.ToDictionary(x => x.Key, x => x.Value), 
//            _asksDict.ToDictionary(x => x.Key, x => x.Value), 
//            DateTime.UtcNow);
//
//        private const decimal DefaultTakerPrice = 5m;
//        private const decimal DefaultBalanceBase = 9999999999m;
//        private const decimal DefaultBalanceQuote = 9999999999m;
//        
//        private DecisionService _decisionService;
//
//        [SetUp]
//        public void Setup()
//        {
//            _decisionService = new DecisionService();
//        }
//        
//        [Test]
//        public void TestGetBestPriceShouldReturnCorrectBestBidPrice()
//        {
//            _decisionService.GetBestPrice(_orderBook, OrderSide.Buy).Should().Be(99.9m);
//        }
//
//        [Test]
//        public void TestGetBestPriceShouldReturnCorrectBestAskPrice()
//        {
//            _decisionService.GetBestPrice(_orderBook, OrderSide.Sell).Should().Be(100m);
//        }
//
//        [Test]
//        public void TestGetSizeToShowTaker1()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.BalanceQuote = 20m;
//            execution.MakerTaker = OrderType.Taker;
//
//            var sizeToShow = _decisionService.GetSizeToShow(_orderBook, DefaultTakerPrice, execution, 0m, 20m);
//
//            sizeToShow.Should().Be(execution.BalanceQuote / DefaultTakerPrice);
//        }
//
//        [Test]
//        public void TestGetSizeToShowWithMaxQuantity()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.BalanceQuote = 20m;
//            execution.MakerTaker = OrderType.Taker;
//            execution.MaximumQuantity = 3m;
//
//            var sizeToShow = _decisionService.GetSizeToShow(_orderBook, DefaultTakerPrice, execution, 0, 20);
//
//            sizeToShow.Should().Be(execution.MaximumQuantity);
//        }
//
//        [Test]
//        public void TestGetSizeToShowBuy1()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.DepthRef = 5m;
//            execution.SizePercent = 25m;
//
//            var sizeToShow = _decisionService.GetSizeToShow(
//                _orderBook,
//                DefaultTakerPrice, 
//                execution, 
//                DefaultBalanceBase, 
//                DefaultBalanceQuote);
//
//            sizeToShow.Should().Be(2m);
//        }
//
//        [Test]
//        public void TestGetSizeToShowBuy2()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.DepthRef = 10m;
//            execution.SizePercent = 25m;
//            
//            var sizeToShow = _decisionService.GetSizeToShow(
//                _orderBook, 
//                DefaultTakerPrice, 
//                execution, 
//                DefaultBalanceBase, 
//                DefaultBalanceQuote);
//
//            sizeToShow.Should().Be(2.5m);
//        }
//        
//        [Test]
//        public void TestGetSizeToShowSell1()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Sell;
//            execution.DepthRef = 5m;
//            execution.SizePercent = 25m;
//
//            var sizeToShow = _decisionService.GetSizeToShow(
//                _orderBook, 
//                DefaultTakerPrice, 
//                execution,
//                DefaultBalanceBase, DefaultBalanceQuote);
//
//            sizeToShow.Should().Be(1m);
//        }
//
//        [Test]
//        public void TestGetSizetoShowSell2()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Sell;
//            execution.DepthRef = 10m;
//            execution.SizePercent = 25m;
//
//            var sizeToShow = _decisionService.GetSizeToShow(
//                _orderBook,
//                DefaultTakerPrice,
//                execution,
//                DefaultBalanceBase,
//                DefaultBalanceQuote);
//
//            sizeToShow.Should().Be(2.5m);
//        }
//
//        [Test]
//        public void TestGetPriceBuyMaker1()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.SimpleSkewPercent = 0m;
//
//            var price = _decisionService.GetPrice(_orderBook, execution, DefaultBalanceBase, DefaultBalanceQuote);
//
//            price.Should().Be(99.9m);
//        }
//
//        [Test]
//        public void TestGetPriceBuyMaker2()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.SimpleSkewPercent = 10m;
//
//            var price = _decisionService.GetPrice(_orderBook, execution, DefaultBalanceBase, DefaultBalanceQuote);
//
//            price.Should().Be(99.91m);
//        }
//
//        [Test]
//        public void TestGetPriceSellMaker1()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Sell;
//            execution.SimpleSkewPercent = 0m;
//
//            var price = _decisionService.GetPrice(_orderBook, execution, DefaultBalanceBase, DefaultBalanceQuote);
//
//            price.Should().Be(100m);
//        }
//
//        [Test]
//        public void TestGetPriceSellMaeker2()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Sell;
//            execution.SimpleSkewPercent = 10m;
//
//            var price = _decisionService.GetPrice(_orderBook, execution, DefaultBalanceBase, DefaultBalanceQuote);
//
//            price.Should().Be(99.99m);
//        }
//
//        [Test]
//        public void TestGetPriceBuyTaker()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.BalanceQuote = 20m;
//            execution.MakerTaker = OrderType.Taker;
//
//            var price = _decisionService.GetPrice(_orderBook, execution, 0m, 20m);
//
//            price.Should().Be(100m);
//        }
//
//        [Test]
//        public void TestGetPriceSellTaker()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Sell;
//            execution.BalanceBase = 4m;
//            execution.MakerTaker = OrderType.Taker;
//
//            var price = _decisionService.GetPrice(_orderBook, execution, 4m, 0m);
//
//            price.Should().Be(95m);
//        }
//
//        [Test]
//        public void TestGetDecisionShouldWaitIfQtyBelowMin()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.SimpleSkewPercent = 0m;
//
//            var orderBook2 = new LocalOrderBookSnapshot(
//                new Dictionary<decimal, decimal>
//                {
//                    { 99.9m, 0.09m }
//                }, 
//                new Dictionary<decimal, decimal>
//                {
//                    { 100m, 0.1m }
//                }, 
//                DateTime.UtcNow);
//            
//            var decision = _decisionService.GetDecision(orderBook2, execution, new Order[0], DefaultBalanceBase, DefaultBalanceQuote);
//
//            decision.AwaitingMoreBalance.Should().BeTrue();
//        }
//
//        [Test]
//        public void TestGetDecisionShouldNotTradeNorWaitIfEmptyBook()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Buy;
//            execution.SimpleSkewPercent = 0m;
//
//            var orderBook2 = new LocalOrderBookSnapshot(
//                new Dictionary<decimal, decimal>(), 
//                new Dictionary<decimal, decimal>(), 
//                DateTime.UtcNow);
//            
//            var decision = _decisionService.GetDecision(orderBook2, execution, new Order[0], DefaultBalanceBase, DefaultBalanceQuote);
//
//            decision.Trade.Should().BeFalse();
//            decision.AwaitingMoreBalance.Should().BeFalse();
//        }
//
//        [Test]
//        public void TestGetDecisionDoesNotTradeIfWithinPriceAndQtySlopPrecent()
//        {
//            var execution = GetBaseExecution();
//            execution.QuantitySlopPercent = 5m;
//            execution.PriceSlopPercent = 5m;
//
//            var openOrders = new[]
//            {
//                new Order(null, null, null, null, null, OrderSide.Sell, price: 99.95m, amount: 6m, type: CoreOrderType.Market)
//                {
//                    FilledAmount = 1m,
//                    Status = OrderStatus.Creating
//                },
//            };
//
//            var decision = _decisionService.GetDecision(_orderBook, execution, openOrders, DefaultBalanceBase, DefaultBalanceQuote);
//
//            decision.Trade.Should().BeFalse();
//            decision.AwaitingMoreBalance.Should().BeFalse();
//            decision.Message.Should().Be("Newly calculated order price/quantity is within slop threshold");
//        }
//
//        [Test]
//        public void TestGetDecisionDoesTradeIfWithinPriceSlopButNotQtySlop()
//        {
//            var execution = GetBaseExecution();
//            execution.Side = OrderSide.Sell;
//            execution.QuantitySlopPercent = 0m;
//            execution.PriceSlopPercent = 5m;
//
//            var openOrders = new[]
//            {
//                new Order(null, null, null, null, null, OrderSide.Unknown, price: 99.95m, amount: 5.1m, type: CoreOrderType.Market)
//                {
//                    FilledAmount = 0m,
//                    Status = OrderStatus.Creating
//                },
//            };
//
//            var decision = _decisionService.GetDecision(_orderBook, execution, openOrders, DefaultBalanceBase, DefaultBalanceQuote);
//
//            decision.Trade.Should().BeTrue();
//        }
//
//        [Test]
//        public void TestGetDecisionDoesTradeIfWithinQtySlopButNotPriceSlop()
//        {
//            var execution = GetBaseExecution();
//            execution.QuantitySlopPercent = 5m;
//            execution.PriceSlopPercent = 0;
//            
//            var openOrders = new []
//            {
//                new Order(null, null, null, null, null, OrderSide.Unknown, price: 99.96m, amount: 5.0m, type: CoreOrderType.Market), 
//            };
//
//            var decision = _decisionService.GetDecision(_orderBook, execution, openOrders, DefaultBalanceBase, DefaultBalanceQuote);
//
//            decision.Trade.Should().BeTrue();
//        }
//
//        [Test]
//        public void TestMaximumSpreadPercent()
//        {
//            var execution = GetBaseExecution();
//            execution.MaximumSpreadPercent = 0.1m;
//            execution.MaximumSkewPercent = 0m;
//            var openOrders = new Order[0];
//
//            var decision = _decisionService.GetDecision(_orderBook, execution, openOrders, DefaultBalanceBase, DefaultBalanceQuote);
//
//            decision.Trade.Should().BeTrue();
//            decision.Order.Price.Should().Be(99.9m);
//        }
//
//        [Test]
//        public void TestSimpleSkew()
//        {
//            CheckSkewPrice(x => { }, 0.9m);
//            CheckSkewPrice(x => x.SimpleSkewPercent = 50m, 1m);
//            CheckSkewPrice(x => x.SimpleSkewPercent = -50m, 0.8m);
//        }
//
//        [Test]
//        public void TestExchangeBalanceSkew()
//        {
//            CheckSkewPrice(x =>
//                {
//                    x.ExchangeBalanceSkewFactor = 50m;
//                    x.ExchangeBalanceBase = 0m;
//                    x.ExchangeBalanceQuote = 1m;
//                },
//                1m);
//            
//            CheckSkewPrice(x =>
//                {
//                    x.ExchangeBalanceSkewFactor = 50m;
//                    x.ExchangeBalanceBase = 1m;
//                    x.ExchangeBalanceQuote = 0m;
//                },
//                0.8m);
//        }
//
//        [Test]
//        public void TestTotalBalanceSkew()
//        {
//            CheckSkewPrice(x =>
//                {
//                    x.TotalBalanceSkewFactor = 50m;
//                    x.ExchangeBalanceBase = 0m;
//                    x.ExchangeBalanceQuote = 0.5m;
//                    x.ColdStorageBase = 0m;
//                    x.ColdStorageQuote = 0.5m;
//                },
//                1m);
//            CheckSkewPrice(x =>
//                {
//                    x.TotalBalanceSkewFactor = 50m;
//                    x.ExchangeBalanceBase = 0.5m;
//                    x.ExchangeBalanceQuote = 0m;
//                    x.ColdStorageBase = 0.5m;
//                    x.ColdStorageQuote = 0m;
//                },
//                0.8m);
//        }
//        
//        public void CheckSkewPrice(Action<Execution> updateExecution, decimal expectedPrice)
//        {
//            var execution = GetBaseExecution();
//            execution.ColdStorageBase = 0m;
//            execution.ColdStorageQuote = 0m;
//            execution.ExchangeBalanceBase = 0m;
//            execution.ExchangeBalanceQuote = 0m;
//            execution.SimpleSkewPercent = 0m;
//            execution.TotalBalanceSkewFactor = 0m;
//
//            updateExecution(execution);
//            
//            var orderBook = new LocalOrderBookSnapshot(
//                new Dictionary<decimal, decimal>
//                {
//                    { 0.9m, 1m }
//                }, 
//                new Dictionary<decimal, decimal>
//                {
//                    { 1.1m, 1m}
//                },
//                DateTime.UtcNow);
//
//            var price = _decisionService.GetPrice(orderBook, execution, 1m, 1m);
//
//            price.Should().Be(expectedPrice);
//        }
//        
//        private Execution GetBaseExecution()
//        {
//            return new Execution
//            {
//                BaseCurrencyCode = "BTC",
//                QuoteCurrenycCode = "USDT",
//                SizePercent = 50,
//                SimpleSkewPercent = 50,
//                DepthRef = 10,
//                Side = OrderSide.Buy,
//                MinimumQuantity = 0.1m,
//                LotSize = 1e-8m,
//                PrecisionPrice = 8,
//                PrecisionAmount = 8,
//                PriceSlopPercent = 1m,
//                QuantitySlopPercent = 1m,
//                TimeSlopSeconds = TimeSpan.FromSeconds(5),
//                CumQuantityFilled = 0,
//                AverageFillPrice = 0,
//                MakerTaker = OrderType.Maker,
//                MaximumQuantity = null,
//                MinimumSpreadPercent = null,
//                MaximumSpreadPercent = null,
//                TotalBalanceSkewFactor = 0m,
//                ExchangeBalanceSkewFactor = 0m,
//                TotalBalanceBase = null,
//                TotalBalanceQuote = null,
//                MaximumSkewPercent = 50m
//            };
//        }
//    }
//}

namespace Bot.Liquidity.Tests.Old
{
}