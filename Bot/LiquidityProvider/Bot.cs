﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Exchanges.Core;
using Core.Model;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;
using Marten;
using Microsoft.AspNetCore.NodeServices;
using Serilog;

namespace CryptoLp.Bot.LiquidityProvider
{
    public class Bot: BotApi
    {
        private readonly AppSettings<BotSettings> _appSettings;

        [UsedImplicitly]
        private Thread _marketDataThread;

        public Bot(IDocumentStore documentStore, ICollection<Exchange> exchanges, AppSettings<BotSettings> appSettings, INodeServices nodeServices) 
            : base(documentStore, exchanges, appSettings, nodeServices)
        {
            _appSettings = appSettings;
        }
        
        public override async Task Initialize()
        {
            await base.Initialize();
            
            await CancelOpenOrders();
            await InitializeBalances();
            

            _marketDataThread = new Thread(HandleMarketData);
            _marketDataThread.Start();
        }

        private async void HandleMarketData(object obj)
        {

            try
            {
                var tasks = new List<Task>
                {
                };

                await await Task.WhenAny(tasks);
                
                throw new Exception("Happend something wrong");
            }
            catch (Exception e)
            {
                Log.Logger.Error(e, "Exception in HandleMarketData");
                throw;
            }
        }
        
        public new void Dispose()
        {
            base.Dispose();
 
        }
    }
}