using System;
using System.Collections.Generic;
using System.Linq;
using Core.Model;
using Core.Model.Enums;
using JetBrains.Annotations;

namespace CryptoLp.Bot.LiquidityProvider.Models
{
    public class CompositeOrder
    {
        public CompositeOrder(OrderSide side)
        {
            Side = side;
        }

        [NotNull]
        [ItemNotNull]
        public List<Order> Orders { get; private set; } = new List<Order>();
        
        public decimal Price { get; set; }
        public decimal Amount
        {
            get
            {
                decimal sum = 0;
                foreach (var x in Orders)
                {
                    sum += x.Amount;
                }

                return sum;
            }
        }

        public OrderSide Side { get; set; }


        public bool IsEquivalent([NotNull] EstimatingOrder order)
        {
            switch (order)
            {
                case NoneEstimatingOrder _ when Orders.Count == 0:
                    return true;
                case NoneEstimatingOrder _:
                    return false;
                case SomeEstimatingOrder someOrder:
                    return someOrder.Side == Side &&
                           someOrder.Price == Price &&
                           someOrder.Amount == Amount;
                default:
                    throw new ArgumentException("Unknown type of estimating order", nameof(order));
            }
        }

        public void ClearFinishedOrders()
        {
            Orders = Orders.Where(x => !x.IsFinished).ToList();
        }
    }
}