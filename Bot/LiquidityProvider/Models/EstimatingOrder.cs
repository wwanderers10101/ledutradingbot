﻿using Core.Model.Enums;

namespace CryptoLp.Bot.LiquidityProvider.Models
{
    public abstract class EstimatingOrder
    {
    }
    
    /// <remarks>
    /// When order isn't needed
    /// </remarks>>
    public class NoneEstimatingOrder : EstimatingOrder
    {
    }
    
    public class SomeEstimatingOrder : EstimatingOrder
    {
        public SomeEstimatingOrder(decimal price, decimal amount, OrderSide side)
        {
            Price = price;
            Amount = amount;
            Side = side;
        }

        public decimal Price { get; }
        public decimal Amount { get; }
        public OrderSide Side { get; }

        
        protected bool Equals(SomeEstimatingOrder other)
        {
            return Price == other.Price && Amount == other.Amount && Side == other.Side;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((EstimatingOrder) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Price.GetHashCode();
                hashCode = (hashCode * 397) ^ Amount.GetHashCode();
                hashCode = (hashCode * 397) ^ (int) Side;
                return hashCode;
            }
        }
    }

}