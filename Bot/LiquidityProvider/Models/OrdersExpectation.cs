﻿using JetBrains.Annotations;

namespace CryptoLp.Bot.LiquidityProvider.Models
{
    public class OrdersExpectation
    {
        public OrdersExpectation([NotNull] EstimatingOrder buyOrder, [NotNull] EstimatingOrder sellOrder)
        {
            BuyOrder = buyOrder;
            SellOrder = sellOrder;
        }

        [NotNull] 
        public EstimatingOrder BuyOrder { get; }
        
        [NotNull] 
        public EstimatingOrder SellOrder { get; }


        protected bool Equals(OrdersExpectation other)
        {
            return Equals(BuyOrder, other.BuyOrder) && Equals(SellOrder, other.SellOrder);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((OrdersExpectation) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((BuyOrder != null ? BuyOrder.GetHashCode() : 0) * 397) ^ (SellOrder != null ? SellOrder.GetHashCode() : 0);
            }
        }
    }
}