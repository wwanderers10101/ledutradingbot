﻿using Core.Model.Enums;
using CryptoLp.Bot.LiquidityProvider.Helpers;
using JetBrains.Annotations;

namespace CryptoLp.Bot.LiquidityProvider.Models
{
    public class CurrentOrdersState
    {
        public CurrentOrdersState()
        {
            BuyOrder = new LockWrapper<CompositeOrder>(new CompositeOrder(OrderSide.Buy));
            SellOrder = new LockWrapper<CompositeOrder>(new CompositeOrder(OrderSide.Sell));
        }

        [NotNull]
        public LockWrapper<CompositeOrder> BuyOrder { get; }

        [NotNull]
        public LockWrapper<CompositeOrder> SellOrder { get; }
    }
}