﻿using System;
using Core.Model.Enums;

namespace CryptoLp.Bot.LiquidityProvider.Old.Models
{
    public class Execution
    {
        public string BaseCurrencyCode { get; set; }
        public string QuoteCurrenycCode { get; set; }
        public decimal BalanceQuote { get; set; }
        public decimal BalanceBase { get; set; }
        public decimal SizePercent { get; set; }
        public decimal SimpleSkewPercent { get; set; }
        public decimal DepthRef { get; set; }
        public OrderSide Side { get; set; }
        public decimal MinimumQuantity { get; set; }
        public decimal LotSize { get; set; }
        public int PrecisionPrice { get; set; }
        public int PrecisionAmount { get; set; }
        public decimal PriceSlopPercent { get; set; }
        public decimal QuantitySlopPercent { get; set; }
        public TimeSpan TimeSlopSeconds { get; set; }
        public int CumQuantityFilled { get; set; }
        public int AverageFillPrice { get; set; }
        public OrderType MakerTaker { get; set; }
        public decimal? MaximumQuantity { get; set; }
        public decimal? MinimumSpreadPercent { get; set; }
        public decimal? MaximumSpreadPercent { get; set; }
        public decimal TotalBalanceSkewFactor { get; set; }
        public decimal ExchangeBalanceSkewFactor { get; set; }
        public decimal? TotalBalanceBase { get; set; }
        public decimal? TotalBalanceQuote { get; set; }
        public decimal MaximumSkewPercent { get; set; }
        public decimal? ColdStorageBase { get; set; }
        public decimal? ColdStorageQuote { get; set; }
        public decimal? ExchangeBalanceBase { get; set; }
        public decimal? ExchangeBalanceQuote { get; set; }
    }
}