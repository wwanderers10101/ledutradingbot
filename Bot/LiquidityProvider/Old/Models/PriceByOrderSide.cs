﻿namespace CryptoLp.Bot.LiquidityProvider.Old.Models
{
    public class PriceByOrderSide
    {
        public PriceByOrderSide(decimal? buyPrice, decimal? sellPrice)
        {
            BuyPrice = buyPrice;
            SellPrice = sellPrice;
        }

        public readonly decimal? BuyPrice;
        public readonly decimal? SellPrice;
    }
}