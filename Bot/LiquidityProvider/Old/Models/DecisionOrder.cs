﻿namespace CryptoLp.Bot.LiquidityProvider.Old.Models
{
    public class DecisionOrder
    {
        public decimal Price { get; set; }
        public decimal Quantity { get; set; }
    }
}