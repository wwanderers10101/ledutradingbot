﻿namespace CryptoLp.Bot.LiquidityProvider.Old.Models
{
    public class Decision
    {
        public bool Trade { get; set; }
        public bool AwaitingMoreBalance { get; set; }
        public string Message { get; set; }
        public DecisionOrder Order { get; set; }
    }
}