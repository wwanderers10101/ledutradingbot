﻿using System;
using System.Linq;
using Core.Model;
using Core.Model.Enums;
using CryptoLp.Bot.LiquidityProvider.MathHelper;
using CryptoLp.Bot.LiquidityProvider.Old.Models;
using JetBrains.Annotations;
using OrderType = CryptoLp.Bot.LiquidityProvider.Old.Models.OrderType;

namespace CryptoLp.Bot.LiquidityProvider.Old
{
    public class DecisionService
    {
        public decimal GetSizeToShow(LocalOrderBookSnapshot orderBook, decimal takerPrice, Execution execution, decimal balanceBase, decimal balanceQuote)
        {
            decimal sizeCanShow;

            switch (execution.MakerTaker)
            {
                case OrderType.Taker:
                    switch (execution.Side)
                    {
                        // If it's a taker order, take everything at once (get_price goes as deep into book as required for fill)
                        case OrderSide.Sell:
                            sizeCanShow = balanceBase;
                            break;
                        case OrderSide.Buy:
                            sizeCanShow = balanceQuote / takerPrice;
                            break;
                        default:
                            throw new ArgumentOutOfRangeException(nameof(execution.Side), execution.Side, null);
                    }
                    
                    break;
                case OrderType.Maker:
                    decimal size;
                    var side = execution.Side;
                    var bestPrice = GetBestPrice(orderBook, side);

                    switch (side)
                    {
                        case OrderSide.Buy:
                        {
                            var refPrice = bestPrice - bestPrice * execution.DepthRef / 100m;
                            size = orderBook.Bids.Where(x => x.Key > refPrice).Select(x => x.Value).Sum();
                            break;
                        }
                        case OrderSide.Sell:
                        {
                            var refPrice = bestPrice + bestPrice * execution.DepthRef / 100m;
                            size = orderBook.Asks.Where(x => x.Key <= refPrice).Select(x => x.Value).Sum();
                            
                            break;
                        }
                        // ReSharper disable once RedundantCaseLabel
                        case OrderSide.Unknown:
                        default:
                            throw new ArgumentOutOfRangeException(nameof(side), side, null);
                    }

                    sizeCanShow = size * execution.SizePercent / 100m;

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (execution.MaximumQuantity != null && execution.MaximumQuantity <= sizeCanShow)
            {
                sizeCanShow = execution.MaximumQuantity.Value;
            }

            return sizeCanShow.FloorD(execution.PrecisionAmount);
        }

        public decimal GetPrice(LocalOrderBookSnapshot orderBook, Execution execution, decimal balanceBase, decimal balanceQuote)
        {
            var price = 0m;
            var bestAsk = GetBestPrice(orderBook, OrderSide.Sell);
            var bestBid = GetBestPrice(orderBook, OrderSide.Buy);

            var mid = (bestBid + bestAsk) / 2m;
            var spreadPercent = (bestAsk / bestBid - 1m) * 100m;

            var spread = execution.MinimumSpreadPercent != null && spreadPercent < execution.MinimumSpreadPercent
                ? execution.MinimumSpreadPercent.Value / 100m * mid
                : bestAsk - bestBid;

            var simpleSkew = spread * execution.SimpleSkewPercent / 100m;

            var exchangeBalanceSkew = execution.ExchangeBalanceBase != null && execution.ExchangeBalanceQuote != null
                ? CalculateBalanceSkew(
                    spread,
                    mid,
                    execution.ExchangeBalanceBase.Value,
                    execution.ExchangeBalanceQuote.Value,
                    execution.ExchangeBalanceSkewFactor,
                    execution.Side)
                : 0m;

            var totalBalanceSkew =
                execution.ExchangeBalanceBase != null &&
                execution.ColdStorageBase != null &&
                execution.ExchangeBalanceQuote != null &&
                execution.ColdStorageQuote != null
                    ? CalculateBalanceSkew(
                        spread,
                        mid,
                        execution.ExchangeBalanceBase.Value + execution.ColdStorageBase.Value,
                        execution.ExchangeBalanceQuote.Value + execution.ColdStorageQuote.Value,
                        execution.TotalBalanceSkewFactor,
                        execution.Side
                    )
                    : 0m;

            var totalSkew = simpleSkew + totalBalanceSkew + exchangeBalanceSkew;
            var maxSkew = execution.MaximumSkewPercent * spread;
            if (totalSkew > maxSkew)
            {
                totalSkew = maxSkew;
            }

            var maxSpreadPercent = execution.MaximumSpreadPercent;
            if (execution.Side == OrderSide.Buy)
            {
                if (execution.MakerTaker == OrderType.Taker)
                {
                    var sizeCanTake = 0m;
                    foreach (var pair in orderBook.Asks)
                    {
                        sizeCanTake += pair.Value;
                        price = pair.Key;
                        // TODO need more precise calculation for sizeCanTake 
                        // TODO it seems that we do 1 step for price
                        if (sizeCanTake >= balanceQuote / price)
                        {
                            break;
                        }
                    }
                } 
                else if (maxSpreadPercent != null && spreadPercent > maxSpreadPercent.Value)
                {
                    price = bestAsk * (1 - maxSpreadPercent.Value / 100m) + totalSkew;
                }
                else
                {
                    price = bestBid + totalSkew;
                }
            } 
            else if (execution.Side == OrderSide.Sell)
            {
                if (execution.MakerTaker == OrderType.Taker)
                {
                    var sizeCanTake = 0m;
                    foreach (var pair in orderBook.Bids)
                    {
                        sizeCanTake += pair.Value;
                        price = pair.Key;
                        // TODO need more precise calculation for sizeCanTake 
                        // TODO it seems that we do 1 step for price
                        if (sizeCanTake >= balanceBase)
                        {
                            break;
                        }
                    }
                }
                // TODO posible we should addition price offset by analogy with OrderSide.Buy conditional branch
                else
                {
                    price = bestAsk - totalSkew;
                }
            }

            return price.FloorD(execution.PrecisionPrice);
        }

        private decimal CalculateBalanceSkew(decimal spread, decimal mid, decimal balanceBase, decimal balanceQuote, decimal skewFactor, OrderSide side)
        {
            var numerator = -(balanceBase * mid - balanceQuote);
            var denominator = balanceBase * mid + balanceQuote;
            var ratio = denominator > 0 ? numerator / denominator : 0;
            var skew = skewFactor * ratio * spread / 100m;

            if (side == OrderSide.Sell)
            {
                skew *= -1m;
            }

            return skew;
        }

        public Decision GetDecision(LocalOrderBookSnapshot orderBook, Execution execution, Order[] openOrders, decimal balanceBase, decimal balanceQuote)
        {
            var decision = new Decision
            {
                Trade = false,
                AwaitingMoreBalance = false
            };

            if (execution.Side == OrderSide.Buy && balanceQuote == 0m ||
                execution.Side == OrderSide.Sell && balanceBase == 0m)
            {
                decision.AwaitingMoreBalance = true;
                return decision;
            }

            if (orderBook.Bids.Count == 0 || orderBook.Asks.Count == 0)
            {
                decision.Message = "Order book is empty";
                return decision;
            }

            var price = GetPrice(orderBook, execution, balanceBase, balanceQuote);
            
            var order = new DecisionOrder
            {
                Price = price,
                Quantity = GetSizeToShow(orderBook, price, execution, balanceBase, balanceQuote)
            };

            if (price == 0)
            {
                throw new Exception("DecisionService: price cannot be zero");
            }

            var quantityRemaning = 0m;

            if (execution.Side == OrderSide.Buy)
            {
                quantityRemaning = balanceQuote / price;
            } else if (execution.Side == OrderSide.Sell)
            {
                quantityRemaning = balanceBase;
            }
            else
            {
                throw new ArgumentException("Unknown variant", nameof(execution.Side));
            }

            if (quantityRemaning == 0m || quantityRemaning < execution.MinimumQuantity)
            {
                decision.AwaitingMoreBalance = true;
                return decision;
            }

            if (quantityRemaning <= order.Quantity)
            {
                order.Quantity = quantityRemaning;
            }

            order.Quantity = order.Quantity.FloorD(execution.PrecisionAmount);

            if (order.Quantity < execution.MinimumQuantity)
            {
                decision.Message = $"Waiting for balance: size to show {order.Quantity} is less than minimum order size {execution.MinimumQuantity}";
                decision.AwaitingMoreBalance = true;
                return decision;
            }

            if (IsWithinSlopThresholds(order, execution, openOrders))
            {
                decision.Message = "Newly calculated order price/quantity is within slop threshold";
                return decision;
            }

            if (order.Quantity > 0m)
            {
                decision.Trade = true;
                decision.Order = order;
                return decision;
            }
            
            throw new Exception("Unexpected case");
        }

        private static bool IsWithinSlopThresholds(DecisionOrder order, Execution execution, Order[] openOrders)
        {
            // TODO why only for 1 order 
            if (openOrders.Length == 0)
            {
                return false;
            }

            if (openOrders.Length > 1)
            {
                LogError("Multiple open orders passed into decision service and it doesn\'t know what to do");
                
                throw new IndexOutOfRangeException("Only expected 1 open order");
            }

            var openOrder = openOrders.First();

            // TODO abstract DateTime.UtcNow
            var timeSinceCreation = DateTime.UtcNow - openOrder.CreatedDateTime;
            if (timeSinceCreation < execution.TimeSlopSeconds)
            {
                return true;
            }

            var quantityRemaining = openOrder.Amount - openOrder.FilledAmount;
            var newQuantity = order.Quantity;
            var quantityDeltaPercent = Math.Abs(quantityRemaining - newQuantity) / newQuantity * 100m;
            var priceDeltaPercent = Math.Abs(order.Price - openOrder.Price.Value) / order.Price * 100m;
            var withinSlop = quantityDeltaPercent <= execution.QuantitySlopPercent && priceDeltaPercent <= execution.PriceSlopPercent;
            return withinSlop;
        }

        private static void LogError(string errorMessage)
        {
            // TODO need implement
        }

        public decimal GetBestPrice([NotNull] LocalOrderBookSnapshot orderBook, OrderSide orderSide)
        {
            switch (orderSide)
            {
                case OrderSide.Buy:
                    return orderBook.Bids.First().Key;
                    break;
                case OrderSide.Sell:
                    return orderBook.Asks.First().Key;
                    break;
                // ReSharper disable once RedundantCaseLabel
                case OrderSide.Unknown:
                default:
                    throw new ArgumentOutOfRangeException(nameof(orderSide), orderSide, null);
            }
        }
    }
}