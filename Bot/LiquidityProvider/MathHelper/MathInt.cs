﻿namespace CryptoLp.Bot.LiquidityProvider.MathHelper
{
    public static class MathInt
    {
        public static int Pow(this int value, int exp)
        {
            var acc = 1;
            for (int i = 0; i < exp; i++)
            {
                acc *= value;
            }

            return acc;
        }
    }
}