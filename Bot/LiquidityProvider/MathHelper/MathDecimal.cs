﻿namespace CryptoLp.Bot.LiquidityProvider.MathHelper
{
    public static class MathDecimal
    {
        public static decimal FloorD(this decimal value, int precitionDigits)
        {
            var power = 10.Pow(precitionDigits);

            var result = System.Math.Floor(value * power) / power;
            return result;
        }
    }
}