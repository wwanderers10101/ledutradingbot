﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Bot.LocalSnapshots;
using Core.Model;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;

namespace CryptoLp.Bot.LiquidityProvider
{
    public class LiquidityProviderManager : IDisposable
    {
        private readonly AppSettings<BotSettings> _appSettings;
        private readonly BotApi _botApi;

        [NotNull]
        private readonly Dictionary<ExchangeIdSymbol, LiquidityStrategy> _strategiesByExchangeSymbol = new Dictionary<ExchangeIdSymbol, LiquidityStrategy>();
        
        public LiquidityProviderManager(
            AppSettings<BotSettings> appSettings,
            BotApi botApi)
        {
            _appSettings = appSettings;
            _botApi = botApi;

            if (appSettings.Bot?.Liquidity?.IsEnabled != true)
            {
                return;
            }

            var liquiditySettings = appSettings.Bot.Liquidity;
            if (liquiditySettings?.Exchanges == null)
            {
                return;
            }

            var exchangesConfig = liquiditySettings.Exchanges;
            if (exchangesConfig == null)
            {
                throw new Exception("Can't create liquidity provider because of exchanges is not specified in config");
            }

            var exchangeList = botApi.ExchangesById.Values
                .Where(x => x.IsTrading)
                .ToArray();
            
            var exchangesByExchangeIdAndCurrencyCodePair = exchangeList
                .SelectMany(x => x.Symbols, (exchange, symbol) => (exchange, symbol))
                .ToDictionary(x => (x.exchange.Id, x.symbol.CurrencyCodePair));
            
            foreach (var exchangeConfig in exchangesConfig)
            {
                var exchangeId = exchangeConfig.Key;
                foreach (var currencyCodePair in exchangeConfig.Value)
                {
                    if (!exchangesByExchangeIdAndCurrencyCodePair.TryGetValue((exchangeId, currencyCodePair),
                        out var exchangeSymbolPair))
                    {
                        throw new Exception($"Can't find initialized ExchangeId:CurrencyCodePair {exchangeId}:{currencyCodePair}");
                    }

                    var exchange = exchangeSymbolPair.exchange;
                    var exchangeSymbol = new ExchangeIdSymbol(exchange.Id, exchange.Name, exchangeSymbolPair.symbol);
                    var strategy = new LiquidityStrategy(_appSettings, _botApi, exchangeSymbol, exchange);
                    
                    _strategiesByExchangeSymbol.Add(exchangeSymbol, strategy);
                }
            }
                            
            botApi.CreateOrderSucceeded += OnCreateOrderSucceeded;
            botApi.CreateOrderFailed += OnCreateOrderFailed;
            botApi.CancelOrderSucceeded += OnCancelOrderSucceeded;
            botApi.CancelOrderFailed += OnCancelOrderFailed;
            botApi.OrderFilled += OnOrderFilled;
            botApi.OrderCompleted += OnOrderCompleted;
        }

        private void OnCreateOrderSucceeded(Order order)
        {
            if (!_strategiesByExchangeSymbol.TryGetValue(GetExchangeSymbol(order), out var strategy))
            {
                return;
            }
            
            strategy.OrderCreationSucceededEvent.RaiseEvent(order);
        }

        private void OnCreateOrderFailed(Order order)
        {
            if (!_strategiesByExchangeSymbol.TryGetValue(GetExchangeSymbol(order), out var strategy))
            {
                return;
            }
            
            strategy.OrderCreationFailedEvent.RaiseEvent(order);
        }

        private void OnCancelOrderSucceeded(Order order)
        {
            if (!_strategiesByExchangeSymbol.TryGetValue(GetExchangeSymbol(order), out var strategy))
            {
                return;
            }
            
            strategy.OrderCancellationSuccededEvent.RaiseEvent(order);
        }

        private void OnCancelOrderFailed(Order order)
        {
            if (!_strategiesByExchangeSymbol.TryGetValue(GetExchangeSymbol(order), out var strategy))
            {
                return;
            }
            
            strategy.OrderCancellationFailedEvent.RaiseEvent(order);
        }

        private void OnOrderFilled(Order order)
        {
            if (!_strategiesByExchangeSymbol.TryGetValue(GetExchangeSymbol(order), out var strategy))
            {
                return;
            }
            
            strategy.OrderFilledEvent.RaiseEvent(order);            
        }

        private void OnOrderCompleted(Order order)
        {
            if (!_strategiesByExchangeSymbol.TryGetValue(GetExchangeSymbol(order), out var strategy))
            {
                return;
            }
            
            strategy.OrderCompletedEvent.RaiseEvent(order);
        }

        private ExchangeIdSymbol GetExchangeSymbol(Order order)
        {
            return new ExchangeIdSymbol(order.ExchangeId, order.ExchangeName, _botApi.GetSymbol(order.ExchangeId, order.CurrencyPair));
        }

        [CanBeNull]
        public Task Start()
        {
            if (_appSettings.Bot?.Liquidity?.IsEnabled != true)
            {
                return null;
            }

            var tasks = _strategiesByExchangeSymbol.Values
                .Select(x => Task.Run(x.Start))
                .ToList();

            if (tasks.Count == 0)
            {
                return null;
            }
            
            return InnerStart(tasks);
        }

        private static async Task InnerStart(List<Task> tasks)
        {
            await await Task.WhenAny(tasks);

            throw new Exception("Happend something wrong");
        }


        public void Dispose()
        {
            _botApi.OrderCompleted -= OnOrderCompleted;
        }
    }
}