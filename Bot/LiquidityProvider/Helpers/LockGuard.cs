﻿using System;
using System.Threading;
using JetBrains.Annotations;

namespace CryptoLp.Bot.LiquidityProvider.Helpers
{
    public readonly struct LockGuard<T> : IDisposable where T : class
    {
        private readonly object _syncObj;
        
        public LockGuard([CanBeNull] T value, object syncObj)
        {
            _syncObj = syncObj;
            Value = value;
        }
        
        [PublicAPI]
        [CanBeNull] 
        public readonly T Value;
        
        public void Dispose()
        {
            Monitor.Exit(_syncObj);
        }
    }
}