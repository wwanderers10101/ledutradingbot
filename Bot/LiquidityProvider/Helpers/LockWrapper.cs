﻿using System.Threading;
using JetBrains.Annotations;

namespace CryptoLp.Bot.LiquidityProvider.Helpers
{
    public class LockWrapper<T> where T: class
    {
        // TODO нужно использовать AsyncLock
        private readonly object _syncObj = new object();

        [CanBeNull]
        private T _value;
        
        /// <summary>
        /// Create lock wrapper for value
        /// </summary>
        /// <remarks>
        /// You should exclusively operate with value
        /// </remarks>
        /// <param name="value"></param>
        public LockWrapper([CanBeNull] T value)
        {
            _value = value;
        }

        [PublicAPI]
        public LockGuard<T> Lock()
        {
            var syncObj = _syncObj;
            Monitor.Enter(syncObj);
            return new LockGuard<T>(_value, syncObj);
        }

        [PublicAPI]
        public LockGuard<T>? TryLock()
        {
            var syncObj = _syncObj;
            
            if (Monitor.TryEnter(syncObj))
            {
                return new LockGuard<T>(_value, syncObj);
            }

            return null;
        }

        /// <summary>
        /// Replace old value with new value
        /// </summary>
        /// <param name="newValue">New value</param>
        /// <returns>Returns old value</returns>
        [PublicAPI]
        [CanBeNull]
        public T Replace([CanBeNull] T newValue)
        {
            lock (_syncObj)
            {
                var oldValue = _value;
                _value = newValue;
                return oldValue;
            }
        }

        [PublicAPI]
        public bool TryReplace([CanBeNull] T value, out T oldValue)
        {
            var syncObj = _syncObj;
            if (!Monitor.TryEnter(syncObj))
            {
                oldValue = null;
                return false;
            }
            
            oldValue = 
            _value = value;
            Monitor.Exit(syncObj);
            return true;
        }
    }
}