﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Bot.LocalSnapshots;
using Core.Bot.TaskExtensions;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using CryptoLp.Bot.LiquidityProvider.Helpers;
using CryptoLp.Bot.LiquidityProvider.Models;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;
using Newtonsoft.Json;
using Serilog;

namespace CryptoLp.Bot.LiquidityProvider
{
    public class LiquidityStrategy
    {
        public static readonly string StrategyName = "LiquidityProvider";
        
        private readonly AppSettings<BotSettings> _appSettings;
        private readonly BotApi _botApi;
        private readonly TradeInfoBuilder _tradeInfoBuilder;
        private readonly ExchangeIdSymbol _exchangeIdSymbol;
        private readonly Exchange _exchange;

        internal TaskEvent<Order> OrderCreationSucceededEvent { get; } = new TaskEvent<Order>();
        internal TaskEvent<Order> OrderCreationFailedEvent { get; } = new TaskEvent<Order>();
        internal TaskEvent<Order> OrderCancellationSuccededEvent { get; } = new TaskEvent<Order>();
        internal TaskEvent<Order> OrderCancellationFailedEvent { get; } = new TaskEvent<Order>();
        internal TaskEvent<Order> OrderCompletedEvent { get; } = new TaskEvent<Order>();
        internal TaskEvent<Order> OrderFilledEvent { get; } = new TaskEvent<Order>();

        private readonly TaskEvent<OrdersExpectation> _ordersExpectationChangedEvent = new TaskEvent<OrdersExpectation>();
        
        public LiquidityStrategy(
            AppSettings<BotSettings> appSettings,
            BotApi botApi, 
            ExchangeIdSymbol exchangeIdSymbol,
            Exchange exchange)
        {
            _appSettings = appSettings;
            _botApi = botApi;
            _exchangeIdSymbol = exchangeIdSymbol;
            _exchange = exchange;

            _tradeInfoBuilder = new TradeInfoBuilder(appSettings, _botApi.BalanceManager);
        }

        public async Task Start()
        {
            var estimationTask = Task.Run(EstimateState);
            var ordersManipulationTask = Task.Run(OrdersManipulation);

            var firstTask = await Task.WhenAny(estimationTask, ordersManipulationTask);
            await firstTask;

            var taskName = firstTask == estimationTask 
                ? nameof(estimationTask) 
                : nameof(ordersManipulationTask);

            throw new Exception("Happend something wrong in " + taskName);
        }
        
        private async Task EstimateState()
        {
            while (true)
            {
                var orderBookUpdateTask = _botApi.WaitOrderBookUpdate();

                await orderBookUpdateTask;

                var ordersExpectation = MakeDecision();
                if (ordersExpectation == null)
                {
                    continue;
                }
                
                _ordersExpectationChangedEvent.RaiseEvent(ordersExpectation);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        [CanBeNull]
        private OrdersExpectation MakeDecision()
        {
            var snapshot = _botApi.LocalSnapshotService.GetSnapshot(_exchangeIdSymbol.ExchangeNameSymbol);

            var prices = snapshot.CalculatePrice();

            var tradeInfo = _tradeInfoBuilder.BuildLiquidityTradeInfo(
                _exchange,
                _exchangeIdSymbol,
                prices);

            if (!tradeInfo.CanBeTraded)
            {
                Log.Logger.Verbose(tradeInfo.ErrorMessage);
                return null;
            }

            var buyOrder = new SomeEstimatingOrder(tradeInfo.BuyPrice, tradeInfo.BuyAmount, OrderSide.Buy);
            var sellOrder = new SomeEstimatingOrder(tradeInfo.SellPrice, tradeInfo.SellAmount, OrderSide.Sell);
            var ordersExpectation = new OrdersExpectation(buyOrder, sellOrder);

            return ordersExpectation;
        }

        private async Task OrdersManipulation()
        {
            var state = new CurrentOrdersState();

            OrdersExpectation lastOrdersExpectation = null;
            while (true)
            {
                var ordersExpectationChangedTask = _ordersExpectationChangedEvent.WaitAsync();
                var orderCreationSucceededTask = OrderCreationSucceededEvent.WaitAsync();
                var orderCreationFailedTask = OrderCreationFailedEvent.WaitAsync();
                var orderCancellationSucceededTask = OrderCancellationSuccededEvent.WaitAsync();
                var orderCancellationFailedTask = OrderCancellationFailedEvent.WaitAsync();
                var orderFilledEventTask = OrderCompletedEvent.WaitAsync();
                var orderCompletedEventTask = OrderCompletedEvent.WaitAsync();

                var firstTask = await Task.WhenAny(
                    ordersExpectationChangedTask,
                    orderCreationSucceededTask,
                    orderCreationFailedTask,
                    orderCancellationSucceededTask,
                    orderCancellationFailedTask,
                    orderFilledEventTask,
                    orderCompletedEventTask);
                
                await firstTask;

                if (firstTask == ordersExpectationChangedTask)
                {
                    var newOrdersExpectation = ordersExpectationChangedTask.Result;
                    if (lastOrdersExpectation != null && lastOrdersExpectation.Equals(newOrdersExpectation))
                    {
                        continue;
                    }
                    
                    lastOrdersExpectation = newOrdersExpectation;

                    await OnOrdersExpectationChanged(lastOrdersExpectation, newOrdersExpectation, state);
                }
                else if (firstTask == orderCancellationFailedTask)
                {
                    var order = orderCancellationFailedTask.Result;
                    await CancelOrder(order);
                }
                else if (firstTask == orderCreationFailedTask)
                {
                    var order = orderCreationFailedTask.Result;
                    await CreateOrder(order);
                }
                else if (firstTask == orderCompletedEventTask)
                {
                    var order = orderCompletedEventTask.Result;
                    await OnOrderCompleted(order, lastOrdersExpectation, state);
                }
                else if (firstTask == orderCancellationSucceededTask)
                {
                    var order = orderCancellationSucceededTask.Result;
                    await OnOrderCancellationSucceeded(order, lastOrdersExpectation, state);
                }
                else if (firstTask == orderFilledEventTask)
                {
                    var order = orderFilledEventTask.Result;
                    await OnOrderFilled(order, lastOrdersExpectation, state);
                }
                else if (firstTask == orderCreationSucceededTask)
                {
                    var order = orderCreationSucceededTask.Result;
                    await OnOrderCreationSucceeded(order, lastOrdersExpectation, state);
                }
                else
                {
                    throw new Exception("Completed unknown task of type " + firstTask.GetType());
                }
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private Task OnOrderFilled(Order order, OrdersExpectation ordersExpectation, CurrentOrdersState state)
        {
            return SynchronizeBothSideOrders(ordersExpectation, state);
        }

        private Task OnOrderCreationSucceeded(Order order, OrdersExpectation ordersExpectation, CurrentOrdersState state)
        {
            return SynchronizeBothSideOrders(ordersExpectation, state);
        }

        private Task OnOrderCancellationSucceeded(Order order, OrdersExpectation ordersExpectation, CurrentOrdersState state)
        {
            return SynchronizeBothSideOrders(ordersExpectation, state);
        }

        private Task OnOrderCompleted(Order order, OrdersExpectation ordersExpectation, CurrentOrdersState state)
        {
            return SynchronizeBothSideOrders(ordersExpectation, state);
        }

        private Task CancelOrder(Order order)
        {
            return _botApi.CancelOrder(order);   
        }

        private Task CancelAllOrdersIfCan(List<Order> orders)
        {
            var tasks = new List<Task>();
            foreach (var order in orders)
            {
                if (order.Status == OrderStatus.Created)
                {
                    tasks.Add(_botApi.CancelOrder(order));
                }
            }

            if (tasks.Count == 0)
            {
                return Task.CompletedTask;
            }

            return Task.WhenAll(tasks);
        }

        private Task CreateOrder(Order order)
        {
            return _botApi.CreateOrder(order);
        }

        private Task OnOrdersExpectationChanged(
            [CanBeNull] OrdersExpectation lastOrdersExpectation,
            [NotNull] OrdersExpectation newOrdersExpectation, 
            [NotNull] CurrentOrdersState state)
        {
            return SynchronizeBothSideOrders(newOrdersExpectation, state);
        }

        private Task SynchronizeBothSideOrders([CanBeNull] OrdersExpectation ordersExpectation, [NotNull]CurrentOrdersState state)
        {
            if (ordersExpectation == null)
            {
                return Task.CompletedTask;
            }
            
            var buyOrderCreationTask = SynchronizeOrderSide(state.BuyOrder, ordersExpectation.BuyOrder);
            var sellOrderCreationTask = SynchronizeOrderSide(state.SellOrder, ordersExpectation.SellOrder);

            return Task.WhenAll(buyOrderCreationTask, sellOrderCreationTask);
        }

        private Task SynchronizeOrderSide(
            [NotNull] LockWrapper<CompositeOrder> currentCompositeOrderWrapper, 
            [NotNull] EstimatingOrder estimatingOrder)
        {
            using (var orderGuard = currentCompositeOrderWrapper.Lock())
            {
                var compositeOrder = orderGuard.Value;

                compositeOrder.ClearFinishedOrders();

                switch (estimatingOrder)
                {
                    case NoneEstimatingOrder _:
                    {
                        return CancelAllOrdersIfCan(compositeOrder.Orders);
                    }
                    case SomeEstimatingOrder someEstimatingOrder:
                    {
                        if (compositeOrder.Side != someEstimatingOrder.Side)
                        {
                            var serializedEstimatingOrder = JsonConvert.SerializeObject((object) estimatingOrder);
                            var serializedCompositeOrder = JsonConvert.SerializeObject(compositeOrder);
                            
                            throw new Exception($"Unknown error unmatched orders side. Estimating order {serializedEstimatingOrder}. Composite order {serializedCompositeOrder}");
                        }
                        
                        if (compositeOrder.Price == someEstimatingOrder.Price)
                        {
                            if (compositeOrder.Amount >= someEstimatingOrder.Amount)
                            {
                                return Task.CompletedTask;
                            }
                            
                            // TODO check min amount order condition
                            var newOrderAmount = someEstimatingOrder.Amount - compositeOrder.Amount;

                            return CreateOrder(someEstimatingOrder, compositeOrder, newOrderAmount);                            
                        }

                        if (compositeOrder.Orders.Count == 0)
                        {
                            compositeOrder.Price = someEstimatingOrder.Price;
                                
                            return CreateOrder(someEstimatingOrder, compositeOrder, someEstimatingOrder.Amount);
                        }
                        else
                        {
                            return CancelAllOrdersIfCan(compositeOrder.Orders);
                        }
                    }
                    default:
                    {
                        throw new ArgumentException($"Unknown type of {nameof(estimatingOrder)}", nameof(estimatingOrder));
                    }
                }
            }
        }

        private Task CreateOrder(
            [NotNull] SomeEstimatingOrder someEstimatingOrder, 
            [NotNull] CompositeOrder compositeOrder, 
            decimal amount)
        {
            var exchangeSymbol = _exchangeIdSymbol;

            var newOrder = new Order(
                null,
                exchangeSymbol.ExchangeId,
                exchangeSymbol.ExchangeName,
                exchangeSymbol.Symbol.CurrencyPair,
                _exchange.GenerateClientOrderId(),
                someEstimatingOrder.Side,
                someEstimatingOrder.Price,
                amount,
                OrderType.Limit,
                true,
                StrategyName)
            {
                Status = OrderStatus.Creating
            };

            compositeOrder.Orders.Add(newOrder);

            return _botApi.CreateOrder(newOrder);
        }
    }
}