﻿using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace CryptoLp.Bot.ExchangeScripts.CcxtMarket
{
    public class CcxtMarket
    {
        [JsonProperty("limits")]
        public Limits Limits { get; set; }

        [JsonProperty("precision")]
        public Precision Precision { get; set; }

        [JsonProperty("tierBased")]
        public bool TierBased { get; set; }

        [JsonProperty("percentage")]
        public bool Percentage { get; set; }

        [JsonProperty("taker")]
        public double Taker { get; set; }

        [JsonProperty("maker")]
        public double Maker { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("base")]
        public string Base { get; set; }

        [JsonProperty("quote")]
        public string Quote { get; set; }

        [JsonProperty("baseId")]
        public string BaseId { get; set; }

        [JsonProperty("quoteId")]
        public string QuoteId { get; set; }

        [JsonProperty("info")]
        public Info Info { get; set; }

        [JsonProperty("lot")]
        public double Lot { get; set; }

        [JsonProperty("active")]
        public bool Active { get; set; }
        
        public static Dictionary<string, CcxtMarket> FromJson(string json) => JsonConvert.DeserializeObject<Dictionary<string, CcxtMarket>>(json, Converter.Settings);
    }

    public class Info
    {
        [JsonProperty("symbol")]
        public string Symbol { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("baseAsset")]
        public string BaseAsset { get; set; }

        [JsonProperty("baseAssetPrecision")]
        public long BaseAssetPrecision { get; set; }

        [JsonProperty("quoteAsset")]
        public string QuoteAsset { get; set; }

        [JsonProperty("quotePrecision")]
        public long QuotePrecision { get; set; }

        [JsonProperty("orderTypes")]
        public List<string> OrderTypes { get; set; }

        [JsonProperty("icebergAllowed")]
        public bool IcebergAllowed { get; set; }

        [JsonProperty("filters")]
        public List<Dictionary<string, string>> Filters { get; set; }
    }

    public class Limits
    {
        [JsonProperty("amount")]
        public MinMax Amount { get; set; }

        [JsonProperty("price")]
        public MinMax Price { get; set; }

        [JsonProperty("cost")]
        public Min Cost { get; set; }
    }

    public class MinMax
    {
        [JsonProperty("min")]
        public decimal Min { get; set; }

        [JsonProperty("max")]
        public decimal? Max { get; set; }
    }

    public class Min
    {
        [JsonProperty("min")]
        public decimal MinValue { get; set; }
    }

    public class Precision
    {
        [JsonProperty("base")]
        public long Base { get; set; }

        [JsonProperty("quote")]
        public long Quote { get; set; }

        [JsonProperty("amount")]
        public long Amount { get; set; }

        [JsonProperty("price")]
        public long Price { get; set; }
    }

    public static class Serialize
    {
        public static string ToJson(this Dictionary<string, CcxtMarket> self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = { new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }
}
