﻿namespace CryptoLp.Bot.ExchangeScripts.CcxtMarket
{
    public class CcxtCurrency
    {
        public string Id { get; set; }
        public string Code { get; set; }
    }
}