﻿using System.Collections.Generic;
using System.Linq;

namespace CryptoLp.Bot.Settings
{
    public class BotSettings
    {
        public Dictionary<string, decimal> TradeAmount { get; set; }
        public TelegramSettings Telegram { get; set; }
        public PriceManagementSettings PriceManagement { get; set; }
        public WashTradingSettings WashTrading { get; set; }
        public LiquidityProviderSettings Liquidity { get; set; }
    }

    public class TelegramSettings
    {
        public string ApiKey { get; set; }
        public int ChatId { get; set; }
        public int ChatId2 { get; set; }
    }

    public class BuyTopOrderSettings
    {
        public double IterationTimeoutInSeconds { get; set; }
        public double OrderCancellationTimeoutInSeconds { get; set; }
        public decimal LastTradeDiffTresholdInPercentage_Min { get; set; }
        public decimal LastTradeDiffTresholdInPercentage_Max { get; set; }
        
        public Dictionary<string, Dictionary<string, PriceManagementExchangeSettings>> Exchanges { get; set; }
    }

    public class MinSpreadSettings
    {
        public double IterationTimeoutInSeconds { get; set; }
        public decimal SpreadTresholdInPercentage_Min { get; set; }
        public decimal SpreadTresholdInPercentage_Max { get; set; }
        public decimal PriceIncreaseInPercentage { get; set; }
        
        public Dictionary<string, Dictionary<string, PriceManagementExchangeSettings>> Exchanges { get; set; }
    }
    
    public class PriceManagementSettings
    {
        public bool IsEnabled { get; set; }
        public BuyTopOrderSettings BuyTopOrder { get; set; }
        public MinSpreadSettings MinSpread { get; set; }        
    }

    public class PriceManagementExchangeSettings
    {
        public decimal BuyAmount_Min { get; set; }
        public decimal BuyAmount_Max { get; set; }
    }
    
    public class WashTradingSettings
    {
        public bool IsEnabled { get; set; }
        
        /// <summary>
        /// in percents
        /// </summary>
        public decimal PriceMaxDropdown { get; set; }

        /// <summary>
        /// in seconds
        /// </summary>
        public double TradesAnalizeWindowDuration { get; set; }
        
        public WashTradingExchangePariSettings[] ExchangePairs { get; set; }
    }

    public class WashTradingExchangePariSettings
    {
        /// <summary>
        /// ExchangeId
        /// </summary>
        public string Exchange1 { get; set; }

        /// <summary>
        /// ExchangeId
        /// </summary>
        public string Exchange2 { get; set; }

        /// <summary>
        /// CurrencyCodePairs
        /// </summary>
        public string[] Symbols { get; set; }
    }

    public class LiquidityProviderSettings
    {
        public bool IsEnabled { get; set; }
        
        /// <remarks>
        /// In percents
        /// </remarks>
        public decimal AllowedMaxSpread { get; set; }
        
        public Dictionary<string, string[]> Exchanges { get; set; }
    }

    public static class Helper
    {
        public static Dictionary<string, decimal> TradeAmountSettingFix(this Dictionary<string, decimal> tradeAmount)
        {
            return tradeAmount.ToDictionary(x => x.Key.ToUpper(), x => x.Value);
        }
    }
}