﻿using System;
using Core.Bot;
using Core.Exchanges.Core;
using Core.Model;
using CryptoLp.Bot.Models.WashTreding;
using JetBrains.Annotations;
using Serilog;

namespace CryptoLp.Bot
{
    public class TradeInfoBuilder
    {
        private readonly AppSettings _appSettings;
        private readonly BalanceManager _balanceManager;

        public TradeInfoBuilder(AppSettings appSettings, BalanceManager balanceManager)
        {
            _appSettings = appSettings;
            _balanceManager = balanceManager;
        }
        
        public TradeInfo BuildCommonTradeInfo(
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            Symbol symbol,
            string fromCurrencyCode,
            string toCurrencyCode,
            PriceByOrderSide prices)
        {
            if (!_appSettings.TradeAmount.ContainsKey(symbol.BaseCurrencyCode))
            {
                return new TradeInfo($"These currencies are not specified in config {symbol}");
            }
            
//            if (transactions.Keys.Any(x => x.BuyExchangeSymbol.Equals(exchangeSymbol)) ||
//                transactions.Keys.Any(x => x.BuyExchangeSymbol.Equals(toExchangeSymbol)) ||
//                transactions.Keys.Any(x => x.SellExchangeSymbol.Equals(exchangeSymbol)) ||
//                transactions.Keys.Any(x => x.SellExchangeSymbol.Equals(toExchangeSymbol)))
//            {
//                Log.Logger.Verbose(
//                    "These ExchangeSymbols are being traded {fomExchangeSymbol} {toExchangeSymbol}",
//                    exchangeSymbol, toExchangeSymbol);
//                return new TradeInfo(false);
//            }

            if (!_balanceManager.BalanceWasReceived(exchangeAccount1.Id))
            {
                return new TradeInfo($"Balance for accountId {exchangeAccount1.Id} was not received yet");
            }

            if (!_balanceManager.BalanceWasReceived(exchangeAccount2.Id))
            {
                return new TradeInfo($"Balance for accountId {exchangeAccount2.Id} was not received yet");
            }

            var tradeInfo = CheckBidAskSpreadSize(prices);
            if (tradeInfo != null)
                return tradeInfo;
            
            var buyPriceBound = prices.BidTopTopPrice.Value;
            var sellPriceBound = prices.AskTopPrice.Value;
            
            var buyPrice = GetPrice(buyPriceBound, sellPriceBound, symbol);
            var sellPrice = buyPrice; // GetSellPrice(exchangeSymbol, sellPriceBound);

            //We use these fake prices for hardcore cancellation testing
//            buyPrice = 5000;
//            sellPrice = 9900;
            
            var tradeAmount = GetTradeAmount(symbol);

            var tradeInfoVariant1 = BuildCommonTradeInfoVariant(exchangeAccount1, exchangeAccount2, symbol, fromCurrencyCode, toCurrencyCode, buyPrice, tradeAmount, sellPrice);
            var tradeInfoVariant2 = BuildCommonTradeInfoVariant(exchangeAccount2, exchangeAccount1, symbol, fromCurrencyCode, toCurrencyCode, buyPrice, tradeAmount, sellPrice);

            if (!tradeInfoVariant1.CanBeTraded && !tradeInfoVariant2.CanBeTraded)
            {
                var errorMessage = "Can't start transaction both variants have errors:\n" +
                    $"from {exchangeAccount1} to  {exchangeAccount2} error: {tradeInfoVariant1.ErrorMessage}\n" + 
                    $"from {exchangeAccount2} to  {exchangeAccount1} error: {tradeInfoVariant1.ErrorMessage}";
                return new TradeInfo(errorMessage);
            }
            
            if (tradeInfoVariant1.CanBeTraded && !tradeInfoVariant2.CanBeTraded)
            {
                return tradeInfoVariant1;
            }
            
            if (!tradeInfoVariant1.CanBeTraded && tradeInfoVariant2.CanBeTraded)
            {
                return tradeInfoVariant2;
            }

            if (tradeInfoVariant1.CanBeTraded && tradeInfoVariant2.CanBeTraded)
            {
                return tradeInfoVariant1.BuyAmount > tradeInfoVariant2.BuyAmount ? tradeInfoVariant1 : tradeInfoVariant2;
            }
            
            throw new Exception("Unexpected execution situation");
        }

        [CanBeNull]
        private static TradeInfo CheckBidAskSpreadSize(PriceByOrderSide prices)
        {
            if (prices.AskTopPrice - prices.BidTopTopPrice > 30)
            {
                return new TradeInfo($"Bid-Ask spread is {prices.AskTopPrice - prices.BidTopTopPrice} > 30");
            }

            return null;
        }

        private TradeInfo BuildCommonTradeInfoVariant(
            Exchange buyExchangeAccount, 
            Exchange sellExchangeAccount, 
            Symbol symbol,
            string fromCurrencyCode, 
            string toCurrencyCode, 
            decimal buyPrice, 
            decimal tradeAmount, 
            decimal sellPrice)
        {
            var fromQuoteBalance = _balanceManager.GetBalance(buyExchangeAccount.Id, symbol.QuoteCurrencyCode);
            var buyAmount = fromQuoteBalance / buyPrice;
            if (tradeAmount < buyAmount)
            {
                buyAmount = tradeAmount;
            }

            var toBaseBalance = _balanceManager.GetBalance(sellExchangeAccount.Id, symbol.BaseCurrencyCode);
            var toAmount = toBaseBalance;
            if (tradeAmount < toAmount)
            {
                toAmount = tradeAmount;
            }

            var sellAmount = toAmount;
            var amount = Math.Min(buyAmount, sellAmount);

            amount = Floor(amount, symbol.AmountPrecision);
            buyAmount = amount;
            sellAmount = amount;

            //sellAmount += 0.0002m;

            if (!IsEnoughAmountAndCost(symbol, fromCurrencyCode, buyPrice, buyAmount))
            {
                return new TradeInfo(
                    $"Not enough amount or cost (exchange: {buyExchangeAccount.Id}; currency: {fromCurrencyCode}; price: {buyPrice}; amount: {buyAmount})");
            }

            if (!IsEnoughAmountAndCost(symbol, toCurrencyCode, sellPrice, sellAmount))
            {
                return new TradeInfo(
                    $"Not enough amount or cost (exchange: {sellExchangeAccount.Id}; currency: {toCurrencyCode}; price: {sellPrice}; amount: {sellAmount})");
            }

            return new TradeInfo(buyExchangeAccount, buyPrice, buyAmount, sellExchangeAccount, sellPrice, sellAmount);
        }

        public decimal GetTradeAmount(Symbol symbol)
        {
            return _appSettings.TradeAmount[symbol.BaseCurrencyCode];
        }

        public decimal GetPrice(decimal buyPriceBound, decimal sellPriceBound, Symbol symbol)
        {
            var price = (buyPriceBound + sellPriceBound) / 2;
            var pricePrecision = symbol.PricePrecision;
            return Floor(price, pricePrecision);
        }

        public bool CheckNeedRebalancing(
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            PriceByOrderSide prices,
            Symbol symbol)
        {
            var best = CalculateBestRebalancing(exchangeAccount1, exchangeAccount2, prices, symbol);

            var minTotalBalance = Math.Min(best.Balance1.TotalBalance, best.Balance2.TotalBalance);

            const decimal maxRateForRebalancing = 0.4m;

            return maxRateForRebalancing * minTotalBalance < best.RebalancingVolume;
        }

        public (TradeInfo, TradeInfo) BuildRebalancingTradeInfo(
            Exchange exchangeAccount1, 
            Exchange exchangeAccount2,
            PriceByOrderSide prices,
            Symbol symbol)
        {
            var tradeInfo = CheckBidAskSpreadSize(prices);
            if (tradeInfo != null)
                return (tradeInfo, tradeInfo);
            
            var amountPrecision = symbol.AmountPrecision;

            var rebalancing = CalculateBestRebalancing(exchangeAccount1, exchangeAccount2, prices, symbol);
            var commission = GetCommissionRate(exchangeAccount1.Name);

            var sellPrice = rebalancing.SellPrice;
            var rawSellAmount = rebalancing.QouteDelta / (1 + commission);
            var sellAmount = Floor(rawSellAmount, amountPrecision);
            var sellExchangeAccount = rebalancing.Balance1.ExchangeAccount;

            var buyPrice = rebalancing.BuyPrice;
            var rawBuyAmount = rebalancing.BaseDelta / (1 + commission);
            var buyAmount = Floor(rawBuyAmount, amountPrecision);
            var buyExchangeAccount = rebalancing.Balance2.ExchangeAccount;

            // buy
            var quoteCurrencyCode = symbol.QuoteCurrencyCode;
            var tradeInfo1 = !IsEnoughAmountAndCost(symbol, quoteCurrencyCode, buyPrice, buyAmount, needLog: false)
                ? new TradeInfo($"Not enough amount or cost (exchange: {buyExchangeAccount.Id}; currency: {quoteCurrencyCode}; price: {buyPrice}; amount: {buyAmount})")
                : new TradeInfo(buyExchangeAccount, buyPrice, buyAmount, sellExchangeAccount, 0, 0);

            // sell
            var baseCurrencyCode = symbol.BaseCurrencyCode;
            var tradeInfo2 = !IsEnoughAmountAndCost(symbol, baseCurrencyCode, sellPrice, sellAmount, needLog: false)
                ? new TradeInfo($"Not enough amount or cost (exchange: {sellExchangeAccount.Id}; currency: {baseCurrencyCode}; price: {sellPrice}; amount: {sellAmount})")
                : new TradeInfo(sellExchangeAccount, 0, 0, sellExchangeAccount, sellPrice, sellAmount);

            return (tradeInfo1, tradeInfo2);
        }

        private RebalancingVariant CalculateBestRebalancing(
            Exchange exchangeAccount1, 
            Exchange exchangeAccount2,
            PriceByOrderSide prices, 
            Symbol symbol)
        {
            var rebalancingVariant1 = CalculateRebalancingVariant(exchangeAccount1, exchangeAccount2, prices, symbol);
            var rebalancingVariant2 = CalculateRebalancingVariant(exchangeAccount2, exchangeAccount1, prices, symbol);

            var best = rebalancingVariant1.RebalancingVolume < rebalancingVariant2.RebalancingVolume
                ? rebalancingVariant1
                : rebalancingVariant2;
            
            return best;
        }

        private RebalancingVariant CalculateRebalancingVariant(
            Exchange exchangeAccount1, 
            Exchange exchangeAccount2,
            PriceByOrderSide prices,
            Symbol symbol)
        {
            // TODO do not foget calculate price as midle ask & bids to not lost balance on rebalancing
            var buyPrice = Floor(prices.AskTopPrice.Value, symbol.PricePrecision);
            var sellPrice = Floor(prices.BidTopTopPrice.Value, symbol.PricePrecision);
            
            var balance1 = GetBalances(exchangeAccount1, symbol, buyPrice);
            var balance2 = GetBalances(exchangeAccount2, symbol, buyPrice);
            
            var minTotalBalance = Math.Min(balance1.RestrictedTotalBalance, balance2.RestrictedTotalBalance);

            var quoteDelta = balance1.RestrictedQuoteBalance < minTotalBalance ? minTotalBalance - balance1.RestrictedQuoteBalance : 0m;
            var baseDelta = balance2.RestrictedBaseBalance < minTotalBalance ? minTotalBalance - balance2.RestrictedBaseBalance : 0m;

            return new RebalancingVariant(quoteDelta, baseDelta, balance1, balance2, buyPrice, sellPrice);
        }

        private class RebalancingVariant
        {
            public RebalancingVariant(decimal qouteDelta, decimal baseDelta, Balance balance1, Balance balance2, decimal buyPrice, decimal sellPrice)
            {
                QouteDelta = qouteDelta;
                BaseDelta = baseDelta;
                Balance1 = balance1;
                Balance2 = balance2;
                BuyPrice = buyPrice;
                SellPrice = sellPrice;
            }

            public decimal RebalancingVolume => QouteDelta + BaseDelta;
            public decimal QouteDelta { get; }
            public decimal BaseDelta { get; }
            public decimal BuyPrice { get; }
            public decimal SellPrice { get; }
            public Balance Balance1 { get; }
            public Balance Balance2 { get; }
        }

        private class Balance
        {
            public Balance(decimal quoteBalance, decimal baseBalance, decimal tradeAmount, Exchange exchangeAccount)
            {
                QuoteBalance = quoteBalance;
                BaseBalance = baseBalance;
                TradeAmount = tradeAmount;
                ExchangeAccount = exchangeAccount;
            }

            public decimal QuoteBalance { get; set; }
            public decimal RestrictedQuoteBalance => Math.Min(QuoteBalance, TradeAmount);
            public decimal BaseBalance { get; set; }
            public decimal RestrictedBaseBalance => Math.Min(BaseBalance, TradeAmount);
            public decimal TradeAmount { get; set; }
            public Exchange ExchangeAccount { get; }

            public decimal TotalBalance => QuoteBalance + BaseBalance;
            public decimal RestrictedTotalBalance => Math.Min(TotalBalance, TradeAmount);
        }
        
        private Balance GetBalances(Exchange exchangeAccount, Symbol symbol, decimal buyPrice)
        {
            //var tradeAmount = GetTradeAmount(symbol);
            var tradeAmount = 1.5m;
            
            var quoteBalance = _balanceManager.GetBalance(exchangeAccount.Id, symbol.QuoteCurrencyCode) / buyPrice;
            
            var baseBalance = _balanceManager.GetBalance(exchangeAccount.Id, symbol.BaseCurrencyCode);

            return new Balance(quoteBalance, baseBalance, tradeAmount, exchangeAccount);
        }
        
        public bool CheckNeedRebalancingOld(Exchange exchangeAccount, PriceByOrderSide prices, Symbol symbol)
        {
            // ReSharper disable PossibleInvalidOperationException
            var price = GetPrice(prices.BidTopTopPrice.Value, prices.AskTopPrice.Value, symbol);
            // ReSharper restore PossibleInvalidOperationException

            var quoteBalance = _balanceManager.GetBalance(exchangeAccount.Id, symbol.QuoteCurrencyCode) / price;
            var baseBalance = _balanceManager.GetBalance(exchangeAccount.Id, symbol.BaseCurrencyCode);

            const decimal maxRateForRebalancing = 0.8m;

            var result = Math.Min(quoteBalance, baseBalance) / Math.Max(quoteBalance, baseBalance) < maxRateForRebalancing;
            return result;
        }

        public TradeInfo BuildRebalancingTradeInfoOld(
            Exchange exchangeAccount, 
            PriceByOrderSide prices,
            Symbol symbol)
        {
            // ReSharper disable PossibleInvalidOperationException
            var price = GetPrice(prices.BidTopTopPrice.Value, prices.AskTopPrice.Value, symbol);
            
            // ReSharper restore PossibleInvalidOperationException

            var quoteBalance =
                _balanceManager.GetBalance(exchangeAccount.Id, symbol.QuoteCurrencyCode) / price;
            var baseBalance =
                _balanceManager.GetBalance(exchangeAccount.Id, symbol.BaseCurrencyCode);

            var commissionRate = GetCommissionRate(exchangeAccount.Name);
            var amountPrecision = symbol.AmountPrecision;
            
            var amount = Floor((quoteBalance - baseBalance) / (2 * (1 + commissionRate)), amountPrecision);

            if (amount > 0)
            {
                // need buy
                price = prices.AskTopPrice.Value;
            }
            else
            {
                // need sell
                price = prices.BidTopTopPrice.Value;
            }

            // recalculate amount 1 time more because price changed
            quoteBalance =
                _balanceManager.GetBalance(exchangeAccount.Id, symbol.QuoteCurrencyCode) /
                price;

            amount = Floor((quoteBalance - baseBalance) / (2 * (1 + commissionRate)), amountPrecision);

            if (amount > 0)
            {
                // need buy
                var quoteCurrencyCode = symbol.QuoteCurrencyCode;
                if (!IsEnoughAmountAndCost(symbol, quoteCurrencyCode, price, amount, needLog: false))
                {
                    return new TradeInfo($"Not enough amount or cost (exchange: {exchangeAccount.Id}; currency: {quoteCurrencyCode}; price: {price}; amount: {amount})");
                }
                
                return new TradeInfo(exchangeAccount, price, amount, exchangeAccount, 0, 0);
            }
            else
            {
                amount = -amount;
                // need sell
                var baseCurrencyCode = symbol.BaseCurrencyCode;
                if (!IsEnoughAmountAndCost(symbol, baseCurrencyCode, price, amount, needLog: false))
                {
                    return new TradeInfo($"Not enough amount or cost (exchange: {exchangeAccount.Id}; currency: {baseCurrencyCode}; price: {price}; amount: {amount})");
                }
                
                return new TradeInfo(exchangeAccount, 0, 0, exchangeAccount, price, amount);
            }
        }

        private decimal GetCommissionRate(string exchangeName)
        {
            var commissionPercents = _appSettings.Commissions[exchangeName];
            return 0.01m * commissionPercents;
        }

        public bool IsEnoughAmountAndCost(Symbol symbol, string currencyCode, decimal price,
            decimal amount, bool needLog = true)
        {
            if (!IsEnoughAmount(symbol, currencyCode, amount, needLog))
            {
                return false;
            }

            var buyCost = amount * price;
            if (!IsEnoughCost(symbol, currencyCode, buyCost, needLog))
            {
                return false;
            }

            return true;
        }

        private bool IsEnoughCost(Symbol symbol, string currencyCode, decimal buyCost, bool needLog = true)
        {
            if (buyCost <= symbol.MinCost)
            {
                if (needLog)
                {
                    Log.Logger.Error("Can't creater order with cost {cost} <= {minCost} of {currency}",
                        buyCost, symbol.MinCost, currencyCode);                    
                }
                
                return false;
            }

            return true;
        }

        private bool IsEnoughAmount(Symbol symbol, string currencyCode, decimal buyAmount, bool needLog = true)
        {
            if (buyAmount <= symbol.MinAmount)
            {
                if (needLog)
                {
                    Log.Logger.Error("Can't create order for amount {amount} <= {minAmount} of {currency}",
                        buyAmount, symbol.MinAmount, currencyCode);                    
                }
                
                return false;
            }

            return true;
        }

        public decimal GetSellPrice(ExchangeSymbol sellExchangeSymbol, decimal sellPriceBound)
        {
            var pricePrecision = sellExchangeSymbol.Symbol.PricePrecision;
            var sellPriceWithOffset = Floor(sellPriceBound - (decimal)Math.Pow(0.1d, pricePrecision), pricePrecision);
            return sellPriceWithOffset;
        }

        public decimal GetBuyPrice(ExchangeSymbol buyExchangeSymbol, decimal buyPriceBound)
        {
            var pricePrecision = buyExchangeSymbol.Symbol.PricePrecision;
            var buyPriceWithOffset = Floor(buyPriceBound + (decimal)Math.Pow(0.1d, pricePrecision), pricePrecision);
            return buyPriceWithOffset;
        }
        
        private static decimal Floor(decimal value, long precision)
        {
            var powPrecision = (decimal) Math.Pow(10, precision);
            return Math.Floor(value * powPrecision) / powPrecision;
        }
    }

    public class TradeInfo
    {
        public string ErrorMessage { get; set; }
        public Exchange BuyExchangeAccount { get; set; } 
        public decimal BuyPrice { get; set; }
        public decimal BuyAmount { get; set; }
        public Exchange SellExchangeAccount { get; set; }
        public decimal SellPrice { get; set; }
        public decimal SellAmount { get; set; }

        public bool CanBeTraded => string.IsNullOrEmpty(ErrorMessage);

        public TradeInfo(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
        
        public TradeInfo(Exchange buyExchangeAccount, decimal buyPrice, decimal buyAmount, Exchange sellExchangeAccount, decimal sellPrice, decimal sellAmount)
        {
            BuyExchangeAccount = buyExchangeAccount;
            BuyPrice = buyPrice;
            BuyAmount = buyAmount;
            SellExchangeAccount = sellExchangeAccount;
            SellPrice = sellPrice;
            SellAmount = sellAmount;
        }
    }
}