﻿using Core.Model;

namespace CryptoLp.Bot.Helpers
{
    public static class OrderExtensions
    {
        public static string GetOrderInfoDescription(this Order order)
        {
            var result = $"status {order.Status}";
            if (order.Status == OrderStatus.Canceled)
            {
                result += $" filled {order.FilledAmount}";
            }

            return result;
        }
    }
}