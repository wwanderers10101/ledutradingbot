﻿using System;
using JetBrains.Annotations;

namespace CryptoLp.Bot.Helpers
{
    public static class ArrayExtensions
    {
        [MustUseReturnValue]
        public static TItem MaxItem<TItem>(this TItem[] items, Func<TItem, decimal> selector)
        {
            if (items.Length == 0)
            {
                throw new ArgumentException("Collection can't be empty", nameof(items));
            }

            if (items.Length == 1)
            {
                return items[0];
            }
            
            var maxValue = selector(items[0]);
            var maxItem = items[0];
            foreach (var item in items)
            {
                var value = selector(item);

                if (maxValue < value)
                {
                    maxValue = value;
                    maxItem = item;
                }
            }

            return maxItem;
        }
        
        [MustUseReturnValue]
        public static TItem MinItem<TItem>(this TItem[] items, Func<TItem, decimal> selector)
        {
            if (items.Length == 0)
            {
                throw new ArgumentException("Collection can't be empty", nameof(items));
            }
            
            var minValue = selector(items[0]);
            var minItem = items[0];
            foreach (var item in items)
            {
                var value = selector(item);

                if (minValue > value)
                {
                    minValue = value;
                    minItem = item;
                }
            }

            return minItem;
        }
    }
}