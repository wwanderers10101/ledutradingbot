using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Bot;
using Core.Bot.LocalSnapshots;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;
using Marten;
using Microsoft.AspNetCore.NodeServices;
using Serilog;
using Telegram.Bot;

namespace CryptoLp.Bot
{
    public class BotApi : BotBase, IDisposable
    {               
        private readonly TelegramBot _telegramBot;

        [NotNull]
        private readonly Dictionary<ExchangeNameSymbol, Trades> _lastTradesBySymbol = new Dictionary<ExchangeNameSymbol, Trades>();
        
        [NotNull]
        private readonly object _tradesSyncObject = new object();

        [CanBeNull]
        private TaskCompletionSource<Trades> _tradesTcs;
        
        public BotApi(
            IDocumentStore documentStore, 
            ICollection<Exchange> exchanges, 
            AppSettings<BotSettings> appSettings,
            INodeServices nodeServices)
            : base(documentStore, exchanges, appSettings.Core, new DateTimeService())
        {
            //ToDo: Dependency injection
            _telegramBot = new TelegramBot(new TelegramBotClient(appSettings.Bot.Telegram.ApiKey));

            //ToDo: We need to intoduce a way start this thread (an other similar threads) only after the bot has started
            //Otherwise sometimes we get NullReferenceException at start

            BalanceUpdate += OnBalanceUpdate;
            CancelOrderFailed += OnCancelOrderFailed;
            Prints += OnPrints;
        }
        
                        
        [NotNull]
        public TelegramBot TelegramBot => _telegramBot;                
        
        [NotNull]
        public Dictionary<ExchangeNameSymbol, Trades> LastTradesBySymbol => _lastTradesBySymbol;
        
        private void OnPrints(Trades trades)
        {
            lock (_tradesSyncObject)
            {
                if (trades.TradeItems.IsEmpty())
                {
                    Log.Logger.Verbose("Received an empty list of trades from {exchangeId}", trades.ExchangeId);
                    return;
                }

                var symbol = GetSymbolByCurrencyPair(trades.ExchangeId, trades.CurrencyPair);
                var exchangeNameSymbol = new ExchangeNameSymbol(trades.ExchangeName, symbol.CurrencyCodePair);

                _lastTradesBySymbol[exchangeNameSymbol] = trades;
                
                var triggerTcs = _tradesTcs;

                if (triggerTcs != null && !triggerTcs.Task.IsCompleted)
                {
                    triggerTcs.SetResult(trades);
                }
            }
        }
        
        public Task<Trades> WaitTrades()
        {
            //ToDo: We should wait by exchange.Name
            return InitTaskCompletion(ref _tradesTcs, _tradesSyncObject);
        }
        
        private static Task<T> InitTaskCompletion<T>(ref TaskCompletionSource<T> tcs, object syncObj)
        {
            lock (syncObj)
            {
                var localTcs = tcs;
                if (localTcs == null || localTcs.Task.IsCompleted)
                {
                    localTcs = new TaskCompletionSource<T>();
                    tcs = localTcs;
                }

                return localTcs.Task;
            }
        }
        
        private async void OnCancelOrderFailed(Order order)
        {
            try
            {
                await ShortTimeout(ExchangesById[order.ExchangeId]);
                await CancelOrder(order);
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Error on cancel order");
            }
        }

        private void OnBalanceUpdate(string exchangeId, IDictionary<string, decimal> positions)
        {
            BalanceManager.UpdateExchangeBalance(exchangeId, positions, null);
        }
        

        
        public Task ShortTimeout(Exchange exchange)
        {
            TimeoutManager.UpdateRequestTime(exchange.Id);
            return TimeoutManager.Timeout(exchange.Id);
        }

        public Task ShortTimeout(Exchange exchange1, Exchange exchange2)
        {
            TimeoutManager.UpdateRequestTime(exchange1.Id, exchange2.Id);
            return TimeoutManager.Timeout(exchange1.Id, exchange2.Id);
        }

        public async Task BotGetBalance(Exchange exchangeAccount1, Exchange exchangeAccount2)
        {
            var balanceTask1 = exchangeAccount1.GetBalance();
            var balanceTask2 = exchangeAccount2.GetBalance();

            await balanceTask1;
            await balanceTask2;
            
            await ShortTimeout(exchangeAccount1, exchangeAccount2);
        }

        public async Task BotCancelOrder(Exchange exchangeAccount, Order order, bool needTimeout = true)
        {
            await exchangeAccount.CancelOrder(order);
            if (needTimeout)
            {
                await ShortTimeout(exchangeAccount);
            }
        }

        public async Task<bool> ReCancel(Exchange exchangeAccount, Order order)
        {
            if (order.Status == OrderStatus.FailedToCancel)
            {
                await BotCancelOrder(exchangeAccount, order, false);
                return true;
            }

            return false;
        }

        public async Task CheckOrderFilled(Exchange exchangeAccount, Order order, bool timeout = true)
        {
            if (order.Status == OrderStatus.FailedToCreate)
            {
                return;
            }

            var actualBuyOrder = await GetActualOrder(order);
            if (actualBuyOrder != null)
            {
                InnerCheckOrderFilled(order, actualBuyOrder.FilledAmount);
            }
            
            if (timeout)
            {
                await ShortTimeout(exchangeAccount);
            }                
        }

        private static void InnerCheckOrderFilled(Order order, decimal filled)
        {
            Log.Logger.Information("Check order filled {filled} orderId={orderId}", filled, order.ExchangeOrderId);
            order.FilledAmount = filled;
            if (order.Amount == filled)
            {
                order.Status = OrderStatus.Completed;
                Log.Logger.Information("Completed order {clientOrderId} {exchangeOrderId}",
                    order.ClientOrderId, order.ExchangeOrderId);
            }
        }
        
        public async Task<Order> CreateOrder(
            Exchange exchangeAccount,
            Symbol symbol,
            decimal price,
            decimal amount,
            OrderSide orderSide, 
            bool? isMaker,
            string strategy)
        {
            var order = await CreateOrder(
                exchangeAccount.Id, 
                exchangeAccount.Name, 
                symbol.CurrencyPair, 
                price, 
                amount, 
                orderSide, 
                isMaker,
                strategy);

            return order;
        }
                
        public Task<Order> CreateOrder(
            string exchangeId,
            string exchangeName,
            string currencyPair,
            decimal price,
            decimal size,
            OrderSide orderSide, 
            bool? isMaker,
            string strategy)
        {
            return CreateLimitOrder(null, exchangeId, exchangeName, currencyPair, price, size, orderSide, isMaker, strategy);
        }
        
        public void Dispose()
        {
            Prints -= OnPrints;
            BalanceUpdate -= OnBalanceUpdate;
            CancelOrderFailed -= OnCancelOrderFailed;
        }
        
        public Symbol GetSymbol([NotNull] string exchangeId, [NotNull] string currencyPair)
        {
            return ExchangesById[exchangeId].SymbolByCurrencyPair[currencyPair];
        }
    }
}