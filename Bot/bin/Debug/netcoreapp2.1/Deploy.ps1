npm install --silent

$process = Get-Process | where {$_.Name -eq 'dotnet' -and $_.modules.ModuleName -eq 'CryptoLp.Bot.dll'}
if ($process) {
    $process | Stop-Process -Force
}

Start-Sleep 5

Start-Process "dotnet" -WindowStyle Hidden -ArgumentList "CryptoLp.Bot.dll"