module.exports = async function (callback, exchangeName, apiKey, apiSecret) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();
    exchange.apiKey = apiKey;
    exchange.secret = apiSecret;
    let markets = await exchange.fetchBalance();
    callback(/* error */ null, markets);
};

//module.exports((err, result) => console.log(result), 'binance');