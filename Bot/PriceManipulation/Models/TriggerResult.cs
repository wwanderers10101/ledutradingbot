﻿using System;
using System.Threading.Tasks;
using JetBrains.Annotations;

namespace CryptoLp.Bot.PriceManipulation.Models
{
    public class TriggerResult
    {
        public TriggerResult([NotNull] Func<Task> trade)
        {
            Trade = trade;
        }

        internal Func<Task> Trade { get; }
    }
}