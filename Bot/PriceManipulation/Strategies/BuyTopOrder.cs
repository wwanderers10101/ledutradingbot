﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Bot.Events.TaskEvents;
using Core.Bot.Timeout;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using CryptoLp.Bot.PriceManipulation.Helpers;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;
using Serilog;

namespace CryptoLp.Bot.PriceManipulation.Strategies
{
    public class BuyTopOrder
    {
        public static readonly string StrategyName = "PriceManipulation/BuyTopOrder"; 
        
        private readonly AppSettings<BotSettings> _appSettings;
        private readonly BotApi _botApi;
        private readonly Exchange _exchange;
        private readonly Symbol _symbol;
        private readonly IOrderBookTaskEvents _orderBookTaskEvents;
        private readonly ITradesTaskEvents _tradesTaskEvents;

        public BuyTopOrder(
            AppSettings<BotSettings> appSettings, 
            BotApi botApi, 
            Exchange exchange,
            Symbol symbol,
            IOrderBookTaskEvents orderBookTaskEvents,
            ITradesTaskEvents tradesTaskEvents)
        {
            _appSettings = appSettings;
            _botApi = botApi;
            _exchange = exchange;
            _symbol = symbol;
            _orderBookTaskEvents = orderBookTaskEvents;
            _tradesTaskEvents = tradesTaskEvents;
        }
        
        public async Task Start()
        {
            var exchange = _exchange;
            var symbol = _symbol;
            
            var timeout = TimeSpan.FromSeconds(_appSettings.Bot.PriceManagement.BuyTopOrder.IterationTimeoutInSeconds);
            var iterationTimeoutManager = new TimeoutManager(_botApi.DateTimeService, (exchange.Id, timeout));
            
            while (true)
            {
                iterationTimeoutManager.UpdateRequestTime(exchange.Id);
                
                var topAsk = await WaitTrigger(exchange, symbol);

                if (await TradeOrder(exchange, symbol, topAsk, iterationTimeoutManager))
                {
                    continue;
                }
                
                await iterationTimeoutManager.Timeout(exchange.Id);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private async Task<ShortOrder> WaitTrigger(Exchange exchange, Symbol symbol)
        {
            {
                var exchangeNameSymbol = new ExchangeNameSymbol(exchange.Name, symbol.CurrencyCodePair);

                var lastTrades = FindLastTrade(exchangeNameSymbol);
                if (lastTrades != null)
                {
                    var topAsk = CheckBuyTopOrderTrigger(exchangeNameSymbol, lastTrades);
                    if (topAsk != null)
                    {
                        return topAsk.Value;
                    }
                }
            }

            while (true)
            {
                var orderBookUpdateTask = _orderBookTaskEvents.WaitOrderBookUpdate();
                var tradesTask = _tradesTaskEvents.WaitPrints();

                await await Task.WhenAny(orderBookUpdateTask, tradesTask);
                
                var exchangeNameSymbol = new ExchangeNameSymbol(exchange.Name, symbol.CurrencyCodePair);
                var lastTrades = FindLastTrade(exchangeNameSymbol);
                if (lastTrades == null)
                {
                    continue;
                }

                var topAsk = CheckBuyTopOrderTrigger(exchangeNameSymbol, lastTrades);
                if (topAsk != null)
                {
                    return topAsk.Value;
                }
            }
        }

        [CanBeNull]
        private Trades FindLastTrade(ExchangeNameSymbol exchangeNameSymbol)
        {
            if (!_tradesTaskEvents.TradesService.TryGetTrades(exchangeNameSymbol, out var tradesList,
                withFilterByDuration: false))
            {
                return null;
            }
            
            lock (tradesList)
            {
                return tradesList.First().Value;
            }
        }
        
        private ShortOrder? CheckBuyTopOrderTrigger(ExchangeNameSymbol exchangeNameSymbol, Trades trades)
        {
            if (trades == null || trades.TradeItems.Length == 0)
            {
                return null;
            }

            if (!_orderBookTaskEvents.LocalSnapshotService.TryGetSnapshot(exchangeNameSymbol, out var snapshot))
            {
                return null;
            }
            
            var topAsk = snapshot.GetTopAsk();
            if (topAsk == null)
            {
                return null;
            }

            //We assume the last trade is the first in the list
            var maxTradesPrice = trades.TradeItems.Select(x => x.Price).First();

            var threshold = Utils.NextDecimal(
                _appSettings.Bot.PriceManagement.BuyTopOrder.LastTradeDiffTresholdInPercentage_Min,
                _appSettings.Bot.PriceManagement.BuyTopOrder.LastTradeDiffTresholdInPercentage_Max);
            
            var topAskPrice = topAsk.Value.Price;
            if (topAskPrice - maxTradesPrice < topAskPrice * threshold * 0.01m)
            {
                return null;
            }

            // don't trade with our orders
            var topOrders = _botApi.GetTopOrders(exchangeNameSymbol.ExchangeName);
            if (topOrders.TopAsk != null && topOrders.TopAsk.Price <= topAskPrice)
            {
                return null;
            }

            return topAsk.Value;
        }

        private async Task<bool> TradeOrder(Exchange exchange, Symbol symbol, ShortOrder topAsk, TimeoutManager iterationTimeoutManager)
        {
            // TODO take into account balance from BalanceManager
            var buyAmountMin = _appSettings.Bot.PriceManagement.BuyTopOrder.Exchanges[exchange.Id][symbol.CurrencyCodePair].BuyAmount_Min;
            var buyAmountMax = _appSettings.Bot.PriceManagement.BuyTopOrder.Exchanges[exchange.Id][symbol.CurrencyCodePair].BuyAmount_Max;

            var buyAmount = Utils.Floor(Utils.NextDecimal(buyAmountMin, buyAmountMax), symbol.AmountPrecision);

            var amount = Math.Min(topAsk.Amount, buyAmount);

            //ToDo: It should be Symbol.MinAmount after #33 is done
            if (amount < buyAmountMin)
            {
                amount = buyAmountMin;
            }

            var order = await _botApi.CreateOrder(exchange, symbol, topAsk.Price, amount, OrderSide.Buy, false, StrategyName);

            if (order.Status == OrderStatus.FailedToCreate)
            {
                Log.Logger.Information(
                    $"BTO {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} failed to create");
                return true;
            }

            Log.Logger.Information(
                $"BTO {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} is created. Price {order.Price} amount {order.Amount}");

            var cts = new CancellationTokenSource();

            var waitOrderFinishTask = _botApi.WaitOrderFinish(order, cts.Token);
            var iterationTimeoutTask = iterationTimeoutManager.Timeout(exchange.Id);

            await await Task.WhenAny(waitOrderFinishTask, iterationTimeoutTask);

            cts.Cancel();
            
            if (order.Status == OrderStatus.Completed)
            {
                Log.Logger.Information($"BTO {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} is completed");
            }
            else
            {
                if (iterationTimeoutTask.IsCompletedSuccessfully)
                {
                    Log.Logger.Information($"BTO {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} was canceled by timeout");
                }
                
                await _botApi.WaitCancelOrder(order);
                Log.Logger.Information($"BTO {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} is canceled");
            }

            return false;
        }
    }
}