﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Core.Bot.Events.TaskEvents;
using Core.Bot.Timeout;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using CryptoLp.Bot.Settings;
using Serilog;

namespace CryptoLp.Bot.PriceManipulation.Strategies
{
    public class MinSpread
    {
        public static readonly string StrategyName = "PriceManipulation/MinSpread";
        
        private readonly AppSettings<BotSettings> _appSettings;
        private readonly BotApi _botApi;
        private readonly Exchange _exchange;
        private readonly Symbol _symbol;
        private readonly IOrderBookTaskEvents _orderBookTaskEvents;
        private readonly ITradesTaskEvents _tradesTaskEvents;

        public MinSpread(
            AppSettings<BotSettings> appSettings,
            BotApi botApi, 
            Exchange exchange,
            Symbol symbol,
            IOrderBookTaskEvents orderBookTaskEvents,
            ITradesTaskEvents tradesTaskEvents)
        {
            _appSettings = appSettings;
            _botApi = botApi;
            _exchange = exchange;
            _symbol = symbol;
            _orderBookTaskEvents = orderBookTaskEvents;
            _tradesTaskEvents = tradesTaskEvents;
        }
        
        public async Task Start()
        {
            var exchange = _exchange;
            var symbol = _symbol;
                
            var timeout = TimeSpan.FromSeconds(_appSettings.Bot.PriceManagement.MinSpread.IterationTimeoutInSeconds);
            var iterationTimeoutManager = new TimeoutManager(_botApi.DateTimeService, (exchange.Id, timeout));
            
            while (true)
            {
                iterationTimeoutManager.UpdateRequestTime(exchange.Id);
                
                var triggerResult = await WaitTrigger(exchange, symbol);

                if (await TradeOrder(exchange, symbol, triggerResult, iterationTimeoutManager))
                {
                    continue;
                }
                
                await iterationTimeoutManager.Timeout(exchange.Id);
            }
            // ReSharper disable once FunctionNeverReturns
        }

        private async Task<MinSpreadTriggerCheckResult> WaitTrigger(Exchange exchange, Symbol symbol)
        {
            while (true)
            {
                await _orderBookTaskEvents.WaitOrderBookUpdate();

                var spreadThreshold = Utils.NextDecimal(
                    _appSettings.Bot.PriceManagement.MinSpread.SpreadTresholdInPercentage_Min,
                    _appSettings.Bot.PriceManagement.MinSpread.SpreadTresholdInPercentage_Max);
                
                var triggerResult = CheckMinSpreadTrigger(exchange, symbol, spreadThreshold);
                if (triggerResult.IsSuccessed)
                {
                    return triggerResult;
                }
            }
        }

        private MinSpreadTriggerCheckResult CheckMinSpreadTrigger(Exchange exchange, Symbol symbol, decimal spreadTreshold)
        {
            var exchangeSymbol = new ExchangeIdSymbol(exchange.Id, exchange.Name, symbol);
            if (!_orderBookTaskEvents.LocalSnapshotService.TryGetSnapshot(exchangeSymbol.ExchangeNameSymbol, out var localOrderBookSnapshot))
            {
                return MinSpreadTriggerCheckResult.NotSucceded();
            }
                
            var topAsk = localOrderBookSnapshot.GetTopAsk();
            if (topAsk == null)
            {
                return MinSpreadTriggerCheckResult.NotSucceded();
            }

            var topBid = localOrderBookSnapshot.GetTopBid();
            if (topBid == null)
            {
                return MinSpreadTriggerCheckResult.NotSucceded();
            }
            
            var bidAskSpread = (topAsk.Value.Price - topBid.Value.Price) / topAsk.Value.Price * 100m;
            if (bidAskSpread < spreadTreshold)
            {
                return MinSpreadTriggerCheckResult.NotSucceded();
            }

            return new MinSpreadTriggerCheckResult(true, bidAskSpread, spreadTreshold, topBid.Value);
        }

        public class MinSpreadTriggerCheckResult
        {
            public MinSpreadTriggerCheckResult(bool isSuccessed, decimal bidAskSpread, decimal spreadTreshold, ShortOrder topBid)
            {
                IsSuccessed = isSuccessed;
                BidAskSpread = bidAskSpread;
                SpreadTreshold = spreadTreshold;
                TopBid = topBid;
            }

            public bool IsSuccessed { get; }
            public decimal BidAskSpread { get; }
            public decimal SpreadTreshold { get; }
            public ShortOrder TopBid { get; }

            public static MinSpreadTriggerCheckResult NotSucceded()
            {
                return new MinSpreadTriggerCheckResult(false, 0m, 0m, default);
            }
        }

        private async Task<bool> TradeOrder(
            Exchange exchange, Symbol symbol, MinSpreadTriggerCheckResult triggerResult, TimeoutManager iterationTimeoutManager)
        {
            Log.Information(
                "MS {exchangeId}:{currencyPair} Bid-Ask spread is {bidAskSpread}% >= {spreadThreshold}%",
                exchange.Id, symbol.CurrencyPair, triggerResult.BidAskSpread, triggerResult.SpreadTreshold);

            var buyAmountMin = _appSettings.Bot.PriceManagement.MinSpread.Exchanges[exchange.Id][symbol.CurrencyCodePair].BuyAmount_Min;
            var buyAmountMax = _appSettings.Bot.PriceManagement.MinSpread.Exchanges[exchange.Id][symbol.CurrencyCodePair].BuyAmount_Max;

            var buyAmount = Utils.Floor(Utils.NextDecimal(buyAmountMin, buyAmountMax), symbol.AmountPrecision);

            var priceTick = (decimal) Math.Pow(10, -symbol.PricePrecision);
            var topBidPrice = triggerResult.TopBid.Price;
            var priceIncrease = Math.Max(_appSettings.Bot.PriceManagement.MinSpread.PriceIncreaseInPercentage * 0.01m * topBidPrice, priceTick);
            var price = Utils.Floor(topBidPrice + priceIncrease, symbol.PricePrecision);

//            if (!_balanceManager.BalanceWasReceived(exchange.Id))
//            {
//                Log.Logger.Information($"MS {exchange.Id}:{symbol.CurrencyPair} balance wasn't received");
//                return;
//            }
//
//            if (_balanceManager.Reserve(exchange.Id, symbol, OrderSide.Buy, price, buyAmount))
//            {
//                Log.Logger.Information($"MS {exchange.Id}:{symbol.CurrencyPair} not enough balance");
//                return;
//            }

            var order = await _botApi.CreateOrder(exchange, symbol, price, buyAmount, OrderSide.Buy, true, StrategyName);

            if (order.Status == OrderStatus.FailedToCreate)
            {
                Log.Logger.Information(
                    $"MS {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} failed to create");
                return true;
            }

            Log.Logger.Information(
                $"MS {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} is created. Price {order.Price} amount {order.Amount}");

            var cts = new CancellationTokenSource();

            var waitOrderFinishTask = _botApi.WaitOrderFinish(order, cts.Token);
            var cancellationTask = IfNeedCancellation(triggerResult.SpreadTreshold, order, exchange, symbol, cts.Token);
            var iterationTimeoutTask = iterationTimeoutManager.Timeout(exchange.Id);

            await await Task.WhenAny(waitOrderFinishTask, iterationTimeoutTask, cancellationTask);

            cts.Cancel();

            if (order.Status == OrderStatus.Completed)
            {
                //var actualOrder = await GetActualOrder(order);
                //OrderWasFilled(exchange, order.Side, actualOrder.Price.Value, actualOrder.FilledAmount);
                Log.Logger.Information(
                    $"MS {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} is completed");
            }
            else
            {
                // TODO need take into account partionally filled order in BalanceManager
                await _botApi.WaitCancelOrder(order);

                if (iterationTimeoutTask.IsCompletedSuccessfully)
                {
                    Log.Logger.Verbose(
                        $"MS {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} was cancelled by timeout");
                }

                Log.Logger.Information(
                    $"MS {exchange.Id}:{symbol.CurrencyPair} order {order.ExchangeOrderId} is canceled");
            }
            
            //_balanceManager.UnReserve(exchange.Id, symbol, order.Side, order.Price.Value, order.Amount);

            return false;
        }

        private async Task IfNeedCancellation(decimal spreadTreshold, Order order, Exchange exchange,
            Symbol symbol, CancellationToken cancellationToken)
        {
            while (true)
            {            
                var waitNextOrderBookUpdateTask = _orderBookTaskEvents.WaitOrderBookUpdate();
                var waitNextTradesTask = _tradesTaskEvents.WaitPrints();
                
                await await Task.WhenAny(waitNextOrderBookUpdateTask, waitNextTradesTask);

                if (cancellationToken.IsCancellationRequested)
                    return;

                var exchangeSymbol = new ExchangeIdSymbol(exchange.Id, exchange.Name, symbol);
                if (!_orderBookTaskEvents.LocalSnapshotService.TryGetSnapshot(exchangeSymbol.ExchangeNameSymbol, out var snapshot))
                {
                    continue;
                }
                
                var topAsk = snapshot.GetTopAsk();
                if (topAsk == null)
                {
                    continue;
                }

                var topBid = snapshot.GetTopBid();
                if (topBid == null)
                {
                    continue;
                }

                if (topBid.Value.Price > order.Price.Value)
                {
                    Log.Logger.Verbose($"MS {exchange.Id}:{symbol.CurrencyPair} order not in the top of bids (price: {topBid.Value.Price}, amount: {topBid.Value.Amount}");
                    return;
                }
                
                //ToDo: Incapsulate as it's used twice
                if (topBid.Value.Price == order.Price && topBid.Value.Amount > order.Amount)
                {
                    Log.Logger.Verbose($"MS {exchange.Id}:{symbol.CurrencyPair} other order is also at the top (price: {topBid.Value.Price}, amount: {topBid.Value.Amount}");
                    return;
                }
                
                var nextBidPrice = NextBidPrice(order, topBid.Value, snapshot);
            
                var bidAskSpread = (topAsk.Value.Price - nextBidPrice) / topAsk.Value.Price * 100m;
                if (bidAskSpread < spreadTreshold)
                {
                    Log.Logger.Verbose($"MS {exchange.Id}:{symbol.CurrencyPair} bid-ask spread {bidAskSpread} < {spreadTreshold} spread treshold");
                    return;
                }
            }
        }

        private static decimal NextBidPrice(Order order, ShortOrder topBid, LocalOrderBookSnapshot snapshot)
        {
            // TODO need refresh order and check by order.FilledAmount
            if (topBid.Price == order.Price &&
                topBid.Amount > order.Amount)
            {
                return topBid.Price;
            }

            var secondBid = snapshot.GetBidAt(1);
            if (secondBid != null)
            {
                return secondBid.Value.Price;
            }
            
            return 0m;
        }
    }
}