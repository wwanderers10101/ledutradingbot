using System.Linq;
using Core.Bot;
using Core.Model;
using Core.Model.Enums;
using JetBrains.Annotations;

namespace CryptoLp.Bot.PriceManipulation.Helpers
{
    public static class OrdersHelper
    {
        public static TopOrders GetTopOrders([NotNull] this BotBase bot, [NotNull] string exchangeName)
        {
            var exchangeIds = bot.ExchangesById.Values
                .Where(x => x.Name == exchangeName)
                .Select(x => x.Id)
                .ToList();
          
            var ordersByClientOrderId = bot.OrdersByClientOrderId;

            Order minAsk = null;
            Order maxBid = null;
            
            foreach (var exchangeId in exchangeIds)
            {
                var ordersDict = ordersByClientOrderId[exchangeId];
                foreach (var (_, order) in ordersDict)
                {
                    switch (order.Side)
                    {
                        case OrderSide.Buy:
                            if (maxBid == null || maxBid.Price < order.Price)
                            {
                                maxBid = order;
                            }
                            break;
                        case OrderSide.Sell:
                            if (minAsk == null || minAsk.Price > order.Price)
                            {
                                minAsk = order;
                            }
                            break;
                        // ReSharper disable once RedundantCaseLabel
                        case OrderSide.Unknown:
                        default:
                        {
                            // Shouldn't do anything
                            break;
                        }
                    }
                }
            }
            
            return new TopOrders(minAsk, maxBid);
        }
    }
}