using Core.Model;
using JetBrains.Annotations;

namespace CryptoLp.Bot.PriceManipulation.Helpers
{
    public struct TopOrders
    {
        public TopOrders([CanBeNull] Order topAsk, [CanBeNull] Order topBid)
        {
            TopAsk = topAsk;
            TopBid = topBid;
        }

        [CanBeNull] 
        public Order TopAsk { get; }
            
        [CanBeNull] 
        public Order TopBid { get; }
    }
}