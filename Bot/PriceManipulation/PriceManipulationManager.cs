﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Bot.Events;
using Core.Bot.Events.TaskEvents;
using Core.Exchanges.Core;
using Core.Model;
using CryptoLp.Bot.Common;
using CryptoLp.Bot.PriceManipulation.Strategies;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;

namespace CryptoLp.Bot.PriceManipulation
{
    public class PriceManipulationManager: IStrategyManager
    {
        private readonly List<BuyTopOrder> _buyTopOrderStrategies = new List<BuyTopOrder>();
        private readonly List<MinSpread> _minSpreadStrategies = new List<MinSpread>();
        
        [UsedImplicitly]
        private Task _eventsLoopTask;

        private readonly OrderBookTaskEvents _orderBookTaskEvents;
        private readonly TradesTaskEvents _tradesTaskEvents;

        public PriceManipulationManager(
            AppSettings<BotSettings> appSettings,
            BotApi botApi)
        {
            if (appSettings.Bot?.PriceManagement?.IsEnabled != true)
            {
                return;
            }

            var priceManagementSettings = appSettings.Bot.PriceManagement;
            if (priceManagementSettings?.MinSpread?.Exchanges == null &&
                priceManagementSettings?.BuyTopOrder?.Exchanges == null)
            {
                throw new Exception("Can't start price manipulation. You need specify exchanges for BuyTopOrder or MinSpread");
            }

            var (eventsLoop, eventsLoopTask) = ExchangeEventsLoop.Run(botApi.CreateEventChannel());
            _eventsLoopTask = eventsLoopTask;
            _orderBookTaskEvents = new OrderBookTaskEvents(eventsLoop, botApi.ExchangesById);
            _tradesTaskEvents = new TradesTaskEvents(eventsLoop, botApi.DateTimeService, botApi.ExchangesById.Values.ToList(), TimeSpan.Zero);
            
            var exchangeList = botApi.ExchangesById.Values
                .Where(x => x.IsTrading)
                .ToArray();

            var exchangesByExchangeIdAndCurrencyCodePair = exchangeList
                .SelectMany(x => x.Symbols, (exchange, symbol) => (exchange, symbol))
                .ToDictionary(x => (x.exchange.Id, x.symbol.CurrencyCodePair));

            _buyTopOrderStrategies = CreateStrategiesByConfig(
                exchangesByExchangeIdAndCurrencyCodePair,
                priceManagementSettings.BuyTopOrder?.Exchanges,
                exchangeSymbol => new BuyTopOrder(
                    appSettings,
                    botApi,
                    exchangeSymbol.exchange, 
                    exchangeSymbol.symbol,
                    _orderBookTaskEvents, 
                    _tradesTaskEvents));

            _minSpreadStrategies = CreateStrategiesByConfig(
                exchangesByExchangeIdAndCurrencyCodePair,
                priceManagementSettings.MinSpread?.Exchanges,
                exchangeSymbol => new MinSpread(
                    appSettings, 
                    botApi, 
                    exchangeSymbol.exchange, 
                    exchangeSymbol.symbol,
                    _orderBookTaskEvents, 
                    _tradesTaskEvents));
        }

        private static List<T> CreateStrategiesByConfig<T>(
            Dictionary<(string Id, string CurrencyCodePair), (Exchange exchange, Symbol symbol)> exchangesByExchangeIdAndCurrencyCodePair, 
            [CanBeNull] Dictionary<string, Dictionary<string, PriceManagementExchangeSettings>> exchangesConfig,
            Func<(Exchange exchange, Symbol symbol), T> createStrategy)
        {
            if (exchangesConfig == null)
            {
                return new List<T>();
            }
            
            var strategies = new List<T>();
            foreach (var (exchangeId, symbols) in exchangesConfig)
            {
                foreach (var (currencyCodePair, _) in symbols)
                {
                    if (!exchangesByExchangeIdAndCurrencyCodePair.TryGetValue((exchangeId, currencyCodePair),
                        out var exchangeSymbol))
                    {
                        throw new Exception($"Can't find initialized ExchangeId:CurrencyCodePair {exchangeId}:{currencyCodePair}");
                    }

                    var strategy = createStrategy(exchangeSymbol);
                    strategies.Add(strategy);
                }
            }

            return strategies;
        }

        public Task Start()
        {
            var tasks = new List<Task>();
            tasks.AddRange(_buyTopOrderStrategies.Select(x => Task.Run(x.Start)));
            tasks.AddRange(_minSpreadStrategies.Select(x => Task.Run(x.Start)));

            if (tasks.Count == 0)
            {
                return null;
            }
            
            return InnerStart(tasks);
        }

        private static async Task InnerStart(List<Task> tasks)
        {
            await await Task.WhenAny(tasks);

            throw new Exception("Happend something wrong");
        }

        public void Dispose()
        {
            _orderBookTaskEvents.Dispose();
            _tradesTaskEvents.Dispose();
        }
    }
}