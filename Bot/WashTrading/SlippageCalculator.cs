﻿using System;
using System.Collections.Generic;
using Core.Model;
using CryptoLp.Bot.WashTrading.Models;
using JetBrains.Annotations;

namespace CryptoLp.Bot.WashTrading
{
    public class SlippageCalculator
    {
        private readonly BotApi _botApi;

        // TODO синхронизировать обращения к _history из разных потоков
        [NotNull]
        private readonly LinkedList<CommonTransaction> _history = new LinkedList<CommonTransaction>();

        private PriceByOrderSide _previousPrices;
        private DateTime? _previousPricesDateTime;
        
        public SlippageCalculator(
            BotApi botApi)
        {
            _botApi = botApi;
        }

        public SlippageStatistics CalculateSlippage(CommonTransaction commonTransaction)
        {
            var commonTransactionSlippage = commonTransaction.Slippage;
            var transactionMaxFilled = commonTransaction.MaxFilled;
            var transactionSlippagePercent = (transactionMaxFilled == 0 ? 0 : commonTransactionSlippage / transactionMaxFilled) * 100;

            var now = _botApi.DateTimeService.UtcNow;
            var timeBound = now - TimeSpan.FromHours(1);

            var totalSlippageSum = 0m;
            var totalMaxFilledSum = 0m;
            var lastHourSlippageSum = 0m;
            var lastHourMaxFilledSum = 0m;
            var lastHourCount = 0;
            foreach (var transaction in _history)
            {
                var slippage = transaction.Slippage;
                totalSlippageSum += slippage;
                totalMaxFilledSum += transaction.MaxFilled;

                if (timeBound <= transaction.CreationDate)
                {
                    lastHourCount++;
                    lastHourSlippageSum += slippage;
                    lastHourMaxFilledSum += transaction.MaxFilled;
                }
            }

            var lastHourAvrSlippage = lastHourSlippageSum / lastHourCount;
            var totalAvrSlippage = totalSlippageSum / _history.Count;
            
            var lastHourSlippagePercent = (lastHourMaxFilledSum == 0 ? 0 : lastHourSlippageSum / lastHourMaxFilledSum) * 100;
            var totalSlippagePercent =  (totalMaxFilledSum == 0 ? 0 : totalSlippageSum / totalMaxFilledSum) * 100;

            var totalSlippageDeviationSum = 0m; 
            var lastHourSlippageDeviationSum = 0m;
            foreach (var transaction in _history)
            {
                var slippage = transaction.Slippage;
                var totalDiff = slippage - totalAvrSlippage;
                var totalDiff2 = totalDiff * totalDiff;
                totalSlippageDeviationSum += totalDiff2;
                
                if (timeBound <= transaction.CreationDate)
                {
                    var lastHourDiff = slippage - totalAvrSlippage;
                    var lastHourDiff2 = lastHourDiff * lastHourDiff;
                    lastHourSlippageDeviationSum += lastHourDiff2;
                }
            }

            var lastHourSlippageDeviation = (decimal)Math.Sqrt((double)(lastHourSlippageDeviationSum / lastHourCount));
            var totalSlippageDeviation = (decimal) Math.Sqrt((double) (totalSlippageDeviationSum / _history.Count));

            return new SlippageStatistics(
                transactionSlippagePercent,
                lastHourSlippagePercent,
                lastHourAvrSlippage,
                lastHourSlippageDeviation,
                lastHourCount,
                totalSlippagePercent,
                totalAvrSlippage,
                totalSlippageDeviation,
                _history.Count);
        }
    }
}