﻿using System;
using Core.Exchanges.Core;
using Core.Model;
using JetBrains.Annotations;

namespace CryptoLp.Bot.WashTrading.Models
{
    public class CommonTransaction
    {
        private decimal? _slippage;
        private decimal? _maxFilled;
        
        public CommonTransaction(
            [NotNull] Exchange buyExchangeAccount, 
            [NotNull] Order buyOrder,
            [NotNull] Exchange sellExchangeAccount, 
            [NotNull] Order sellOrder, 
            DateTime creationDate)
        {
            BuyOrder = buyOrder;
            SellOrder = sellOrder;
            CreationDate = creationDate;
            BuyExchangeAccount = buyExchangeAccount;
            SellExchangeAccount = sellExchangeAccount;
        }

        public DateTime CreationDate { get; set; }

        [NotNull]
        public Exchange BuyExchangeAccount { get; set; }
        
        [NotNull]
        public Order BuyOrder { get; set; }

        [NotNull]
        public Exchange SellExchangeAccount { get; set; }

        [NotNull]
        public Order SellOrder { get; set; }
        
        public int IterationsCount { get; set; }

        public decimal Slippage
        {
            get
            {
                if (_slippage == null)
                {
                    CalcSlippage();
                }

                // ReSharper disable once PossibleInvalidOperationException
                return _slippage.Value;
            }
        }

        public decimal MaxFilled
        {
            get
            {
                if (_maxFilled == null)
                {
                    CalcSlippage();
                }

                // ReSharper disable once PossibleInvalidOperationException
                return _maxFilled.Value;
            }
        }

        private void CalcSlippage()
        {
            var buyOrderFilledAmount = BuyOrder.FilledAmount;
            var sellOrderFilledAmount = SellOrder.FilledAmount;

            var maxFilled = Math.Max(buyOrderFilledAmount, sellOrderFilledAmount);
            _maxFilled = maxFilled;
            
            var slippage = maxFilled - Math.Min(buyOrderFilledAmount, sellOrderFilledAmount);
            _slippage = slippage;
        }
    }
}