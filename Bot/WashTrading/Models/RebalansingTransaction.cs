﻿using Core.Exchanges.Core;
using Core.Model;
using JetBrains.Annotations;

namespace CryptoLp.Bot.WashTrading.Models
{
    public class RebalansingTransaction
    {
        public RebalansingTransaction([NotNull] Order order, [NotNull] Exchange exchangeAccount)
        {
            Order = order;
            ExchangeAccount = exchangeAccount;
        }

        [NotNull]
        public Order Order { get; }
        
        [NotNull] 
        public Exchange ExchangeAccount { get; set; }
    }
}