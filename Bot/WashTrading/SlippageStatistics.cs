﻿namespace CryptoLp.Bot.WashTrading
{
    public class SlippageStatistics
    {
        public SlippageStatistics(decimal transactionSlippagePercent,
            decimal lastHourSlippagePercent,
            decimal lastHourAvrSlippage,
            decimal lastHourSlippageDeviation,
            decimal lastHourTransactionsCount,
            decimal totalSlippagePercent,
            decimal totalAvrSlippage,
            decimal totalSlippageDeviation,
            decimal totalTransactionsCount)
        {
            TransactionSlippagePercent = transactionSlippagePercent;
            LastHourSlippagePercent = lastHourSlippagePercent;
            LastHourAvrSlippage = lastHourAvrSlippage;
            LastHourSlippageDeviation = lastHourSlippageDeviation;
            LastHourTransactionsCount = lastHourTransactionsCount;
            TotalSlippagePercent = totalSlippagePercent;
            TotalAvrSlippage = totalAvrSlippage;
            TotalSlippageDeviation = totalSlippageDeviation;
            TotalTransactionsCount = totalTransactionsCount;
        }

        public decimal TransactionSlippagePercent { get; set; }
        public decimal LastHourSlippagePercent { get; set; }
        public decimal LastHourAvrSlippage { get; }
        public decimal LastHourSlippageDeviation { get; set; }
        public decimal LastHourTransactionsCount { get; set; }
        public decimal TotalSlippagePercent { get; set; }
        public decimal TotalAvrSlippage { get; }
        public decimal TotalSlippageDeviation { get; set; }
        public decimal TotalTransactionsCount { get; set; }

        public string GetReport()
        {
            var results = $"Transaction slippage: {TransactionSlippagePercent:N3}%\t\n" +
                          $"Last hour slippage: {LastHourSlippagePercent:N3}% (avr: {LastHourAvrSlippage} dev: {LastHourSlippageDeviation} count: {LastHourTransactionsCount})\t\n" +
                          $"Total slippage: {TotalSlippagePercent:N3}% (avr: {TotalAvrSlippage} dev: {TotalSlippageDeviation} count: {TotalTransactionsCount})";
                    
            return results;
        }
    }
}