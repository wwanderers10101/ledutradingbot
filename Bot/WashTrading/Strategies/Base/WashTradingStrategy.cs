using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Bot.Timeout;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using CryptoLp.Bot.Helpers;
using CryptoLp.Bot.Settings;
using CryptoLp.Bot.WashTrading.Models;
using Serilog;

namespace CryptoLp.Bot.WashTrading.Strategies.Base
{
    public abstract class WashTradingStrategy
    {
        private const int RetriesMaxCount = 3;

        private readonly AppSettings<BotSettings> _appSettings;
        private readonly BotApi _botApi;

        private readonly TimeoutManager _createOrdersTimeoutManager;
        private readonly TradeInfoBuilder _tradeInfoBuilder;

        private readonly Symbol _symbol;
        private readonly Exchange _exchange1;
        private readonly Exchange _exchange2;
        private readonly decimal _minAmount;
        private readonly decimal _maxAmount;

        private long _totalTransactionsCount;
        
        public WashTradingStrategy(
            AppSettings<BotSettings> appSettings,
            BotApi botApi, 
            Symbol symbol,
            Exchange exchange1,
            Exchange exchange2,
            decimal minAmount, decimal maxAmount)
        {
            _appSettings = appSettings;
            _botApi = botApi;

            _tradeInfoBuilder = new TradeInfoBuilder(appSettings, botApi.BalanceManager);
            
            _createOrdersTimeoutManager = new TimeoutManager(
                botApi.DateTimeService, 
                (exchange1.Id, TimeSpan.FromMilliseconds(appSettings.Core.Timeout[exchange1.Id])), 
                (exchange2.Id,  TimeSpan.FromMilliseconds(appSettings.Core.Timeout[exchange2.Id])));

            _symbol = symbol;
            _exchange1 = exchange1;
            _exchange2 = exchange2;
            _minAmount = minAmount;
            _maxAmount = maxAmount;
        }
        
        public abstract string StrategyName { get; }

        public abstract Task Trigger();

        public Task Start()
        {
            return Task.Run(async () =>
            {
                var exchangeAccount1 = _exchange1;
                var exchangeAccount2 = _exchange2;
                
                while (!StartCondition(exchangeAccount1, exchangeAccount2))
                {
                    await Task.Delay(50);
                }

                var transactionStatistics = new TransactionStatistics();

                while (true)
                {
                    await Trigger();
                    
                    await ProcessTransaction(exchangeAccount1, exchangeAccount2, transactionStatistics);
                
                    var totalTransactionsCount = Interlocked.Increment(ref _totalTransactionsCount);
                    Log.Logger.Information($"{exchangeAccount1.Id}, {exchangeAccount2.Id}: Total transaction count: {totalTransactionsCount}");
                    Log.Logger.Information($"{exchangeAccount1.Id}, {exchangeAccount2.Id}: transaction count without GetBalance: {transactionStatistics.TransactionsCountAfterLastGetBalance}");

                    if (transactionStatistics.TransactionsCountAfterLastGetBalance >= 30)
                    {
                        await _botApi.BotGetBalance(exchangeAccount1, exchangeAccount2);

                        transactionStatistics.TransactionsCountAfterLastGetBalance = 0;
                    }

                    await _createOrdersTimeoutManager.Timeout(exchangeAccount1.Id, exchangeAccount2.Id);
                }
                // ReSharper disable once FunctionNeverReturns
            });
        }

        private bool StartCondition(Exchange exchangeAccount1, Exchange exchangeAccount2)
        {           
            //var prices = _localSnapshot.CalculatePrice();
            return _botApi.BalanceManager.BalanceWasReceived(exchangeAccount1.Id) &&
                   _botApi.BalanceManager.BalanceWasReceived(exchangeAccount2.Id);
        }

        private async Task ProcessTransaction(
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            TransactionStatistics transactionStatistics)
        {
            // ReSharper disable once PossibleNullReferenceException
            var prices = await GetPrices(exchangeAccount1, exchangeAccount2);
            if (!prices.TopBid.HasValue || !prices.TopAsk.HasValue)
            {
                Log.Error("In snapshot there is no buy prices or sell price");
                await _botApi.ShortTimeout(exchangeAccount1, exchangeAccount2);
                return;
            }

//            var needRebalancing = _tradeInfoBuilder.CheckNeedRebalancing(exchangeAccount1, exchangeAccount2, prices, _symbol);
//            if (needRebalancing)
//            {
//                await ProcessRebalancingTrade(prices, exchangeAccount1, exchangeAccount2, transactionStatistics);
//            }
//            else
//            {
                await ProcessCommonTrade(prices, exchangeAccount1, exchangeAccount2, transactionStatistics);
//            }
        }

        private async Task ProcessCommonTrade(
            PriceByOrderSide prices,
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            TransactionStatistics transactionStatistics)
        {
            var tradeInfo = _tradeInfoBuilder.BuildWashTradeInfo(
                exchangeAccount1,
                exchangeAccount2,
                _symbol,
                _symbol.QuoteCurrencyCode,
                _symbol.BaseCurrencyCode,
                prices, _minAmount, _maxAmount);

            if (!tradeInfo.CanBeTraded)
            {
                Log.Logger.Verbose(tradeInfo.ErrorMessage);
                //_telegramBot.SendTextMessage(_appSettings.Telegram.ChatId, tradeInfo.ErrorMessage);

                await _botApi.ShortTimeout(exchangeAccount1, exchangeAccount2);
                return;
            }

            var commonTransaction = await StartCommonTransaction(tradeInfo, prices);

            while (true)
            {
                await CheckFillOrCancelOrders(commonTransaction);
                await ReCancelOrdersIfFailedToCancel(commonTransaction);
                if (await CheckIfTransactionFinished(commonTransaction))
                {
                    break;
                }

                commonTransaction.IterationsCount += 1;
            }

            transactionStatistics.TransactionsCountAfterLastGetBalance++;
        }

        private async Task<PriceByOrderSide> GetPrices(Exchange exchangeAccount1, Exchange exchangeAccount2)
        {
            // TODO possible we need pass _symbol to method explisitly
            var exchangeSymbol = new ExchangeIdSymbol(exchangeAccount1.Id, exchangeAccount1.Name, _symbol);
            while (true)
            {
                if (_botApi.OrderBookTaskEvents.LocalSnapshotService.TryGetSnapshot(exchangeSymbol.ExchangeNameSymbol, out var snapshot))
                {
                    return snapshot.CalculatePrice();
                }
                
                await Task.Delay(50);
            }
        }
        
        private static (Order buy, Order sell) GetTransactionOrders(CommonTransaction commonTransaction)
        {
            var buyOrder = commonTransaction.BuyOrder;
            var sellOrder = commonTransaction.SellOrder;
            return (buyOrder, sellOrder);
        }
        
        private async Task<CommonTransaction> StartCommonTransaction(TradeInfo tradeInfo, PriceByOrderSide prices)
        {
            var buyPrice = tradeInfo.BuyPrice;
            var buyAmount = tradeInfo.BuyAmount;
            var sellPrice = tradeInfo.SellPrice;
            var sellAmount = tradeInfo.SellAmount;

            var buyExchangeAccount = tradeInfo.BuyExchangeAccount;
            var sellExchangeAccount = tradeInfo.SellExchangeAccount;
            
            var task1 = _botApi.CreateOrder(buyExchangeAccount, _symbol, buyPrice, buyAmount, OrderSide.Buy, true, StrategyName);
            //await Task.Delay(5000);
            var task2 = _botApi.CreateOrder(sellExchangeAccount, _symbol, sellPrice, sellAmount, OrderSide.Sell, true, StrategyName);

            var buyOrder = await task1;
            var sellOrder = await task2;

            _createOrdersTimeoutManager.UpdateRequestTime(buyExchangeAccount.Id, sellExchangeAccount.Id);

            var transaction = new CommonTransaction(buyExchangeAccount, buyOrder, sellExchangeAccount, sellOrder, _botApi.DateTimeService.UtcNow);

            var snapshotPrices = GetSnapshotPrices(prices);

            var message =
                "Common transaction started\n" +
                $"Buy Price: {buyPrice}\tAmount: {buyAmount}\tAccountId: {buyExchangeAccount.Id}\n" +
                $"Sell Price: {sellPrice}\tAmount: {sellAmount}\tAccountId: {sellExchangeAccount.Id}\n" +
                snapshotPrices;
            
            Log.Logger.Verbose(message);
            //_botApi.TelegramBot.SendTextMessage(_appSettings.Bot.Telegram.ChatId, message);
            
            await _botApi.ShortTimeout(buyExchangeAccount, sellExchangeAccount);

            return transaction;
        }
        
        private async Task<bool> CheckIfTransactionFinished(CommonTransaction commonTransaction)
        {           
            var orders = GetTransactionOrders(commonTransaction);
            var buyOrder = orders.buy;
            var sellOrder = orders.sell;
            if (buyOrder.IsFinished && sellOrder.IsFinished)
            {
                // TODO decide what to do with slippage
//                _history.AddLast(commonTransaction);
//                var slippageStatistics = CalculateSlippage(commonTransaction);
//                var slippageReport = slippageStatistics.GetReport();

                var logMessage = "Transaction finished\n" +
                                 $"buy  order {buyOrder.GetOrderInfoDescription()} id= {buyOrder.ExchangeOrderId} AccountId: {commonTransaction.BuyExchangeAccount.Id}\n" +
                                 $"sell order {sellOrder.GetOrderInfoDescription()} id= {sellOrder.ExchangeOrderId} AccountId: {commonTransaction.SellExchangeAccount.Id}";
                Log.Logger.Verbose(logMessage);

                var teleMessage = "Transaction finished\n" +
                                  $"buy  order {buyOrder.GetOrderInfoDescription()}\n" +
                                  $"sell order {sellOrder.GetOrderInfoDescription()}";
                //_botApi.TelegramBot.SendTextMessage(_appSettings.Bot.Telegram.ChatId, teleMessage);

                var buyOrderFinishTask = Task.CompletedTask;
                if (!buyOrder.IsFinished)
                {
                    buyOrderFinishTask = _botApi.WaitOrderFinish(buyOrder, CancellationToken.None);
                }

                var sellOrderFinishTask = Task.CompletedTask;
                if (!sellOrder.IsFinished)
                {
                    sellOrderFinishTask = _botApi.WaitOrderFinish(sellOrder, CancellationToken.None);
                }
                
                await buyOrderFinishTask;
                await sellOrderFinishTask;
                
                _botApi.BalanceManager.OrderWasFinished(buyOrder, _symbol);
                _botApi.BalanceManager.OrderWasFinished(sellOrder, _symbol);

                await _botApi.ShortTimeout(commonTransaction.BuyExchangeAccount, commonTransaction.SellExchangeAccount);
                
                return true;
            }

            return false;
        }

        #region Rebalancing

        private async Task ProcessRebalancingTrade(
            PriceByOrderSide prices, 
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            TransactionStatistics transactionStatistics)
        {
            var (tradeInfo1, tradeInfo2) =
                _tradeInfoBuilder.BuildRebalancingTradeInfo(exchangeAccount1, exchangeAccount2, prices, _symbol);

            var rebalancingTask1 = RebalancingAsync(tradeInfo1, prices);

            var rebalancingTask2 = RebalancingAsync(tradeInfo2, prices);

            var exchangeAccount1ErrorMessage = await rebalancingTask1;
            var exchangeAccount2ErrorMessage = await rebalancingTask2;

            if (!string.IsNullOrEmpty(exchangeAccount1ErrorMessage) && !string.IsNullOrEmpty(exchangeAccount2ErrorMessage))
            {
                var errorMessage =
                    $"{exchangeAccount1.Id}: {exchangeAccount1ErrorMessage}" +
                    $"{exchangeAccount2.Id}: {exchangeAccount2ErrorMessage}";

                Log.Logger.Verbose(errorMessage);
                //_botApi.TelegramBot.SendTextMessage(_appSettings.Bot.Telegram.ChatId, errorMessage);

                await _botApi.ShortTimeout(exchangeAccount1, exchangeAccount2);
            }
            else
            {
                transactionStatistics.TransactionsCountAfterLastGetBalance = 0;

                await _botApi.BotGetBalance(exchangeAccount1, exchangeAccount2);
            }
        }
        
        private async Task<String> RebalancingAsync(TradeInfo tradeInfo, PriceByOrderSide prices)
        {
            if (!tradeInfo.CanBeTraded)
            {
                return tradeInfo.ErrorMessage;
            }
            
            await Task.Run(async () =>
            {
                var rebalansingTransaction = await StartRebalancingTransaction(tradeInfo, prices);

                await _botApi.CheckOrderFilled(rebalansingTransaction.ExchangeAccount, rebalansingTransaction.Order);
                if (await CheckIfTransactionFinished(rebalansingTransaction))
                {
                    return;
                }

                await _botApi.CheckOrderFilled(rebalansingTransaction.ExchangeAccount, rebalansingTransaction.Order);
                if (await CheckIfTransactionFinished(rebalansingTransaction))
                {
                    return;
                }

                var order = rebalansingTransaction.Order;

                while (true)
                {
                    await _botApi.BotCancelOrder(rebalansingTransaction.ExchangeAccount, order);
                    if (order.Status == OrderStatus.Canceled || order.Status == OrderStatus.Completed)
                    {
                        break;
                    }
                }

                await CheckIfTransactionFinished(rebalansingTransaction);
            });

            return null;
        }
        
        private async Task<RebalansingTransaction> StartRebalancingTransaction(TradeInfo tradeInfo, PriceByOrderSide prices)
        {
            decimal price;
            decimal amount;
            Exchange exchangeAccount;
            OrderSide orderSide;
            if (tradeInfo.SellAmount == 0)
            {
                price = tradeInfo.BuyPrice;
                amount = tradeInfo.BuyAmount;
                exchangeAccount = tradeInfo.BuyExchangeAccount;
                //_balanceManager.Reserve(exchangeName, symbol, OrderSide.Buy, price, amount);                
                orderSide = OrderSide.Buy;
            }
            else
            {
                price = tradeInfo.SellPrice;
                amount = tradeInfo.SellAmount;
                exchangeAccount = tradeInfo.SellExchangeAccount;
                //_balanceManager.Reserve(exchangeName, symbol, OrderSide.Sell, price, amount);
                orderSide = OrderSide.Sell;
            }

            var order = await _botApi.CreateOrder(exchangeAccount, _symbol, price, amount, orderSide, true, StrategyName);
            
            _createOrdersTimeoutManager.UpdateRequestTime(exchangeAccount.Id);
            
            await _botApi.ShortTimeout(exchangeAccount);

            var transaction = new RebalansingTransaction(order, exchangeAccount);

            var snapshotPrices = GetSnapshotPrices(prices);

            var message = 
                "Rebalancing transaction started\n" +
                $"Price: {price}\tAmount: {amount}\tAccountId:{exchangeAccount.Id}\n" +
                snapshotPrices;
            Log.Logger.Verbose(message);
            //_botApi.TelegramBot.SendTextMessage(_appSettings.Bot.Telegram.ChatId, message);

            return transaction;
        }
        

        #endregion
        
        private static string GetSnapshotPrices(PriceByOrderSide prices)
        {
            var result = $"Ask: {prices.TopAsk.Value}; Bid: {prices.TopBid.Value}";
            return result;
        }
        
        private Task<bool> CheckIfTransactionFinished(RebalansingTransaction rebalansingTransaction)
        {
            var order = rebalansingTransaction.Order;
            if (!order.IsFinished)
            {
                return Task.FromResult(false);
            }
            
            var logMessage = "Transaction finished\n" +
                             $"order {order.GetOrderInfoDescription()} id= {order.ExchangeOrderId} AccountId: {rebalansingTransaction.ExchangeAccount.Id}";
            Log.Logger.Verbose(logMessage);

            var teleMessage = "Transaction finished\n" +
                              $"order {order.GetOrderInfoDescription()}";
            //_botApi.TelegramBot.SendTextMessage(_appSettings.Bot.Telegram.ChatId, teleMessage);
                
            return Task.FromResult(true);

        }
        
        private async Task CheckFillOrCancelOrders(CommonTransaction commonTransaction)
        {
            var orders = GetTransactionOrders(commonTransaction);
            var buyOrder = orders.buy;
            var sellOrder = orders.sell;

            var buyFillOrCancelTask = CheckFillOrCancelOrder(commonTransaction, buyOrder, commonTransaction.BuyExchangeAccount);
            var sellFillOrCancelTask = CheckFillOrCancelOrder(commonTransaction, sellOrder, commonTransaction.SellExchangeAccount);

            var buyFillOrCancelWasCalled = await buyFillOrCancelTask;
            var sellFillOrCancelWasCalled = await sellFillOrCancelTask;

            if (buyFillOrCancelWasCalled || sellFillOrCancelWasCalled)
            {
                await _botApi.ShortTimeout(commonTransaction.BuyExchangeAccount, commonTransaction.SellExchangeAccount);
            }
        }

        private async Task<bool> CheckFillOrCancelOrder(CommonTransaction commonTransaction, Order order, Exchange exchangeAccount)
        {
            if (order.Status == OrderStatus.Created)
            {
                if (commonTransaction.IterationsCount >= RetriesMaxCount)
                {
                    await _botApi.BotCancelOrder(exchangeAccount, order, false);
                }
                else
                {
                    await _botApi.CheckOrderFilled(exchangeAccount, order, false);
                }

                return true;
            }

            return false;
        }
        
        private async Task ReCancelOrdersIfFailedToCancel(CommonTransaction commonTransaction)
        {
            var buyCancelTask = _botApi.ReCancel(commonTransaction.BuyExchangeAccount, commonTransaction.BuyOrder);
            var sellCancelTask = _botApi.ReCancel(commonTransaction.SellExchangeAccount, commonTransaction.SellOrder);

            var buyWasReCanceled = await buyCancelTask;
            var sellWasReCanceled = await sellCancelTask;

            if (buyWasReCanceled || sellWasReCanceled)
            {
                await _botApi.ShortTimeout(commonTransaction.BuyExchangeAccount, commonTransaction.SellExchangeAccount);
            }
        }

    }
}