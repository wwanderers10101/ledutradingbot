﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Bot;
using Core.Bot.TaskExtensions;
using Core.Exchanges.Core;
using Core.Model;
using CryptoLp.Bot.Helpers;
using CryptoLp.Bot.Settings;
using CryptoLp.Bot.WashTrading.Strategies.Base;

namespace CryptoLp.Bot.WashTrading.Strategies.PriceShockStrategy
{    
    public class PriceShock : WashTradingStrategy
    {        
        private readonly decimal _priceMaxDropdownRate;

        /// <summary>
        /// Trades for last _tradesAnalyzeWindowDuration seconds
        /// </summary>
        private List<Trades> _tradesList = new List<Trades>();
        private readonly object _tradesListSyncObj = new object();
        private readonly TimeSpan _tradesAnalyzeWindowDuration;
        
        private readonly TaskEvent<Trades> _nextTrades = new TaskEvent<Trades>();
        
        private readonly IDateTimeService _dateTimeService;
        
        public PriceShock(
            AppSettings<BotSettings> appSettings,
            BotApi botApi, 
            Symbol symbol, 
            Exchange exchange1,
            Exchange exchange2,
            IDateTimeService dateTimeService)
            :base(appSettings, botApi, symbol, exchange1, exchange2,
                appSettings.Bot.TradeAmount[symbol.BaseCurrencyCode], appSettings.Bot.TradeAmount[symbol.BaseCurrencyCode])
        {
            _dateTimeService = dateTimeService;

            var washTradingSettings = appSettings.Bot.PriceShock;
            _priceMaxDropdownRate = 0.01m * washTradingSettings.PriceMaxDropdown;
            _tradesAnalyzeWindowDuration = TimeSpan.FromSeconds(washTradingSettings.TradesAnalyzeWindowDuration);
        }

        public override string StrategyName => nameof(PriceShock);
        
        public void OnPrints(Trades trades)
        {
            lock (_tradesListSyncObj)
            {
                var now = _dateTimeService.UtcNow;
                var timeBound = now - _tradesAnalyzeWindowDuration;
                var newTradesList = _tradesList.Where(x => x.DateTime >= timeBound).ToList();
                newTradesList.Add(trades);
                _tradesList = newTradesList;
            }         
            
            _nextTrades.RaiseEvent(trades);
        }
        
        public override async Task Trigger()
        {
            while (true)
            {
                await _nextTrades.WaitAsync();

                if (!CheckDropdownTrend(_tradesList))
                {
                    continue;
                }

                return;
            }
        }

        private bool CheckDropdownTrend(List<Trades> tradesList)
        {
            if (tradesList.Count < 2)
            {
                return false;
            }
            
            for (int i = 0; i < tradesList.Count - 1; i++)
            {
                for (int j = 1; j < tradesList.Count; j++)
                {
                    var maxTradeItem = tradesList[i].TradeItems.MaxItem(x => x.Price);
                    var minTradeItem = tradesList[j].TradeItems.MinItem(x => x.Price);

                    var maxPrice = maxTradeItem.Price;
                    var minPrice = minTradeItem.Price;
                    if (maxPrice - minPrice < _priceMaxDropdownRate * maxPrice)
                    {
                        continue;
                    }

                    return true;
                }
            }

            return false;
        }
    }
}