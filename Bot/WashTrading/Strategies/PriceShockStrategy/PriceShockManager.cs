using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Bot.Events;
using Core.Model;
using CryptoLp.Bot.Common;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;

namespace CryptoLp.Bot.WashTrading.Strategies.PriceShockStrategy
{
    public class PriceShockManager: IStrategyManager
    {
        private readonly BotApi _botApi;
        private readonly Dictionary<(string ExchangeName, string CurrencyPair), PriceShockStrategy.PriceShock> _strategies = new Dictionary<(string ExchangeName, string CurrencyPair), PriceShockStrategy.PriceShock>();
        
        [UsedImplicitly] 
        private Task _eventsLoopTask;

        private ExchangeEventsLoop _eventsLoop;

        public PriceShockManager(
            AppSettings<BotSettings> appSettings,
            BotApi botApi)
        {
            _botApi = botApi;


            var dateTimeService = botApi.DateTimeService;
            var exchangeList = botApi.ExchangesById.Values
                .Where(x => x.IsTrading)
                .ToArray();
            
            var exchangesByExchangeIdAndCurrencyCodePair = exchangeList
                .SelectMany(x => x.Symbols, (exchange, symbol) => (exchange, symbol))
                .ToDictionary(x => (x.exchange.Id, x.symbol.CurrencyCodePair));

            if (appSettings.Bot?.PriceShock?.IsEnabled == true)
            {
                if (appSettings.Bot.PriceShock.ExchangePairs == null)
                {
                    throw new Exception("Can't start wash trading. You need specify exchange pairs");
                }
                
                foreach (var exchangePair in appSettings.Bot.PriceShock.ExchangePairs)
                {
                    foreach (var currencyCodePair in exchangePair.Symbols)
                    {
                        if (!exchangesByExchangeIdAndCurrencyCodePair.TryGetValue((exchangePair.Exchange1, currencyCodePair), out var exchangeSymbol1))
                        {
                            throw new Exception($"Can't find initialized exchange:symbol {exchangePair.Exchange1}:{currencyCodePair}");
                        }

                        if (!exchangesByExchangeIdAndCurrencyCodePair.TryGetValue((exchangePair.Exchange2, currencyCodePair), out var exchangeSymbol2))
                        {
                            throw new Exception($"Can't find initialized exchange:symbol {exchangePair.Exchange2}:{currencyCodePair}");
                        }

                        var symbol = exchangeSymbol1.symbol;
                        var exchangeName = exchangeSymbol1.exchange.Name;

                        var strategy = new PriceShock(
                            appSettings, 
                            botApi, 
                            symbol, 
                            exchangeSymbol1.exchange,
                            exchangeSymbol2.exchange,
                            dateTimeService);

                        _strategies.Add((exchangeName, symbol.CurrencyPair), strategy);       
                    }                    
                }

                (_eventsLoop, _eventsLoopTask) = ExchangeEventsLoop.Run(botApi.CreateEventChannel());
                _eventsLoop.Prints += OnPrints;
            }
        }

        private void OnPrints(Trades trades)
        {
            if (!_strategies.TryGetValue((trades.ExchangeName, trades.CurrencyPair), out var strategy))
            {
                return;
            }
            
            strategy.OnPrints(trades);
        }

        public Task Start()
        {
            var tasks = _strategies.Values
                .Select(x => x.Start())
                .ToList();

            if (tasks.Count == 0)
            {
                return null;
            }

            return InnerStart(tasks);
        }

        private static async Task InnerStart(List<Task> tasks)
        {
            await await Task.WhenAny(tasks);

            throw new Exception("Happend something wrong");
        }

        public void Dispose()
        {
            _eventsLoop.Prints -= OnPrints;
        }
    }
}