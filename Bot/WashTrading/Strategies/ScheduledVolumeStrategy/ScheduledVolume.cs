using System.Threading.Tasks;
using Core.Bot.TaskExtensions;
using Core.Exchanges.Core;
using Core.Model;
using CryptoLp.Bot.Settings;
using CryptoLp.Bot.WashTrading.Strategies.Base;
using JetBrains.Annotations;
using Timer = System.Threading.Timer;

namespace CryptoLp.Bot.WashTrading.Strategies.ScheduledVolumeStrategy
{
    public class ScheduledVolume: WashTradingStrategy 
    {
        [UsedImplicitly]
        private readonly Timer _timer;

        [NotNull]
        private readonly TaskEvent<Unit> _triggerTaskEvent = new TaskEvent<Unit>();

        public ScheduledVolume(
            AppSettings<BotSettings> appSettings,
            BotApi botApi, 
            Symbol symbol, 
            Exchange exchange1,
            Exchange exchange2)
            : base(appSettings, botApi, symbol, exchange1, exchange2,
                appSettings.Bot.ScheduledVolume.Exchanges[exchange1.Name][symbol.CurrencyCodePair].Amount_Min,
                appSettings.Bot.ScheduledVolume.Exchanges[exchange1.Name][symbol.CurrencyCodePair].Amount_Max)
        {
            var period = appSettings.Bot.ScheduledVolume.TriggerPeriod;
            _timer = new Timer(OnTimerTick, null, 0, period);
        }

        public override string StrategyName => nameof(ScheduledVolume);

        private void OnTimerTick(object state)
        {
            _triggerTaskEvent.RaiseEvent(new Unit());
        }

        public override Task Trigger()
        {
            return _triggerTaskEvent.WaitAsync();
        }
    }
}