using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Model;
using CryptoLp.Bot.Common;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;

namespace CryptoLp.Bot.WashTrading.Strategies.ScheduledVolumeStrategy
{
    public class ScheduledVolumeManager: IStrategyManager
    {
        private readonly BotApi _botApi;
        private readonly Dictionary<(string ExchangeName, string CurrencyPair), ScheduledVolume> _strategies = new Dictionary<(string ExchangeName, string CurrencyPair), ScheduledVolume>();

        public ScheduledVolumeManager(
            AppSettings<BotSettings> appSettings,
            BotApi botApi)
        {
            _botApi = botApi;
            
            var exchangeList = botApi.ExchangesById.Values
                .Where(x => x.IsTrading)
                .ToArray();
            
            var exchangesByExchangeIdAndCurrencyCodePair = exchangeList
                .SelectMany(x => x.Symbols, (exchange, symbol) => (exchange, symbol))
                .ToDictionary(x => (x.exchange.Id, x.symbol.CurrencyCodePair));

            if (appSettings.Bot?.ScheduledVolume?.IsEnabled == true)
            {
                if (appSettings.Bot.ScheduledVolume.ExchangePairs == null)
                {
                    throw new Exception("Can't start wash trading. You need specify exchange pairs");
                }
                
                foreach (var exchangePair in appSettings.Bot.ScheduledVolume.ExchangePairs)
                {
                    foreach (var currencyCodePair in exchangePair.Symbols)
                    {
                        if (!exchangesByExchangeIdAndCurrencyCodePair.TryGetValue((exchangePair.Exchange1, currencyCodePair), out var exchangeSymbol1))
                        {
                            throw new Exception($"Can't find initialized exchange:symbol {exchangePair.Exchange1}:{currencyCodePair}");
                        }

                        if (!exchangesByExchangeIdAndCurrencyCodePair.TryGetValue((exchangePair.Exchange2, currencyCodePair), out var exchangeSymbol2))
                        {
                            throw new Exception($"Can't find initialized exchange:symbol {exchangePair.Exchange2}:{currencyCodePair}");
                        }

                        var symbol = exchangeSymbol1.symbol;
                        var exchangeName = exchangeSymbol1.exchange.Name;

                        var strategy = new ScheduledVolume(
                            appSettings, 
                            botApi, 
                            symbol, 
                            exchangeSymbol1.exchange,
                            exchangeSymbol2.exchange);

                        _strategies.Add((exchangeName, symbol.CurrencyPair), strategy);       
                    }                    
                }
            }
        }

        public Task Start()
        {
            var tasks = _strategies.Values
                .Select(x => x.Start())
                .ToList();

            if (tasks.Count == 0)
            {
                return null;
            }

            return InnerStart(tasks);
        }

        private static async Task InnerStart(List<Task> tasks)
        {
            await await Task.WhenAny(tasks);

            throw new Exception("Happend something wrong");
        }

        public void Dispose()
        {
        }
    }
}