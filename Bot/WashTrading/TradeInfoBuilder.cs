﻿using System;
using Core.Bot;
using Core.Bot.LocalSnapshots;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Helpers;
using CryptoLp.Bot.Settings;
using JetBrains.Annotations;
using Serilog;

namespace CryptoLp.Bot.WashTrading
{
    public class TradeInfoBuilder
    {
        private readonly AppSettings<BotSettings> _appSettings;
        private readonly BalanceManager _balanceManager;

        public TradeInfoBuilder(AppSettings<BotSettings> appSettings, BalanceManager balanceManager)
        {
            _appSettings = appSettings;
            _balanceManager = balanceManager;
        }

        public TradeInfo BuildWashTradeInfo(
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            Symbol symbol,
            string fromCurrencyCode,
            string toCurrencyCode,
            PriceByOrderSide prices,
            decimal minAmount,
            decimal maxAmount)
        {
            // TODO should we block transactions by Exchange-Symbol?
//            if (transactions.Keys.Any(x => x.BuyExchangeSymbol.Equals(exchangeSymbol)) ||
//                transactions.Keys.Any(x => x.BuyExchangeSymbol.Equals(toExchangeSymbol)) ||
//                transactions.Keys.Any(x => x.SellExchangeSymbol.Equals(exchangeSymbol)) ||
//                transactions.Keys.Any(x => x.SellExchangeSymbol.Equals(toExchangeSymbol)))
//            {
//                Log.Logger.Verbose(
//                    "These ExchangeSymbols are being traded {fomExchangeSymbol} {toExchangeSymbol}",
//                    exchangeSymbol, toExchangeSymbol);
//                return new TradeInfo(false);
//            }

            if (!_balanceManager.BalanceWasReceived(exchangeAccount1.Id))
            {
                return new TradeInfo($"Balance for accountId {exchangeAccount1.Id} was not received yet");
            }

            if (!_balanceManager.BalanceWasReceived(exchangeAccount2.Id))
            {
                return new TradeInfo($"Balance for accountId {exchangeAccount2.Id} was not received yet");
            }

            var tradeInfo = CheckBidAskSpreadSize(prices);
            if (tradeInfo != null)
                return tradeInfo;
            
            var buyPriceBound = prices.TopBid.Value;
            var sellPriceBound = prices.TopAsk.Value;
            
            var buyPrice = GetWashTradingPrice(buyPriceBound, sellPriceBound, symbol);
            var sellPrice = buyPrice; // GetSellPrice(exchangeSymbol, sellPriceBound);
            
            var amount = symbol.AmountFloor(Utils.NextDecimal(minAmount, maxAmount));

            return SelectTradeVariant(exchangeAccount1, exchangeAccount2, symbol, fromCurrencyCode, toCurrencyCode, buyPrice, sellPrice, amount);            
        }

        private decimal GetWashTradingPrice(decimal buyPriceBound, decimal sellPriceBound, Symbol symbol)
        {
            // TODO remove hardcode
            const decimal PriceSpreadRateForSelection = 0.1m;

            var pricePrecision = symbol.PricePrecision;
            //ToDo: Reuse GetPriceTick from Symbol
            var priceTick = GetPriceTick(pricePrecision);
            
            var spread = sellPriceBound - buyPriceBound;
            //var priceOffset = spread * PriceSpreadRateForSelection;
            var priceOffset = Utils.NextDecimal(priceTick, spread - priceTick);

            //priceOffset = priceOffset < priceTick ? priceTick : priceOffset;
            var price = sellPriceBound - priceOffset;
            //ToDo: Reuse Floor from Math
            return Floor(price, pricePrecision);
        }

        public TradeInfo BuildCommonTradeInfo(
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            Symbol symbol,
            string fromCurrencyCode,
            string toCurrencyCode,
            PriceByOrderSide prices)
        {
            if (!_appSettings.Bot.TradeAmount.ContainsKey(symbol.BaseCurrencyCode))
            {
                return new TradeInfo($"{symbol.BaseCurrencyCode} is not specified in config TradeAmount");
            }
            
//            if (transactions.Keys.Any(x => x.BuyExchangeSymbol.Equals(exchangeSymbol)) ||
//                transactions.Keys.Any(x => x.BuyExchangeSymbol.Equals(toExchangeSymbol)) ||
//                transactions.Keys.Any(x => x.SellExchangeSymbol.Equals(exchangeSymbol)) ||
//                transactions.Keys.Any(x => x.SellExchangeSymbol.Equals(toExchangeSymbol)))
//            {
//                Log.Logger.Verbose(
//                    "These ExchangeSymbols are being traded {fomExchangeSymbol} {toExchangeSymbol}",
//                    exchangeSymbol, toExchangeSymbol);
//                return new TradeInfo(false);
//            }

            if (!_balanceManager.BalanceWasReceived(exchangeAccount1.Id))
            {
                return new TradeInfo($"Balance for accountId {exchangeAccount1.Id} was not received yet");
            }

            if (!_balanceManager.BalanceWasReceived(exchangeAccount2.Id))
            {
                return new TradeInfo($"Balance for accountId {exchangeAccount2.Id} was not received yet");
            }

            var tradeInfo = CheckBidAskSpreadSize(prices);
            if (tradeInfo != null)
                return tradeInfo;
            
            var buyPriceBound = prices.TopBid.Value;
            var sellPriceBound = prices.TopAsk.Value;
            
            var buyPrice = GetMiddlePrice(buyPriceBound, sellPriceBound, symbol);
            var sellPrice = buyPrice; // GetSellPrice(exchangeSymbol, sellPriceBound);

            //We use these fake prices for hardcore cancellation testing
//            buyPrice = 5000;
//            sellPrice = 9900;
            
            var tradeAmount = GetTradeAmount(symbol);

            return SelectTradeVariant(exchangeAccount1, exchangeAccount2, symbol, fromCurrencyCode, toCurrencyCode, buyPrice, sellPrice, tradeAmount);
        }

        private TradeInfo SelectTradeVariant(
            Exchange exchangeAccount1, 
            Exchange exchangeAccount2, 
            Symbol symbol,
            string fromCurrencyCode, 
            string toCurrencyCode, 
            decimal buyPrice, 
            decimal sellPrice, 
            decimal tradeAmount)
        {
            var tradeInfoVariant1 = BuildCommonTradeInfoVariant(exchangeAccount1, exchangeAccount2, symbol, fromCurrencyCode,
                toCurrencyCode, buyPrice, sellPrice, tradeAmount);
            var tradeInfoVariant2 = BuildCommonTradeInfoVariant(exchangeAccount2, exchangeAccount1, symbol, fromCurrencyCode,
                toCurrencyCode, buyPrice, sellPrice, tradeAmount);

            if (!tradeInfoVariant1.CanBeTraded && !tradeInfoVariant2.CanBeTraded)
            {
                var errorMessage = "Can't start transaction both variants have errors:\n" +
                                   $"from {exchangeAccount1.Id} to  {exchangeAccount2.Id} error: {tradeInfoVariant1.ErrorMessage}\n" +
                                   $"from {exchangeAccount2.Id} to  {exchangeAccount1.Id} error: {tradeInfoVariant1.ErrorMessage}";
                return new TradeInfo(errorMessage);
            }

            if (tradeInfoVariant1.CanBeTraded && !tradeInfoVariant2.CanBeTraded)
            {
                return tradeInfoVariant1;
            }

            if (!tradeInfoVariant1.CanBeTraded && tradeInfoVariant2.CanBeTraded)
            {
                return tradeInfoVariant2;
            }

            if (tradeInfoVariant1.CanBeTraded && tradeInfoVariant2.CanBeTraded)
            {
                return tradeInfoVariant1.BuyAmount > tradeInfoVariant2.BuyAmount ? tradeInfoVariant1 : tradeInfoVariant2;
            }

            throw new Exception("Unexpected execution situation");
        }

        [CanBeNull]
        private static TradeInfo CheckBidAskSpreadSize(PriceByOrderSide prices)
        {
            //ToDo: This should be either removed or improved/customised as doesn't make any sense for 99.99% of coins
            if (prices.TopAsk - prices.TopBid > 30)
            {
                return new TradeInfo($"Bid-Ask spread is {prices.TopAsk - prices.TopBid} > 30");
            }

            return null;
        }

        private TradeInfo BuildCommonTradeInfoVariant(Exchange buyExchangeAccount,
            Exchange sellExchangeAccount,
            Symbol symbol,
            string fromCurrencyCode,
            string toCurrencyCode,
            decimal buyPrice,
            decimal sellPrice,
            decimal tradeAmount)
        {
            var fromQuoteBalance = _balanceManager.GetBalance(buyExchangeAccount.Id, symbol.QuoteCurrencyCode);
            if (!fromQuoteBalance.HasValue)
            {
                return new TradeInfo($"There's no balance for {symbol.QuoteCurrencyCode} on {buyExchangeAccount.Id}");
            }
            var buyAmount = fromQuoteBalance.Value / buyPrice;
            if (tradeAmount < buyAmount)
            {
                buyAmount = tradeAmount;
            }

            var toBaseBalance = _balanceManager.GetBalance(sellExchangeAccount.Id, symbol.BaseCurrencyCode);
            if (!toBaseBalance.HasValue)
            {
                return new TradeInfo($"There's no balance for {symbol.BaseCurrencyCode} on {sellExchangeAccount.Id}");
            }
            var toAmount = toBaseBalance.Value;
            if (tradeAmount < toAmount)
            {
                toAmount = tradeAmount;
            }

            var sellAmount = toAmount;
            var amount = Math.Min(buyAmount, sellAmount);

            amount = Floor(amount, symbol.AmountPrecision);
            buyAmount = amount;
            sellAmount = amount;

            if (!IsEnoughAmountAndCost(symbol, fromCurrencyCode, buyPrice, buyAmount))
            {
                return new TradeInfo(
                    $"Not enough amount or cost (exchange: {buyExchangeAccount.Id}; currency: {fromCurrencyCode}; price: {buyPrice}; amount: {buyAmount})");
            }

            if (!IsEnoughAmountAndCost(symbol, toCurrencyCode, sellPrice, sellAmount))
            {
                return new TradeInfo(
                    $"Not enough amount or cost (exchange: {sellExchangeAccount.Id}; currency: {toCurrencyCode}; price: {sellPrice}; amount: {sellAmount})");
            }

            return new TradeInfo(buyExchangeAccount, buyPrice, buyAmount, sellExchangeAccount, sellPrice, sellAmount);
        }

        public decimal GetTradeAmount(Symbol symbol)
        {
            return _appSettings.Bot.TradeAmount[symbol.BaseCurrencyCode];
        }

        public decimal GetMiddlePrice(decimal buyPriceBound, decimal sellPriceBound, Symbol symbol)
        {
            var price = (buyPriceBound + sellPriceBound) / 2;
            var pricePrecision = symbol.PricePrecision;
            return Floor(price, pricePrecision);
        }

        public bool CheckNeedRebalancing(
            Exchange exchangeAccount1,
            Exchange exchangeAccount2,
            PriceByOrderSide prices,
            Symbol symbol)
        {
            var best = CalculateBestRebalancing(exchangeAccount1, exchangeAccount2, prices, symbol);

            if (best == null)
            {
                return false;
            }

            var minTotalBalance = Math.Min(best.Balance1.TotalBalance, best.Balance2.TotalBalance);

            const decimal maxRateForRebalancing = 0.4m;

            return maxRateForRebalancing * minTotalBalance < best.RebalancingVolume;
        }

        public (TradeInfo, TradeInfo) BuildRebalancingTradeInfo(
            Exchange exchangeAccount1, 
            Exchange exchangeAccount2,
            PriceByOrderSide prices,
            Symbol symbol)
        {
            var tradeInfo = CheckBidAskSpreadSize(prices);
            if (tradeInfo != null)
                return (tradeInfo, tradeInfo);
            
            var amountPrecision = symbol.AmountPrecision;

            var bestReblancingVariant = CalculateBestRebalancing(exchangeAccount1, exchangeAccount2, prices, symbol);
            if (bestReblancingVariant == null)
            {
                var errorTradeInfo = new TradeInfo($"Best rebalansing variant for {exchangeAccount1.Id} and {exchangeAccount2.Id} is null");
                return (errorTradeInfo, errorTradeInfo);
            }
            var commission = GetEffectiveCommissionRate(exchangeAccount1.Id);

            var sellPrice = bestReblancingVariant.SellPrice;
            var rawSellAmount = bestReblancingVariant.QouteDelta / (1 + commission);
            var sellAmount = Floor(rawSellAmount, amountPrecision);
            var sellExchangeAccount = bestReblancingVariant.Balance1.ExchangeAccount;

            var buyPrice = bestReblancingVariant.BuyPrice;
            var rawBuyAmount = bestReblancingVariant.BaseDelta / (1 + commission);
            var buyAmount = Floor(rawBuyAmount, amountPrecision);
            var buyExchangeAccount = bestReblancingVariant.Balance2.ExchangeAccount;

            // buy
            var quoteCurrencyCode = symbol.QuoteCurrencyCode;
            var tradeInfo1 = !IsEnoughAmountAndCost(symbol, quoteCurrencyCode, buyPrice, buyAmount, needLog: false)
                ? new TradeInfo($"Not enough amount or cost (exchange: {buyExchangeAccount.Id}; currency: {quoteCurrencyCode}; price: {buyPrice}; amount: {buyAmount})")
                : new TradeInfo(buyExchangeAccount, buyPrice, buyAmount, sellExchangeAccount, 0, 0);

            // sell
            var baseCurrencyCode = symbol.BaseCurrencyCode;
            var tradeInfo2 = !IsEnoughAmountAndCost(symbol, baseCurrencyCode, sellPrice, sellAmount, needLog: false)
                ? new TradeInfo($"Not enough amount or cost (exchange: {sellExchangeAccount.Id}; currency: {baseCurrencyCode}; price: {sellPrice}; amount: {sellAmount})")
                : new TradeInfo(sellExchangeAccount, 0, 0, sellExchangeAccount, sellPrice, sellAmount);

            return (tradeInfo1, tradeInfo2);
        }

        [CanBeNull]
        private RebalancingVariant CalculateBestRebalancing(
            Exchange exchangeAccount1, 
            Exchange exchangeAccount2,
            PriceByOrderSide prices, 
            Symbol symbol)
        {
            var rebalancingVariant1 = CalculateRebalancingVariant(exchangeAccount1, exchangeAccount2, prices, symbol);
            var rebalancingVariant2 = CalculateRebalancingVariant(exchangeAccount2, exchangeAccount1, prices, symbol);

            if (rebalancingVariant1 == null || rebalancingVariant2 == null)
            {
                return null;
            }

            var best = rebalancingVariant1.RebalancingVolume < rebalancingVariant2.RebalancingVolume
                ? rebalancingVariant1
                : rebalancingVariant2;
            
            return best;
        }

        [CanBeNull]
        private RebalancingVariant CalculateRebalancingVariant(
            Exchange exchangeAccount1, 
            Exchange exchangeAccount2,
            PriceByOrderSide prices,
            Symbol symbol)
        {
            // TODO do not foget calculate price as midle ask & bids to not lost balance on rebalancing
            var buyPrice = Floor(prices.TopAsk.Value, symbol.PricePrecision);
            var sellPrice = Floor(prices.TopBid.Value, symbol.PricePrecision);
            
            var balance1 = GetBalances(exchangeAccount1, symbol, buyPrice);
            var balance2 = GetBalances(exchangeAccount2, symbol, buyPrice);

            if (balance1 == null || balance2 == null)
            {
                return null;
            }

            var minTotalBalance = Math.Min(balance1.RestrictedTotalBalance, balance2.RestrictedTotalBalance);

            var quoteDelta = balance1.RestrictedQuoteBalance < minTotalBalance ? minTotalBalance - balance1.RestrictedQuoteBalance : 0m;
            var baseDelta = balance2.RestrictedBaseBalance < minTotalBalance ? minTotalBalance - balance2.RestrictedBaseBalance : 0m;

            return new RebalancingVariant(quoteDelta, baseDelta, balance1, balance2, buyPrice, sellPrice);
        }

        private class RebalancingVariant
        {
            public RebalancingVariant(decimal qouteDelta, decimal baseDelta, Balance balance1, Balance balance2, decimal buyPrice, decimal sellPrice)
            {
                QouteDelta = qouteDelta;
                BaseDelta = baseDelta;
                Balance1 = balance1;
                Balance2 = balance2;
                BuyPrice = buyPrice;
                SellPrice = sellPrice;
            }

            public decimal RebalancingVolume => QouteDelta + BaseDelta;
            public decimal QouteDelta { get; }
            public decimal BaseDelta { get; }
            public decimal BuyPrice { get; }
            public decimal SellPrice { get; }
            public Balance Balance1 { get; }
            public Balance Balance2 { get; }
        }

        private class Balance
        {
            public Balance(decimal quoteBalance, decimal baseBalance, decimal tradeAmount, Exchange exchangeAccount)
            {
                QuoteBalance = quoteBalance;
                BaseBalance = baseBalance;
                TradeAmount = tradeAmount;
                ExchangeAccount = exchangeAccount;
            }

            public decimal QuoteBalance { get; set; }
            public decimal RestrictedQuoteBalance => Math.Min(QuoteBalance, TradeAmount);
            public decimal BaseBalance { get; set; }
            public decimal RestrictedBaseBalance => Math.Min(BaseBalance, TradeAmount);
            public decimal TradeAmount { get; set; }
            public Exchange ExchangeAccount { get; }

            public decimal TotalBalance => QuoteBalance + BaseBalance;
            public decimal RestrictedTotalBalance => Math.Min(TotalBalance, TradeAmount);
        }
        
        private Balance GetBalances(Exchange exchangeAccount, Symbol symbol, decimal buyPrice)
        {
            //var tradeAmount = GetTradeAmount(symbol);
            var tradeAmount = 1.5m; //ToDo: Hm... Where does this 1.5 come from?
            
            var quoteBalance = _balanceManager.GetBalance(exchangeAccount.Id, symbol.QuoteCurrencyCode) / buyPrice;
            
            var baseBalance = _balanceManager.GetBalance(exchangeAccount.Id, symbol.BaseCurrencyCode);

            if (quoteBalance == null)
            {
                Log.Logger.Information("There's no balance for {currencyCode} on {exchangeId}", symbol.QuoteCurrencyCode, exchangeAccount.Id);
                return null;
            }
            
            if (baseBalance == null)
            {
                Log.Logger.Information("There's no balance for {currencyCode} on {exchangeId}", symbol.BaseCurrencyCode, exchangeAccount.Id);
                return null;
            }
            
            return new Balance(quoteBalance.Value, baseBalance.Value, tradeAmount, exchangeAccount);
        }

        private decimal GetEffectiveCommissionRate(string exchangeId)
        {
            var commissionPercents = _appSettings.Core.Commissions[exchangeId].Taker.EffectiveComission;
            return 0.01m * commissionPercents;
        }

        public bool IsEnoughAmountAndCost(Symbol symbol, string currencyCode, decimal price,
            decimal amount, bool needLog = true)
        {
            if (!IsEnoughAmount(symbol, currencyCode, amount, needLog))
            {
                return false;
            }

            var buyCost = amount * price;
            if (!IsEnoughCost(symbol, currencyCode, buyCost, needLog))
            {
                return false;
            }

            return true;
        }

        private bool IsEnoughCost(Symbol symbol, string currencyCode, decimal buyCost, bool needLog = true)
        {
            if (buyCost <= symbol.MinCost)
            {
                if (needLog)
                {
                    Log.Logger.Error("Can't create order with cost {cost} <= {minCost} of {currency}",
                        buyCost, symbol.MinCost, currencyCode);                    
                }
                
                return false;
            }

            return true;
        }

        private bool IsEnoughAmount(Symbol symbol, string currencyCode, decimal buyAmount, bool needLog = true)
        {
            if (buyAmount <= symbol.MinAmount)
            {
                if (needLog)
                {
                    Log.Logger.Error("Can't create order for amount {amount} <= {minAmount} of {currency}",
                        buyAmount, symbol.MinAmount, currencyCode);                    
                }
                
                return false;
            }

            return true;
        }

        public decimal GetSellPrice(ExchangeIdSymbol sellExchangeIdSymbol, decimal sellPriceBound)
        {
            var pricePrecision = sellExchangeIdSymbol.Symbol.PricePrecision;
            var sellPriceWithOffset = Floor(sellPriceBound - GetPriceTick(pricePrecision), pricePrecision);
            return sellPriceWithOffset;
        }

        public decimal GetBuyPrice(ExchangeIdSymbol buyExchangeIdSymbol, decimal buyPriceBound)
        {
            var pricePrecision = buyExchangeIdSymbol.Symbol.PricePrecision;
            var buyPriceWithOffset = Floor(buyPriceBound + GetPriceTick(pricePrecision), pricePrecision);
            return buyPriceWithOffset;
        }

        private static decimal GetPriceTick(long pricePrecision)
        {
            return (decimal)Math.Pow(0.1d, pricePrecision);
        }

        private static decimal Floor(decimal value, long precision)
        {
            var powPrecision = (decimal) Math.Pow(10, precision);
            return Math.Floor(value * powPrecision) / powPrecision;
        }
    }

    public class TradeInfo
    {
        public string ErrorMessage { get; set; }
        public Exchange BuyExchangeAccount { get; set; } 
        public decimal BuyPrice { get; set; }
        public decimal BuyAmount { get; set; }
        public Exchange SellExchangeAccount { get; set; }
        public decimal SellPrice { get; set; }
        public decimal SellAmount { get; set; }

        public bool CanBeTraded => string.IsNullOrEmpty(ErrorMessage);

        public TradeInfo(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }
        
        public TradeInfo(Exchange buyExchangeAccount, decimal buyPrice, decimal buyAmount, Exchange sellExchangeAccount, decimal sellPrice, decimal sellAmount)
        {
            BuyExchangeAccount = buyExchangeAccount;
            BuyPrice = buyPrice;
            BuyAmount = buyAmount;
            SellExchangeAccount = sellExchangeAccount;
            SellPrice = sellPrice;
            SellAmount = sellAmount;
        }
    }
}