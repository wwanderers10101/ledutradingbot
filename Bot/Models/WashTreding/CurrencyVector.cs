﻿using JetBrains.Annotations;

namespace CryptoLp.Bot.Models.WashTreding
{
    public struct CurrencyVector
    {
        public CurrencyVector([NotNull] string exchange, [NotNull] string currencyPair)
        {
            Exchange = exchange;
            CurrencyPair = currencyPair;
        }

        [NotNull]
        public string Exchange { get; set; }
        
        [NotNull]
        public string CurrencyPair { get; set; }

        public override string ToString()
        {
            return $"{nameof(Exchange)}: {Exchange}, {nameof(CurrencyPair)}: {CurrencyPair}";
        }

        public bool Equals(CurrencyVector other)
        {
            return string.Equals(Exchange, other.Exchange) && string.Equals(CurrencyPair, other.CurrencyPair);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is CurrencyVector && Equals((CurrencyVector) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((Exchange != null ? Exchange.GetHashCode() : 0) * 397) ^
                       (CurrencyPair != null ? CurrencyPair.GetHashCode() : 0);
            }
        }
    }
}
