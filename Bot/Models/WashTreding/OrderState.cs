﻿using System;
using Core.Model;

namespace CryptoLp.Bot.Models.WashTreding
{
    public class OrderState
    {
        private DateTime? _startNotCompetitiveDate;

        public OrderState(Order order)
        {
            Order = order;
        }

        public Order Order { get; }

        /// <summary>
        /// Time when we detected that order not in top of order book
        /// </summary>
        public ref DateTime? StartNotCompetitiveDate => ref _startNotCompetitiveDate;
    }
}