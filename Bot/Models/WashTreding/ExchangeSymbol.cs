﻿using Core.Model;

namespace CryptoLp.Bot.Models.WashTreding
{
    public struct ExchangeSymbol
    {
        public ExchangeSymbol(string exchangeId, string exchangeName, Symbol symbol)
        {
            ExchangeId = exchangeId;
            ExchangeName = exchangeName;
            Symbol = symbol;
        }

        public string ExchangeId { get; }
        public string ExchangeName { get; }
        public Symbol Symbol { get; }

        public CurrencyVector CurrencyVector => new CurrencyVector(ExchangeName, Symbol.CurrencyPair);
        
        public override string ToString()
        {
            return $"{nameof(ExchangeId)}: {ExchangeId}, {nameof(Symbol)}: {Symbol.CurrencyPair}";
        }

        public bool Equals(ExchangeSymbol other)
        {
            return string.Equals(ExchangeId, other.ExchangeId) && Symbol.Equals(Symbol, other.Symbol);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is ExchangeSymbol && Equals((ExchangeSymbol) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return ((ExchangeId != null ? ExchangeId.GetHashCode() : 0) * 397) ^
                       (Symbol != null ? Symbol.GetHashCode() : 0);
            }
        }
    }
}