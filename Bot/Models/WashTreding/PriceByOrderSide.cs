﻿namespace CryptoLp.Bot.Models.WashTreding
{
    public class PriceByOrderSide
    {
        public PriceByOrderSide(decimal? bidTopPrice, decimal? askTopPrice)
        {
            BidTopTopPrice = bidTopPrice;
            AskTopPrice = askTopPrice;
        }

        public readonly decimal? BidTopTopPrice;
        public readonly decimal? AskTopPrice;
    }
}