﻿using System;
using System.Collections.Generic;
using System.Linq;
using Core.Model;
using JetBrains.Annotations;

namespace CryptoLp.Bot.Models.WashTreding
{
    public class LocalOrderBookSnapshot
    {
        public object SyncObject = new object();
        
        public LocalOrderBookSnapshot(IDictionary<decimal, decimal> bids, IDictionary<decimal, decimal> asks, DateTime dateTime)
        {
            Asks = new SortedDictionary<decimal, decimal>(asks);
            Bids = new SortedDictionary<decimal, decimal>(bids, new DecimalDescendingComparer());
            
            DateTime = dateTime;
        }

        public SortedDictionary<decimal, decimal> Bids { get; }
        public SortedDictionary<decimal, decimal> Asks { get; }
        
        public DateTime DateTime { get; private set; }

        [NotNull]
        public static LocalOrderBookSnapshot Create([NotNull] OrderBookSnapshot snapshot)
        {
            return new LocalOrderBookSnapshot(snapshot.Bids, snapshot.Asks, snapshot.DateTime);
        }

        public void Apply(OrderBookUpdate[] updates)
        {
            lock (SyncObject)
            {
                foreach (var update in updates)
                {
                    UpdateDictionary(Asks, update.Asks);
                    UpdateDictionary(Bids, update.Bids);
                }

                DateTime = updates.Last().DateTime;
            }
        }

        public void Apply(OrderBookUpdate update)
        {
            lock (SyncObject)
            {
                UpdateDictionary(Asks, update.Asks);
                UpdateDictionary(Bids, update.Bids);

                DateTime = update.DateTime;
            }
        }
        
        public PriceByOrderSide CalculatePrice()
        {
            lock (SyncObject)
            {
                var minAsk = Asks.Count == 0 ? (decimal?)null : Asks.First().Key;
                var maxBid = Bids.Count == 0 ? (decimal?)null : Bids.First().Key;

                return new PriceByOrderSide(maxBid, minAsk);
            }
        }

        public ShortOrder? GetTopAsk()
        {
            lock (SyncObject)
            {
                if (Asks.Count == 0)
                {
                    return null;
                }

                var pair = Asks.FirstOrDefault();
                return new ShortOrder(pair.Key, pair.Value);
            }
        }

        public ShortOrder? GetTopBid()
        {
            lock (SyncObject)
            {
                if (Bids.Count == 0)
                {
                    return null;
                }

                var pair = Bids.FirstOrDefault();
                return new ShortOrder(pair.Key, pair.Value);
            }
        }

        public ShortOrder? GetBidAt(int index)
        {
            lock (SyncObject)
            {
                if (Bids.Count <= 0)
                {
                    return null;
                }

                var pair = Bids.ElementAt(index);
                return new ShortOrder(pair.Key, pair.Value);
            }
        }
        
        private static void UpdateDictionary(SortedDictionary<decimal, decimal> owned, IDictionary<decimal, decimal> updates)
        {
            if (updates == null)
            {
                return;
            }
            
            foreach (var pair in updates)
            {
                if (pair.Value == 0)
                {
                    owned.Remove(pair.Key);                        
                }
                else
                {
                    owned[pair.Key] = pair.Value;
                }
            }
        }
    }

    public readonly struct ShortOrder
    {
        public ShortOrder(decimal price, decimal amount)
        {
            Price = price;
            Amount = amount;
        }

        public decimal Price { get; }
        public decimal Amount { get; }
    }
    
    public class DecimalDescendingComparer : IComparer<decimal>
    {
        public int Compare(decimal x, decimal y)
        {
            return -x.CompareTo(y);
        }
    }
}