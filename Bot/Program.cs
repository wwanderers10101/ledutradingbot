﻿using System;
using System.Threading.Tasks;
using Core.Bot;
using CryptoLp.Bot.Settings;

namespace CryptoLp.Bot
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var botCreator = new BotCreator();
            var bot = await botCreator.Create<BotSettings>((store, exchanges, settings, node) => new Bot(store, exchanges, settings, node));
            await bot.Start();

            Console.ReadLine();
        }
    }
}