﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Exchanges.Core;
using Core.Model;
using CryptoLp.Bot.LiquidityProvider;
using CryptoLp.Bot.PriceManipulation;
using CryptoLp.Bot.Settings;
using CryptoLp.Bot.WashTrading;
using JetBrains.Annotations;
using Marten;
using Microsoft.AspNetCore.NodeServices;
using Serilog;

namespace CryptoLp.Bot
{
    public class Bot : BotApi
    {
        private readonly AppSettings<BotSettings> _appSettings;

        [UsedImplicitly]
        private Thread _marketDataThread;

        private WashTradingManager _washTradingManager;
        private PriceManipulationManager _priceManipulationManager;
        private LiquidityProviderManager _liquidityProvider;

        public Bot(IDocumentStore documentStore, ICollection<Exchange> exchanges, AppSettings<BotSettings> appSettings, INodeServices nodeServices) 
            : base(documentStore, exchanges, appSettings, nodeServices)
        {
            _appSettings = appSettings;
        }
        
        public override async Task Initialize()
        {
            await base.Initialize();
            
            SettingsFix(_appSettings.Bot);
            
            await CancelOpenOrders();
            await InitializeBalances();
            
            _priceManipulationManager = new PriceManipulationManager(_appSettings, this);
            _washTradingManager = new WashTradingManager(_appSettings, this, DateTimeService);
            _liquidityProvider = new LiquidityProviderManager(_appSettings, this);


            _marketDataThread = new Thread(HandleMarketData);
            _marketDataThread.Start();
        }

        private void SettingsFix(BotSettings botSettings)
        {
            if (botSettings.TradeAmount != null)
            {
                botSettings.TradeAmount = botSettings.TradeAmount.TradeAmountSettingFix();
            }
        }

        private async void HandleMarketData(object obj)
        {
            try
            {
                var tasks = new List<Task>
                {
                    _washTradingManager.Start(), 
                    _priceManipulationManager.Start(),
                    _liquidityProvider.Start()
                };

                var filteredTasks = tasks.Where(x => x != null).ToList();
                
                await await Task.WhenAny(filteredTasks);

                throw new Exception("Happend something wrong");
            }
            catch (Exception e)
            {
                Log.Logger.Error(e, "Exception in HandleMarketData");
                throw;
            }
        }
        
        public new void Dispose()
        {
            base.Dispose();
            
            _washTradingManager.Dispose();
            _liquidityProvider.Dispose();
        }
    }
}