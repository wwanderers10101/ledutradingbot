module.exports = async function (callback, exchangeName, apiKey, apiSecret, parameters) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();
    exchange.apiKey = apiKey;
    exchange.secret = apiSecret;
    
    try {
        let closedPosition = await exchange.privatePostPositionClose(parameters);
        callback(null, closedPosition);
    }
    catch(e) {
        callback(e, null);
    }
};