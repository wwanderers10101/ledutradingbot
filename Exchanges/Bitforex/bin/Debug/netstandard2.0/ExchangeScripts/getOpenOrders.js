module.exports = async function (callback, exchangeName, apiKey, apiSecret) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();
    exchange.apiKey = apiKey;
    exchange.secret = apiSecret;
    exchange.options["warnOnFetchOpenOrdersWithoutSymbol"] = false;

    try {
        let openOrders = await exchange.fetchOpenOrders();
        callback(null, openOrders);
    }
    catch(e) {
        callback(e, null);
    }
};