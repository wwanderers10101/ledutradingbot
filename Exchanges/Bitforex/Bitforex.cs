﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using Newtonsoft.Json;
using Nito.AsyncEx;
using RestSharp;
using Serilog;

namespace Core.Exchanges.Bitforex
{
    public partial class Bitforex : Exchange
    {
        private int Timeout = 1000;
        
        private AsyncLock _asyncLock = new AsyncLock();
        
        public Bitforex(ExchangeSettings settings, bool isRecordingMarketData, bool isReducingMarketData, bool isTrading)
            : base(ExtendSettings(settings), isRecordingMarketData, isReducingMarketData, isTrading)
        {
            WebSocketSupportedFeatures.SupportsOpenOrdersByCurrencyPair = true;
        }

        private static ExchangeSettings ExtendSettings(ExchangeSettings settings)
        {
            settings.RestHost = "https://api.bitforex.com";
            return settings;
        }

        public override void AddRestAuthenticationHeaders(RestRequest request, IDictionary<string, object> param)
        {
            param["accessKey"] = ApiKey;
            param["nonce"] = Utils.GetCurrentMiliseconds();

            var inputFields = param.OrderBy(x => x.Key).ToArray();
            var inputString = Utils.GetHttpString(inputFields);
            var concatString = $"{request.Resource}?{inputString}";
            var inputBytes = Encoding.UTF8.GetBytes(concatString);
            var secretBytes = Encoding.UTF8.GetBytes(ApiSecret);
            var hashBytes = new HMACSHA256(secretBytes).ComputeHash(inputBytes);
            var signature = hashBytes.ToHexString().ToLower();
            param["signData"] = signature;
        }

        public class BitforexResponse<T>
        {
            // Always present.
            public bool Success;
            // Present if success is false.
            public long Code;
            public string Message;
            // Present if success is true.
            public T Data;
        }

        private bool IsRestError(RestRequestResult response, [CallerMemberName] string callerName = "")
        {
            return IsRestError(response, out var errorMessage, out var errorCode, callerName);
        }

        private bool IsRestError(RestRequestResult response, out string errorMessage, out long errorCode, [CallerMemberName] string callerName = "")
        {
            errorMessage = null;
            errorCode = 0;
            
            if (response.Content.Contains("\"success\":true"))
                return false;
            
            try
            {
                var data = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(response.Content);

                errorMessage = (string)data["message"];
                errorCode = Convert.ToInt64(data["code"]);
                Log.Logger.Error("{callerName} callback got an error on {exchangeId}: {code} {errorMessage}", callerName, Id, errorCode, errorMessage);
            }
            catch (Exception ex) //we did not receive proper error response, most likely an empty result or an html error page
            {
                Log.Logger.Error(ex, "{callerName} callback got an error on {exchangeId}: {errorMessage}", callerName, Id, response.Content);
            }

            return true;
        }

        public override async Task BuildMetadata()
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);
                
                var response = await RestRequest(Method.GET, "/api/v1/market/symbols", null, false,
                    RestRequestType.JsonBody);
                if (IsRestError(response))
                    throw new Exception();

                var data = JsonConvert.DeserializeObject<BitforexResponse<List<BitforexSymbol>>>(response.Content);

                SupportedSymbols = data.Data
                    .ComputeAlso(x => x.Symbol.Split('-'))
                    .Select(x => new Symbol(
                        x.computed[2], x.computed[2].ToUpper(),
                        x.computed[1], x.computed[1].ToUpper(),
                        x.element.Symbol,
                        0m, null, x.element.PricePrecision,
                        x.element.MinOrderAmount, null, x.element.AmountPrecision,
                        0m
                    )).ToArray();

                SupportedCurrencies = GetSupportedCurrencies(SupportedSymbols);
            }
        }

        public class BitforexSymbol
        {
            public string Symbol;
            public int PricePrecision;
            public int AmountPrecision;
            public decimal MinOrderAmount;
        }

        protected override async Task<Dictionary<string, decimal>> GetBalanceCore()
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);

                var response = await RestRequest(Method.POST, "/api/v1/fund/allAccount", null, true,
                    RestRequestType.Default);

                if (IsRestError(response))
                    return null;

                var data = JsonConvert.DeserializeObject<BitforexResponse<List<BitforexBalance>>>(response.Content);

                return data.Data
                    .ToDictionary(x => x.Currency.ToUpper(), x => x.Active);
            }
        }

        public class BitforexBalance
        {
            public string Currency;
            public decimal Active;
//            public decimal Fix;
//            public decimal Frozen;
        }

        protected override async Task<OrderInfo[]> GetOpenOrdersCore(string currencyPair)
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);

                var param = new Dictionary<string, object>();
                param["symbol"] = currencyPair;
                param["state"] = 0;

                var response = await RestRequest(Method.POST, "/api/v1/trade/orderInfos", param, true);

                if (IsRestError(response))
                    return null;

                var data = JsonConvert.DeserializeObject<BitforexResponse<List<BitforexOrderInfo>>>(response.Content);

                return data.Data
                    .Select(x => new OrderInfo(x.OrderId.ToString(), currencyPair, OrderSide.Unknown,
                        OrderStatus.Created, x.OrderPrice, x.OrderAmount, x.DealAmount))
                    .ToArray();
            }
        }

        protected override async Task<GetOrderBookResult> GetOrderBookCore(string currencyPair)
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);

                var param = new Dictionary<string, object>();
                param["symbol"] = currencyPair;
                param["size"] = 200;

                var response = await RestRequest(Method.GET, "/api/v1/market/depth", param, false,
                    RestRequestType.Default);
                if (IsRestError(response))
                    return null;

                var data = JsonConvert.DeserializeObject<BitforexResponse<BitforexOrderBooks>>(response.Content);

                return new GetOrderBookResult(null,
                    data.Data.Bids.ToDictionary(x => x.Price, x => x.Amount),
                    data.Data.Asks.ToDictionary(x => x.Price, x => x.Amount));
            }
        }

        public class BitforexOrderBooks
        {
            public List<BitforexAskBid> Asks;
            public List<BitforexAskBid> Bids;
        }

        public class BitforexAskBid
        {
            public decimal Amount;
            public decimal Price;
        }

        protected override async Task<GetTradesResult> GetTradesCore(string currencyPair)
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);

                var param = new Dictionary<string, object>();
                param["symbol"] = currencyPair;
                param["size"] = 600;

                var response = await RestRequest(Method.GET, "/api/v1/market/trades", param, false,
                    RestRequestType.Default);
                if (IsRestError(response))
                    return null;

                var data = JsonConvert.DeserializeObject<BitforexResponse<List<BitforexTrade>>>(response.Content);

                var trades = data.Data
                    .Select(ConvertTrade)
                    .ToArray();

                return new GetTradesResult(trades);
            }
        }

        private Trade ConvertTrade(BitforexTrade x)
        {
            var transactionTime = DateTimeOffset.FromUnixTimeMilliseconds(x.Time).UtcDateTime;
            var trade = new Trade(x.TID, x.Price, x.Amount, DirectionToOrderSide(x.Direction), transactionTime);
            return trade;
        }

        private OrderSide DirectionToOrderSide(int direction)
        {
            switch (direction)
            {
                case 1:
                    return OrderSide.Buy;
                case 2:
                    return OrderSide.Sell;
            }
            throw new Exception("Unknown direction in trades, neither 1 or 2");
        }

        public class BitforexTrade
        {
            public decimal Amount;
            public int Direction;
            public decimal Price;
            public string TID;
            public long Time;
        }

        protected override async Task<CreateOrderResult> CreateOrderCore(Order order)
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);

                if (order.Type != OrderType.Limit)
                    throw new Exception("Bitforex only supports Limit type orders");

                var param = new Dictionary<string, object>();
                param["symbol"] = order.CurrencyPair;
                param["price"] = order.Price;
                param["amount"] = order.Amount;
                param["tradeType"] = OrderSideToDirection(order.Side);

                var response = await RestRequest(Method.POST, "/api/v1/trade/placeOrder", param, true,
                    RestRequestType.Default);
                if (IsRestError(response, out var errorMessage, out var errorCode))
                {
                    return new CreateOrderResult(errorMessage, errorCode);
                }
                else
                {
                    var data = JsonConvert.DeserializeObject<BitforexResponse<BitforexCreatedOrder>>(response.Content);

                    var exchangeOrderId = data.Data.OrderId.ToString();

                    return new CreateOrderResult(exchangeOrderId);
                }
            }
        }

        private int OrderSideToDirection(OrderSide side)
        {
            switch (side)
            {
                case OrderSide.Buy:
                    return 1;
                case OrderSide.Sell:
                    return 2;
            }
            throw new Exception("Unknown order side");
        }

        public class BitforexCreatedOrder
        {
            public long OrderId;
        }

        protected override async Task<CancelOrderResult> CancelOrderCore(Order order)
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);

                var param = new Dictionary<string, object>();
                param["symbol"] = order.CurrencyPair;
                param["orderId"] = long.Parse(order.ExchangeOrderId);

                var response = await RestRequest(Method.POST, "/api/v1/trade/cancelOrder", param, true);
                if (IsRestError(response, out var errorMessage, out var errorCode))
                {
                    return new CancelOrderResult(errorMessage, errorCode, false);
                }
                else
                {
                    return new CancelOrderResult();
                }
            }
        }

        protected override async Task<OrderInfo> GetOrderInfoCore(Order order)
        {
            using (await _asyncLock.LockAsync())
            {
                await Task.Delay(Timeout);

                var param = new Dictionary<string, object>();
                param["symbol"] = order.CurrencyPair;
                param["orderId"] = long.Parse(order.ExchangeOrderId);

                var response = await RestRequest(Method.POST, "/api/v1/trade/orderInfo", param, true);
                if (IsRestError(response))
                {
                    return null;
                }
                else
                {
                    var data = JsonConvert.DeserializeObject<BitforexResponse<BitforexOrderInfo>>(response.Content);

                    return new OrderInfo(data.Data.OrderId.ToString(), data.Data.Symbol, order.Side,
                        OrderStatus.Created, data.Data.AvgPrice, data.Data.OrderAmount, data.Data.DealAmount);
                }
            }
        }

        public class BitforexOrderInfo
        {
            public string Symbol;
            public decimal AvgPrice;
            public long CreateTime;
            public long LastTime;
            public decimal DealAmount;
            public decimal OrderAmount;
            public long OrderId;
            public decimal OrderPrice;
            public int OrderState;
            public decimal TradeFee;
            // Note: docs say its a double (not int) but real data shows its an int.
            public int TradeType;
        }

    }
}
