module.exports = async function (callback, exchangeName, apiKey, apiSecret, parameters) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();
    exchange.apiKey = apiKey;
    exchange.secret = apiSecret;

    try {
        let balance = await exchange.fetchBalance(parameters);
        callback(null, balance);
    }
    catch(e) {
        callback(e, null);
    }
};