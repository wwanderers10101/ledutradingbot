﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Core.Bot;
using Core.Bot.TaskExtensions;
using Core.Bot.Timeout;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using FluentAssertions;
using Marten;
using Microsoft.AspNetCore.NodeServices;
using NUnit.Framework;

namespace Core.Exchanges.Bitforex.Tests
{
    [TestFixture]
    [Ignore("Development only")]
    public class BitforexTests
    {
        private const string CurrencyPair = "coin-usdt-btc";
        
        private BotBase _bot;
        private Exchange _exchange;
        
        [OneTimeSetUp]
        public async Task Setup()
        {
            var botCreator = new BotCreator();
            _bot = await botCreator.Create<Unit>(CreateBot);
            await _bot.Start();

            _exchange = _bot.ExchangesById.Values.First();

            await Task.Delay(1000);
        }

        [Test]
        public async Task ShouldBuildMetadata()
        {
            //BuildMetadata is called during bot creation
            _exchange.SupportedSymbols.Should().NotBeEmpty();
        }

        [Test]
        public async Task ShouldReturnOrderBook()
        {
            var index = 0;

            var order = new Order(null, null, null, CurrencyPair, null, OrderSide.Sell, null, 0,
                OrderType.Limit, null, null) {ExchangeOrderId = "83823275"};
            
            while (true)
            {
                //ToDo: Use Trigger to wait for market data
                //await _exchange.GetOrderBook(CurrencyPair);
                await _exchange.GetOrderInfo(order);

                index++;
            }
        }
        
        [Test]
        public async Task ShouldReturnTrades()
        {
            //ToDo: Use Trigger to wait for market data
            await _exchange.GetTrades(CurrencyPair);
        }

        [Test]
        public async Task ShouldReturnBalances()
        {
            var balances = await _exchange.GetBalance();

            balances.Should().NotBeEmpty();
        }

        [Test]
        public async Task ShouldReturnOpenOrders()
        {
            _exchange.CurrencyPairs = new[] { CurrencyPair };
            
            var order = new Order(null, _exchange.Id, _exchange.Name,
                CurrencyPair, "order2", OrderSide.Sell, 100000m, 0.002m, OrderType.Limit, null, "test");
            await _bot.CreateOrder(order);

            await Task.Delay(1000);

            await _exchange.GetOpenOrders();

            //ToDo: Ideally we need to recreate new BotBase for each test (by replacing OneTimeSetup to Setup)
            //but currently it doesn't work (seems like BuildMetadata call without TimeoutManager is a problem)
            //So for now we have to filter out orders from other tests which makes this test less pure
            var orders = _bot.OrdersByExchangeOrderId[_exchange.Id]
                .Values
                .Where(x => x.Status == OrderStatus.Created)
                .ToList();

            orders.Count.Should().Be(1);
            orders.First().ExchangeOrderId.Should().Be(order.ExchangeOrderId);

            foreach (var openOrder in orders)
            {
                await Task.Delay(1000);
                
                await _exchange.CancelOrder(openOrder);

                openOrder.Status.Should().Be(OrderStatus.Canceled);
            }
        }
        
        [Test]
        public async Task ShouldFailToCreateIfCreateOrderForZeroAmount()
        {
            var order = new Order(null, _exchange.Id, _exchange.Name,
                CurrencyPair, "order2", OrderSide.Sell, 100000m, 0.000m, OrderType.Limit, null, "test");
            await _bot.CreateOrder(order);

            order.Status.Should().Be(OrderStatus.FailedToCreate);
        }

        [Test]
        public async Task ShouldReturnGetOrderInfo()
        {
            var order = new Order(null, _exchange.Id, _exchange.Name,
                CurrencyPair, "order2", OrderSide.Sell, 100000m, 0.002m, OrderType.Limit, null, "test");
            await _bot.CreateOrder(order);

            var orderInfo = await _exchange.GetOrderInfo(order);

            orderInfo.Filled.Should().Be(0);
            
            await _bot.CancelOrder(order);
            order.Status.Should().Be(OrderStatus.Canceled);
        }

        [Test]
        public async Task ShouldCancelAlreadyCanceledOrder()
        {
            var order = new Order(null, _exchange.Id, _exchange.Name,
                CurrencyPair, "order2", OrderSide.Sell, 100000m, 0.002m, OrderType.Limit, null, "test");
            await _bot.CreateOrder(order);

            order.Status.Should().Be(OrderStatus.Created);

            await _bot.CancelOrder(order);
            
            order.Status.Should().Be(OrderStatus.Canceled);
            
            await _bot.CancelOrder(order);
            order.Status.Should().Be(OrderStatus.Canceled);
        }
        
        private static BotBase CreateBot(
            IDocumentStore documentStore,
            ICollection<Exchange> exchanges,
            AppSettings<Unit> appSettings,
            IDateTimeService dateTimeService,
            ITimeoutManager timeoutManager)
        {
            return new BotBase(documentStore, exchanges, appSettings.Core, new DateTimeService(), null);
        }
    }
}