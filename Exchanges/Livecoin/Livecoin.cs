﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using RestSharp;
using Serilog;

namespace Core.Exchanges.Livecoin
{
    public partial class Livecoin : Exchange
    {
        private Task _timerTask;

        public Livecoin(ExchangeSettings settings, bool isRecordingMarketData, bool isReducingMarketData, bool isTrading)
            : base(ExtendSettings(settings), isRecordingMarketData, isReducingMarketData, isTrading)
        {
           _timerTask = Task.Run(TimerCallback);
        }

        private static ExchangeSettings ExtendSettings(ExchangeSettings settings)
        {
            settings.RestHost = "https://api.livecoin.net";
            return settings;
        }

        public override void AddRestAuthenticationHeaders(RestRequest request, IDictionary<string, object> parameters)
        {
            var inputString = Utils.HttpEncode(Utils.GetSortedHttpString(parameters));
            var inputBytes = Encoding.UTF8.GetBytes(inputString);
            var secretBytes = Encoding.UTF8.GetBytes(ApiSecret);
            var signatureBytes = new HMACSHA256(secretBytes).ComputeHash(inputBytes);
            var signature = signatureBytes.ToHexString().ToUpper();

            request.AddHeader("Api-Key", ApiKey);
            request.AddHeader("Sign", signature);
        }
        
        private bool IsRestError(string json, out string errorMessage, out long errorCode, [CallerMemberName] string callerName = "")
        {
            errorMessage = String.Empty;
            errorCode = 0;
            
            if (string.IsNullOrEmpty(json))
            {
                Log.Logger.Error("{callerName} returned content null or empty on {exchangeId}", callerName, Id);

                return true;
            }
            
            if (json.Contains("\"success\":false"))
            {
                try
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);

                    errorMessage = (string)data["exception"];
                    Log.Logger.Error("{callerName} callback got an error on {exchangeId}: {errorMessage}", callerName, Id, errorMessage);
                }
                catch (Exception ex) //we did not receive proper error response, most likely html error instead
                {
                    Log.Logger.Error(ex, "{callerName} callback got an error on {exchangeId}: {errorMessage}", callerName, Id, json);
                }
                return true;
            }

            return false;
        }

        public override async Task BuildMetadata()
        {
            var response = await RestRequest(Method.GET, "/exchange/restrictions");
            //if (IsRestError(response.Content, out var errorMessage, out var errorCode))

            var data = JsonConvert.DeserializeObject<LivecoinRestrictions>(response.Content);

            // AmountPrecision is unknown, 8 is just a guessestimate.
            SupportedSymbols = data.Restrictions
                .Select(x => new Symbol(
                    Utils.CutLeft(x.CurrencyPair), Utils.CutLeft(x.CurrencyPair),
                    Utils.CutRight(x.CurrencyPair), Utils.CutRight(x.CurrencyPair),
                    x.CurrencyPair,
                    0m, null, x.PriceScale,
                    x.MinLimitQuantity, null, 8,
                    0m
                )).ToArray();

            SupportedCurrencies = GetSupportedCurrencies(SupportedSymbols);
        }

        public class LivecoinRestrictions
        {
            public bool Success;
            public decimal MinBtcVolume;
            public List<LivecoinCurrencyPair> Restrictions;
        }

        public class LivecoinCurrencyPair
        {
            public string CurrencyPair;
            public int PriceScale;
            public decimal MinLimitQuantity;
        }

        public override async Task<Dictionary<string, decimal>> GetBalanceCore()
        {
            var response = await RestRequest(Method.GET, "/payment/balances", null, true);
            
            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
                return null;

            var data = JsonConvert.DeserializeObject<List<LivecoinAsset>>(response.Content);

            // x.Value can be 0E-8 but parses as decimal without issue.
            return data
                .Where(x => x.Type == "available")
                .ToDictionary(x => x.Currency, x => x.Value);
        }

        public class LivecoinAsset
        {
            public string Type;
            public string Currency;
            public decimal Value;
        }

        private async Task TimerCallback()
        {
            try
            {
                while (true)
                {
                    if (CurrencyPairs == null)
                    {
                        await Task.Delay(1000);
                        continue;
                    }

                    foreach (var currencyPair in CurrencyPairs)
                    {
                        await Task.Delay(7000);
                        await GetOrderBooks(currencyPair);
                        await Task.Delay(7000);
                        await GetTrades(currencyPair);
                        await Task.Delay(7000);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Logger.Error(e, "Failed to receive market data on {exchangerId}", Id);
            }
        }

        public async Task GetOrderBooks(string currencyPair)
        {
            var param = new Dictionary<string,object>();
            param["currencyPair"] = currencyPair.Replace('_', '/').ToUpper();
            param["groupByPrice"] = true; //default is False
            param["depth"] = 50; //default is none

            var response = await RestRequest(Method.GET, "/exchange/order_book", param, false);
            
            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
                return; //ToDo: How to indicate an error?

            var data = JsonConvert.DeserializeObject<LivecoinOrderBooks>(response.Content);

            HandleOrderBookSnapshot(currencyPair, null,
                data.Bids.ToDictionary(x => (decimal) double.Parse(x[0]), x => (decimal) double.Parse(x[1])),
                data.Asks.ToDictionary(x => (decimal)double.Parse(x[0]), x => (decimal)double.Parse(x[1])));
        }

        public class LivecoinOrderBooks
        {
            public long Timestamp;
            public List<List<string>> Asks;
            public List<List<string>> Bids;
        }

        public async Task GetTrades(string currencyPair)
        {
            var param = new Dictionary<string,object>();
            param["currencyPair"] = currencyPair.Replace('_', '/').ToUpper();
//            param["minutesOrHour"] = true; // true=minute false=hour, default is hour
//            param["type"] = "BUY" or "SELL"; //can filter

            var response = await RestRequest(Method.GET, "/exchange/last_trades", param);
            
            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
                return;

            var data = JsonConvert.DeserializeObject<List<LivecoinTrade>>(response.Content);

            HandlePrints(currencyPair,
                data.Select(x => new Trade(x.Id.ToString(), x.Price, x.Quantity, x.Type)) );
        }

        public class LivecoinTrade
        {
            public long Time;
            public long Id;
            public decimal Price;
            public decimal Quantity;
            [JsonConverter(typeof(StringEnumConverter))]
            public OrderSide Type;
        }

        public override async Task CreateOrder(Order order)
        {
            var param = new Dictionary<string,object>();
            param["currencyPair"] = order.CurrencyPair.Replace('_', '/').ToUpper();
            param["price"] = order.Price;
            param["quantity"] = order.Amount;

            var url = order.Type == OrderType.Limit ? (order.Side == OrderSide.Buy ? "exchange/buylimit" : "exchange/selllimit") :
                      order.Type == OrderType.Market ? (order.Side == OrderSide.Buy ? "exchange/buymarket" : "exchange/sellmarket") :
                      null;

            var response = await RestRequest(Method.POST, url, param, true);

            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
            {
                HandleCreateOrderFailed(order.ClientOrderId, response.Content);
            }
            else
            {
                var data = JsonConvert.DeserializeObject<LivecoinCreatedOrder>(response.Content);
                
                HandleCreateOrderSucceeded(order.ClientOrderId, data.OrderId.ToString());
            }
        }

        public class LivecoinCreatedOrder
        {
            public bool Success;
            public bool Added;
            public long OrderId;
        }

        //cancel Livecoin LEDU/BTC 14960961651
        public override async Task CancelOrder(Order order)
        {
            var param = new Dictionary<string,object>();
            param["currencyPair"] = order.CurrencyPair.Replace('_', '/').ToUpper();
            param["orderId"] = order.ExchangeOrderId;

            var response = await RestRequest(Method.POST, "/exchange/cancellimit", param, true);

            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
            {
                var notExisting = errorMessage == "Cannot find order";
                HandleCancelOrderFailed(order.ExchangeOrderId, errorMessage, errorCode, notExisting);
            }
            else
            {
                HandleCancelOrderSucceeded(order.ExchangeOrderId);
            }
        }

        public class LivecoinCancelledOrder
        {
            public bool Success;
            public bool Cancelled;
            public string Exception;
            public decimal Quantity;
            public decimal TradeQuantity;
        }

        private object _getOrderInfoSyncObject = new object();
        protected override async Task<OrderInfo> GetOrderInfoCore(string exchangeOrderId)
        {
            lock (_getOrderInfoSyncObject)
            {
                //ToDo: Remove after TimeoutManager is implemented
                Thread.Sleep(1000);
            }

            var param = new Dictionary<string, object>();
            param["orderId"] = long.Parse(exchangeOrderId);

            var response = await RestRequest(Method.GET, "exchange/order", param, true);

            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
                return null;

            var data = JsonConvert.DeserializeObject<LivecoinOrderInfo>(response.Content);

            // - Livecoin supports both Market/Limited orders, but getting order info doesnt reveal the type.
            //ToDo: Pass proper OrderStatus
            return new OrderInfo(exchangeOrderId, null, OrderSide.Unknown, OrderStatus.Created, 
                data.Price, data.Quantity, data.Remaining_quantity);
        }

        public class LivecoinOrderInfo
        {
            public long Id;
            public long Client_Id;
            public string Status;
            public string Symbol;
            public decimal Price;
            public decimal Quantity;
            public decimal Remaining_quantity;
            public decimal Blocked;
            public decimal Blocked_remain;
            public decimal Commission_rate;
            public dynamic Trades;
        }
    }
}