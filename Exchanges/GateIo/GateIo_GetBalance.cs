﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using RestSharp;

namespace Core.Exchanges.GateIo
{
    public partial class GateIo
    {
        public override async Task<Dictionary<string, decimal>> GetBalanceCore()
        {
            var response = await RestRequest(Method.POST, "api2/1/private/balances", null, isSigned: true);

            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
                return null;

            var data = JsonConvert.DeserializeObject<GateIoBalance>(response.Content);
            
            return data.Available.Where(x => IsCurrencyIdSupported(x.Key)).ToDictionary(x => x.Key, x => x.Value);
        }
    }

    public class GateIoBalance
    {
        public bool Result;
        public int Code;
        public string Message;

        public IDictionary<string, decimal> Available;
        public IDictionary<string, decimal> Locked;
    }
}