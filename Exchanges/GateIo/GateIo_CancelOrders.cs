﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model;
using RestSharp;

namespace Core.Exchanges.GateIo
{
    public partial class GateIo
    {
        //cancel GateIo ETH_BTC 1039000586
        public override async Task CancelOrder(Order order)
        {
            var param = new Dictionary<string,object>();
            param["currencyPair"] = order.CurrencyPair;
            param["orderNumber"] = order.ExchangeOrderId;

            var response = await RestRequest(Method.POST, "api2/1/private/cancelOrder", param, isSigned: true);

            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
            {
                HandleCancelOrderFailed(order.ExchangeOrderId, errorMessage);
            }
            else
            {
                HandleCancelOrderSucceeded(order.ExchangeOrderId);
            }
        }
    }

    public class GateIoCancel
    {
        public bool Result;
        public int Code;
        public string Message;
    }
}