using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using Core.Exchanges.Core;
using Core.Model;
using Core.Model.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Linq;
using RestSharp;
using Serilog;

namespace Core.Exchanges.GateIo
{
    public partial class GateIo : Exchange
    {
        public GateIo(ExchangeSettings settings, bool isRecordingMarketData, bool isReducingMarketData, bool isTrading)
            : base(ExtendSettings(settings), isRecordingMarketData, isReducingMarketData, isTrading)
        {
            WebSocketSupportedFeatures = new WebSocketSupportedFeatures
            {
                GetOrderCompletion = true
            };
            
            Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
        }

        private static ExchangeSettings ExtendSettings(ExchangeSettings settings)
        {
            settings.WebSocketHost = "wss://ws.gate.io/v3/";
            settings.RestHost = "https://api.gate.io/";
            return settings;
        }

        public override void AddRestAuthenticationHeaders(RestRequest restRequest, IDictionary<string, object> parameters)
        {
            var signature = GetSignature(parameters, ApiSecret);
            restRequest.AddHeader("KEY", ApiKey);
            restRequest.AddHeader("SIGN", signature);
        }

        public static string GetSignature(IDictionary<string, object> param, string apiSecret)
        {
            var inputBytes = Encoding.UTF8.GetBytes(Utils.GetHttpString(param));
            var apiSecretBytes = Encoding.UTF8.GetBytes(apiSecret);
            var signatureBytes = new HMACSHA512(apiSecretBytes).ComputeHash(inputBytes);
            var signature = signatureBytes.ToHexString().ToLower();
            return signature;
        }

        public override void Subscribe()
        {
            var tradesSubscribe = new
            {
                id = 1,
                method = "trades.subscribe",
                @params = CurrencyPairs.Select(x => x.ToUpper()).ToArray(),
            };

            WebSocketRequest(tradesSubscribe);

            var depthSubscribe = new
            {
                id = 2,
                method = "depth.subscribe",
                // 3 fields: a string "market", an integer "limit", a decimal string "interval"
                @params = CurrencyPairs.Select(x => new object[] {x.ToUpper(), 30, "0"}).ToArray(),
            };

            WebSocketRequest(depthSubscribe);

            if (IsTrading)
            {
                var nonce = Utils.GetCurrentMiliseconds();
                var signature = GetWebSocketSignature(nonce, ApiSecret);
                var signRequest = new
                {
                    id = 3,
                    method = "server.sign",
                    @params = new object[] {ApiKey, signature, nonce},
                };

                WebSocketRequest(signRequest);
            }
        }

        public static string GetWebSocketSignature(long nonce, string apiSecret)
        {
            var inputBytes = Encoding.UTF8.GetBytes(nonce.ToString());
            var apiSecretBytes = Encoding.UTF8.GetBytes(apiSecret);
            var signatureBytes = new HMACSHA512(apiSecretBytes).ComputeHash(inputBytes);
            var signature = Convert.ToBase64String(signatureBytes);
            return signature;
        }

        public override void OnWebsocketMessage(string message)
        {
            var data = JsonConvert.DeserializeObject<GateIoResponse>(message);

            if ((string) data.Result?.status == "success" && data.Id == 3) //auth succeeded
            {
                var ordersSubscribe = new
                {
                    id = 4,
                    method = "order.subscribe",
                    @params = CurrencyPairs.Select(x => x.ToUpper()).ToArray(),
                };

                WebSocketRequest(ordersSubscribe);
            }
            
            if (data.Method == "trades.update")
            {
                // Deserializing a heterogenous array is somewhat tricky, used a solution from:
                // https://stackoverflow.com/questions/28519972/deserializing-an-array-of-different-object-types
                var param = (JArray)JObject.Parse(message)["params"];
                var market = (string) param[0];
                var trades = param[1].ToObject<List<GateIoTrade>>();

                HandlePrints(market.ToLower(), 
                    trades.Select(x => new Trade(x.Id.ToString(), (decimal)x.Price, (decimal)x.Amount, x.Type)));
            }
            else if (data.Method == "depth.update")
            {
                var @params = (JArray)JObject.Parse(message)["params"];
                var clean = (bool) @params[0];
                var depth = @params[1].ToObject<GateIoAsksBids>();
                var market = (string) @params[2];

                if (clean)
                {
                    HandleOrderBookSnapshot(market.ToLower(), data.Id?.ToString(),
                        depth.Bids.ToDictionary(x => x[0], x => x[1]), depth.Asks.ToDictionary(x => x[0], x => x[1]));
                }
                else
                {
                    HandleOrderBookUpdate(market.ToLower(), data.Id?.ToString(),
                        depth.Bids?.ToDictionary(x => x[0], x => x[1]), depth.Asks?.ToDictionary(x => x[0], x => x[1]));
                }
            }
            else if (data.Method == "order.update")
            {
                var @params = (JArray)JObject.Parse(message)["params"];
                var @event = (int) @params[0]; // event type,Integer, 1: PUT, 2: UPDATE, 3: FINISH
                var order = @params[1].ToObject<GateIoOrder>();

                if (@event == 1) //Put
                {
                    //We can't really use this call for HandleCreateOrderSucceeded as they don't support clientId and
                    //we will not be able to identify our original orders
                }
                else if (@event == 2) //Update
                {
                    //It seems like GateIo doesn't provide enough information to call a granular Fill,
                }
                else if (@event == 3) //Finish
                {
                    if (order.Left == 0)
                        HandleOrderCompleted(order.Id.ToString());
                }
            }
            else if (data.Error != null)
            {
                Log.Logger.Error("Error happened on {exchangeName}", Name);
                Log.Logger.Verbose("Error {errorObject} happened on {exchangeName}", data.Error, Name);
            }
        }
        
        private bool IsRestError(string json, out string errorMessage, out long errorCode, [CallerMemberName] string callerName = "")
        {
            errorMessage = String.Empty;
            errorCode = 0;
            
            if (string.IsNullOrEmpty(json))
            {
                Log.Logger.Error("{callerName} returned content null or empty on {exchangeId}", callerName, Id);

                return true;
            }
            
            //sometimes result's value has quotes and sometimes it doesn't
            if (!(json.Contains("\"result\":true") || json.Contains("\"result\":\"true\"")))
            {
                try
                {
                    var data = JsonConvert.DeserializeObject<Dictionary<string, dynamic>>(json);

                    errorMessage = (string)data["message"];
                    errorCode = (long) data["code"];
                    Log.Logger.Error("{callerName} callback got an error on {exchangeId}: {code} {errorMessage}", callerName, Id, errorCode, errorMessage);
                }
                catch (Exception ex) //we did not receive proper error response, most likely html error instead
                {
                    Log.Logger.Error(ex, "{callerName} callback got an error on {exchangeId}: {errorMessage}", callerName, Id, json);
                }
                return true;
            }

            return false;
        }

        public class GateIoResponse
        {
            public long? Id;
            /// <summary>
            /// Null for success.
            /// </summary>
            public dynamic Error;
            /// <summary>
            /// Null for failure.
            /// </summary>
            public dynamic Result;

            public dynamic Method;
        }

        public class GateIoNotify
        {
            public Int64 Id;
            public string Method;
            public List<string> Params;
        }

        public class GateIoTradesUpdate
        {
            public string Market;
            public List<GateIoTrade> Trades;
        }

        public class GateIoTrade
        {
            public Int64 Id;
            // It seems like seconds since Unix epoch 1970, albeit docs do not specify that explicitly.
            public double Time;
            public double Price;
            public double Amount;
            [JsonConverter(typeof(StringEnumConverter))]
            public OrderSide Type;
        }

        public class GateIoAsksBids
        {
            public List<List<decimal>> Asks;
            public List<List<decimal>> Bids;
        }

        public class GateIoOrder
        {
            public long Id;
            public string Market;
            public int OrderType; // order type, 1: limit, 2: market
            public int Type; // type, 1: sell, 2: buy
            public long User;
            public double Ctime;
            public double Mtime;
            public decimal Price;
            public decimal Amount;
            public decimal Left;
            public decimal FilledAmount;
            public decimal FilledTotal;
            public decimal DealFee;
        }
    }
}
