﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Core.Model;
using Core.Model.Enums;
using Newtonsoft.Json;
using RestSharp;

namespace Core.Exchanges.GateIo
{
    public partial class GateIo
    {
        //buy GateIo ETH_BTC 0.001 0.001
        //sell GateIo ETH_BTC 10000 0.001
        public override async Task CreateOrder(Order order)
        {
            var param = new Dictionary<string,object>();
            param["currencyPair"] = order.CurrencyPair;
            param["rate"] = order.Price;
            param["amount"] = order.Amount;

            string path = null;
            if (order.Side == OrderSide.Buy)
                path = "api2/1/private/buy";
            else if (order.Side == OrderSide.Sell)
                path = "api2/1/private/sell";

            var response = await RestRequest(Method.POST, path, param, true);

            if (IsRestError(response.Content, out var errorMessage, out var errorCode))
            {
                HandleCreateOrderFailed(order.ClientOrderId, errorMessage);
            }
            else
            {
                var data = JsonConvert.DeserializeObject<GateIoBuySell>(response.Content);
                
                HandleCreateOrderSucceeded(order.ClientOrderId, data.OrderNumber.ToString());
            }
        }
    }

    public class GateIoBuySell
    {
        public bool Result;
        public int Code;
        public string Message;

        public ulong OrderNumber;
        public decimal Rate;
        public decimal LeftAmount;
        public decimal FilledAmount;
        public decimal FilledRate;
    }
}