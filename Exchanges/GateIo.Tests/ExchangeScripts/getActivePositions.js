module.exports = async function (callback, exchangeName, apiKey, apiSecret, parameters) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();
    exchange.apiKey = apiKey;
    exchange.secret = apiSecret;

    try {
        let activePositions = await exchange.privatePostPositions();
        callback(null, activePositions);
    }
    catch(e) {
        callback(e, null);
    }
};