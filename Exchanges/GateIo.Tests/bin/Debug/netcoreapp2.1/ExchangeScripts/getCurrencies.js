module.exports = async function (callback, exchangeName) {
    const ccxt = require ('ccxt');
    let exchange = new ccxt[exchangeName]();

    try {
        let markets = await exchange.load_markets();
        callback(null, exchange.currencies);
    }
    catch(e) {
        callback(e, null);
    }
};